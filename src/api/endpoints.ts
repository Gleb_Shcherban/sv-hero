export const BASE_URL = process.env.REACT_APP_BASE_API_URL;

export const AuthEndpoints = {
  Login: `${BASE_URL}/api/customer/auth/login`,
  RefreshToken: `${BASE_URL}/api/customer/auth/refresh`,
  GetMe: `${BASE_URL}/api/customer/auth/me`,
  PatchProfile: `${BASE_URL}/api/customer/auth/me`,
  UpdatePassword: `${BASE_URL}/api/customer/auth/update_password`,
};

export const VenueEndpoints = {
  Venue: `${BASE_URL}/api/customer/venue`,
  GetVenueAttributes: `${BASE_URL}/api/customer/venue/attributes`,
  CreateOrUpdateVenueAttributes: `${BASE_URL}/api/customer/venue/attributes`,
};

export const UsersEndpoints = {
  GetUsers: `${BASE_URL}/api/customer/users`,
  GetUser: `${BASE_URL}/api/customer/users/:id`,
  GetUsersStats: `${BASE_URL}/api/customer/users/stats`,
  GetUserRewards: `${BASE_URL}/api/customer/users/:id/rewards`,
  GetUserPerformanceRecords: `${BASE_URL}/api/customer/user-performance-records`,
  AddPointsManually: `${BASE_URL}/api/customer/user-performance-records`,
  GetUserRefereeRedeemed: `${BASE_URL}/api/customer/users/:id/referee-redeemed`,
  VerifyPerformanceRecords: `${BASE_URL}/api/customer/user-performance-records/:id/verify`,
  UnverifyPerformanceRecords: `${BASE_URL}/api/customer/user-performance-records/:id/unverify`,
  UpdateUserReward: `${BASE_URL}/api/customer/users/:id/rewards/:userRewardId/:action`,
};

export const getUpdateRewardUrl = (
  id: string,
  userRewardId: string,
  action: string
) => `${BASE_URL}/api/customer/users/${id}/rewards/${userRewardId}/${action}`;

export const SponsorCardsEndpoints = {
  GetSponsorCardById: `${BASE_URL}/api/customer/sponsor-cards/:id`,
  GetSponsorCards: `${BASE_URL}/api/customer/sponsor-cards`,
  CreateSponsorCard: `${BASE_URL}/api/customer/sponsor-cards`,
  PatchSponsorCard: `${BASE_URL}/api/customer/sponsor-cards/:id`,
  DeleteSponsorCard: `${BASE_URL}/api/customer/sponsor-cards/:id`,
};

export const SponsorsEndpoints = {
  GetSponsorById: `${BASE_URL}/api/customer/sponsors/:id`,
  GetSponsors: `${BASE_URL}/api/customer/sponsors`,
  CreateSponsor: `${BASE_URL}/api/customer/sponsors`,
  PatchAmplfiTemplate: `${BASE_URL}/api/customer/sponsors/:id`,
};

export const MediaEndpoints = {
  UploadFile: `${BASE_URL}/api/customer/media/upload`,
  UploadFileFromUrl: `${BASE_URL}/api/customer/media/upload-from-url`,
};

export const CampaignsEndpoints = {
  GetCampaigns: `${BASE_URL}/api/customer/amplfi-campaigns`,
  GetCampaignById: `${BASE_URL}/api/customer/amplfi-campaigns/:id`,
  CreateCampaigns: `${BASE_URL}/api/customer/amplfi-campaigns`,
  PatchCampaigns: `${BASE_URL}/api/customer/amplfi-campaigns/:id`,
  DeleteCampaignById: `${BASE_URL}/api/customer/amplfi-campaigns/:id`,
  UpdateCampaignFilter: `${BASE_URL}/api/customer/amplfi-campaigns/:id/update-video-filter`,
};

export const StockVideosEndpoints = {
  GetVideos: `${BASE_URL}/api/customer/stock-videos`,
  GetVideoById: `${BASE_URL}/api/customer/stock-videos/:id`,
  CreateVideos: `${BASE_URL}/api/customer/stock-videos`,
  PatchVideo: `${BASE_URL}/api/customer/stock-videos/:id`,
  DeleteVideo: `${BASE_URL}/api/customer/stock-videos/:id`,
  CreateVideo2: `${BASE_URL}/api/customer/stock-videos/create2`,
};

export const StoryVideosEndpoints = {
  GetVideos: `${BASE_URL}/api/customer/amplfi-story-videos`,
  GetVideoById: `${BASE_URL}/api/customer/amplfi-story-videos/:id`,
};

export const AnalyticsEndpoints = {
  GetUsersGeoJson: `${BASE_URL}/api/customer/analytics/users_geojson`,
  GetGeneralKPIs: `${BASE_URL}/api/customer/analytics/kpi`,
  GetKPI: `${BASE_URL}/api/customer/analytics/kpi/:id`,
};

export const FiltersEndpoints = {
  GetFilters: `${BASE_URL}/api/customer/stock-video-filters`,
  CreateFilter: `${BASE_URL}/api/customer/stock-video-filters`,
  GetFilterById: `${BASE_URL}/api/customer/stock-video-filters/:id`,
  DeleteFilterById: `${BASE_URL}/api/customer/stock-video-filters/:id`,
  PatchFilterById: `${BASE_URL}/api/customer/stock-video-filters/:id`,
};

export const RewardsEndpoints = {
  GetRewards: `${BASE_URL}/api/customer/rewards`,
  CreateReward: `${BASE_URL}/api/customer/rewards`,
  UpdateReward: `${BASE_URL}/api/customer/rewards/:id`,
  DeleteReward: `${BASE_URL}/api/customer/rewards/:id`,
};

export const RewardablesEndpoints = {
  GetRewardables: `${BASE_URL}/api/customer/rewardable-activities`,
  UpdateRewardable: `${BASE_URL}/api/customer/rewardable-activities/:id`,
};

export const CodesEndpoints = {
  GetCodes: `${BASE_URL}/api/customer/referral-codes?isUsed=false`,
  UpdateCodes: `${BASE_URL}/api/customer/referral-codes/import`,
};

export const getApiUrlForId = (apiUrl: string, id: string): string => {
  return apiUrl.replace(/:id/, id);
};
