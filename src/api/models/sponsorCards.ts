export interface SponsorCardApiModel {
  id: string;
  name: string;
  ctaUrl: string;
  imageUri: string;
  venueId: string;
}

export interface CreateCTACardApiModel {
  name: string;
  ctaUrl: string;
  imageUri: string;
}

export interface PatchCTACardApiModel {
  name?: string;
  ctaUrl?: string;
  imageUri?: string;
}

export interface GetSponsorCardByIdRequest {
  id: string;
}
