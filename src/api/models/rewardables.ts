export enum RewardActivityType {
  SIGN_UP = 'SIGN_UP',
  SHARE_VIDEO = 'SHARE_VIDEO',
  REFER = 'REFER'
}

export interface RewardActivityApiModel {
  id: string;
  activityType: RewardActivityType,
  venueId: string;
  title: string;
  description: string;
  imageUrl: string | null;
  points: number;
}

export interface UpdateRewardableApiModel {
  id: string;
  activityType?: string;
  venueId?: string;
  title?: string;
  description?: string;
  imageUrl?: string | null;
  points?: number;
}
