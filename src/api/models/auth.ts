export interface LoginRequestBody {
  username: string;
  password: string;
}

export interface MeApiModel {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  accessScopes: string[];
  generatedPassword: boolean;
  venueId: string;
}

export interface PatchMe {
  firstName?: string;
  lastName?: string;
  imageUrl?: string;
}

export interface UpdatePassword {
  oldPassword: string;
  newPassword: string;
}
