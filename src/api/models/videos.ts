export interface VideosApiModel {
  id: string;
  name: string;
  uri: string;
  rawContentUri: string;
  venueId: string;
  status: string;
  type: string;
  description: string;
  actionLabel: string | null;
  campaignId: string | null;
  sponsorId: string | null;
  cloudinaryUrl: string | null;
  variations: string | null;
  tags: [] | null;
  duration: number;
}

export interface PatchStockVideo {
  name: string;
}

export const isVideosApiModel = (item: any): item is VideosApiModel => {
  return (
    item.hasOwnProperty('name') &&
    item.hasOwnProperty('rawContentUri') &&
    item.hasOwnProperty('description')
  );
};
