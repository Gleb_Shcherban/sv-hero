export interface CodesApiModel {
  code: string;
  venueId: string;
  isUsed: boolean;
  createdAt: string;
}
