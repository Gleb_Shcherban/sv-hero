export interface IUsersGeoFeaturesProperties {
  city: string;
  region: string;
  country: string;
  value: number;
}

interface IUsersGeoFeaturesGeometry {
  type: string;
  coordinates: Array<number>;
}

export interface IUsersGeoFeatures {
  type: string;
  properties: IUsersGeoFeaturesProperties;
  geometry: IUsersGeoFeaturesGeometry;
}

export interface UsersGeoApiModel {
  type: string;
  features: Array<IUsersGeoFeatures>;
}

export interface KPIValueApiModel {
  date: string;
  value: number;
}

export interface GeneralKPIsApiModel {
  influencers: number;
  audience: number;
  shares: number;
  impressions: number;
  engagements: number;
  redemptions: number;
  marketingValue: number;
  salesValue: number;
  assetValue: number;
}
