export interface StoryVideosApiModel {
  id: string;
  uri: string;
  cloudinaryUrl: string | null;
  venueId: string;
  userId: string;
  shortcode: string;
  stockAudioId: string | null;
  stockVideoId: string | null;
  createdAt: string;
  campaignId?: string | null;
}

export const isStoryVideosApiModel = (
  item: any
): item is StoryVideosApiModel => {
  return (
    item.hasOwnProperty('userId') &&
    item.hasOwnProperty('shortcode') &&
    item.hasOwnProperty('cloudinaryUrl')
  );
};
