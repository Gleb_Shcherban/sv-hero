import { SponsorCardApiModel } from './sponsorCards';
import { FilterApiModel } from './filters';

export interface CampaignsApiModel {
  id: string;
  title: string;
  description: string;
  venueId: string;
  published: boolean;
  sponsorCard: SponsorCardApiModel | null;
  videoFilter: FilterApiModel | null;
}

export interface CreateCampaignsApiModel {
  title: string;
  code: string;
  description: string;
  sponsorCardId?: string;
}

export interface PatchCampaignsApiModel {
  title: string;
  description: string;
}

export interface GetCampaignsAPIModel {
  id: string;
  title: string;
  code: string;
  description: string;
  venueId: string;
  sponsorCard: SponsorCardApiModel | null;
  videoFilter: FilterApiModel | null;
  published: boolean;
  createdAt: string;
}
