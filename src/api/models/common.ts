import { ChosenFile } from '../../common/components/dragAndDrop/DragAndDropState';

export interface GetTableDataRequest {
  page: number;
  size?: number;
  sort?: string;
  campaignId?: string;
  stockVideoCampaignId?: string;
  firstName?: string;
  userId?: string;
}

export interface GetTableDataResponse<T> {
  items: T[];
  totalPages: number;
  totalItems: number;
  page: number;
  size: number;
}

export enum MediaFilePrefix {
  Client = 'client',
  Sponsors = 'sponsors',
}

export interface UploadMediaFileRequest {
  mediaFile: ChosenFile;
  prefix: MediaFilePrefix;
}

export interface UploadMediaFileResponse {
  url: string;
}

export interface DeleteItemRequest {
  id: string;
}

export interface DeleteItemResponse {
  success: boolean;
}
