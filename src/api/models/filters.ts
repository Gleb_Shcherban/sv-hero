export enum VideoFilterVariationType {
  jumbo = 'JUMBO',
  circular = 'CIRCULAR',
  sequential = 'SEQUENTIAL',
}

export interface VideoFilterVariation {
  type: VideoFilterVariationType;
  imageUri: string;
}

export interface FilterApiModel {
  id: string;
  name: string;
  venueId?: string | null;
  variations: VideoFilterVariation[];
}

export interface PatchFilterApiModel {
  name: string;
}
