export interface VenueAPIModel {
  id: string;
  name: string;
  organizationName: string;
  city: string;
  logo: string;
  timeZone: string;
  subdomain: string;
}

export interface VenueAttributesAPIModel {
  [key: string]: string;
}

export enum ModalUploadImageType {
  Logo = 'logo',
  VenueAttribute = 'venueAttribute',
}

export enum VenueAttributeName {
  HomeHero = 'webapp.promo-image',
  RewardsPopUp = 'webapp.rewards-popup',
  VideoProcessingScreen = 'webapp.video-processing-screen',
  DesktopHero = 'webapp.promo-image-desktop',
  DesktopImage2 = 'webapp.desktop-phone-image',
  HowToCreateVideo = 'webapp.ftue-video',
  InviteText = 'webapp.invite-text',
  SalesCodeStructure = 'webapp.sales-code-structure',
  ShareText = 'webapp.share-text',
  AverageInfluencerFollowers = 'analytics.default-followers',
  AssetValueMultiplier = 'analytics.asset-value-multiplier',
  ImpressionViewRate = 'analytics.impressions-multiplier',
  CMPRate = 'analytics.cpm',
  SalesValue = 'analytics.sales-value',
  EngagementsMultiplier = 'analytics.engagements-multiplier',
}

export interface ImageAttribute {
  name: VenueAttributeName;
  value: string;
}

export type CreateOrUpdateVenueAttributesRequest = ImageAttribute[];
export interface CreateOrUpdateVenueAttributesResponseItem {
  id: string;
  objectId: string;
  name: VenueAttributeName;
  value: string;
}
export type CreateOrUpdateVenueAttributesResponse = CreateOrUpdateVenueAttributesResponseItem[];

export interface PatchVenue {
  logo?: string;
  organizationName?: string;
  name?: string;
}
