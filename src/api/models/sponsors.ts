export interface SponsorsApiModel {
  id: string;
  name: string;
  imageUri: string;
  isAgeRestricted: boolean;
}

export interface CreateSponsorApiModel {
  name: string;
  imageUri: string;
}

export interface PatchApmlfiTemplateApiModel {
  name?: string;
  imageUri?: string;
}
