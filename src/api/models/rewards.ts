export interface RewardApiModel {
  id: string;
  name: string;
  reward: string;
  imageUri: string;
  isActive: boolean;
  redemptionPoints: number;
  venueId: string;
}

export interface CreateRewardApiModel {
  name: string;
  reward: string;
  imageUri: string;
  isActive: boolean;
  redemptionPoints: number;
}

export interface UpdateRewardApiModel {
  id: string;
  name: string;
  reward: string;
  imageUri: string;
  isActive: boolean;
  redemptionPoints: number;
  venueId: string;
}

