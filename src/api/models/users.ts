import { RewardApiModel } from "./rewards";

export interface UserApiModel {
  id: string;
  firstName: string | null;
  lastName: string | null;
  fbId: string | null;
  email: string | null;
  phoneNumber: string | null;
  gender: string | null;
  photoUrl: string | null;
  verified: boolean;
  registrationIp: RegistrationIp;
  createdAt: string;
  referralCode: string;
  rewardPoints: number;
  socialProfiles: SocialProfile[]
}

export type SocialHandles = {
  followers: number | null;
  following:  number | null;
  biography: string | null;
  posts:  number | null;
};

export interface SocialProfile {
  id: string;
  userId: string;
  network: string;
  handle: string;
  followers: number | null;
  data: SocialHandles;
}


export interface InfluencerApiModel {
  id: string;
  firstName: string | null;
  lastName: string | null;
  email: string;
  createdAt: string;
  photoUrl: string | null;
  referralCode: string | null;
  rewardPoints: number;
  audience: number;
  shares: number;
  impressions: number;
  engagements: number;
  redemptions: number;
}
export interface RegistrationIp {
  ipAddress: string;
  countryCode: string;
  country: string;
  city: string;
  region: string | null;
  postalCode: string;
  latitude: number;
  longitude: number;
}

export enum RewardStatuses {
  verified = "VERIFIED",
  unverified = "UNVERIFIED"
}

export interface UserReward {
  id: string;
  userId: string;
  reward: RewardApiModel;
  status: RewardStatuses,
  createdAt: string;
}

export interface UserPerformanceRecord {
  id: string;
  title: string | null;
  description: string;
  activityType: string;
  status: RewardStatuses;
  imageUri: string | null;
  relatedStoryVideoId: null | string;
  rewardPoints: number;
  createdAt: string;
  userId: string;
  venueId: string;
}
export interface GetUsersWithStatsRequest {
  page?: number;
  size?: number;
  sort?: string;
  search: string;
  filterUnverifiedRewards?: boolean;
  filterUnverifiedRecords?: boolean;
}
