import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import me from './slices/meSlice';
import influencers from './slices/influencersSlice';
import sponsorCards from './slices/sponsorCardsSlice';
import sponsors from './slices/sponsorsSlice';
import campaigns from './slices/campaignsSlice';
import campaignsStoryVideos from './slices/campaignsStoryVideosSlice';
import videos from './slices/videosSlice';
import storyVideos from './slices/storyVideosSlice';
import rewards from './slices/rewardsSlice';
import venue from './slices/venueSlice';
import rewardables from './slices/rewardablesSlice';
import codes from './slices/codes';

const store = configureStore({
  reducer: {
    me,
    influencers,
    sponsorCards,
    sponsors,
    campaigns,
    campaignsStoryVideos,
    videos,
    storyVideos,
    rewards,
    venue,
    rewardables,
    codes,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
export default store;
