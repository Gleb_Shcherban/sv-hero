import { InfluencerApiModel } from '../api/models/users';
import { SponsorCardApiModel } from '../api/models/sponsorCards';
import { SponsorsApiModel } from '../api/models/sponsors';
import { CampaignsApiModel } from '../api/models/campaigns';
import { VideosApiModel } from '../api/models/videos';
import { StoryVideosApiModel } from '../api/models/storyVideos';
import { RewardApiModel } from '../api/models/rewards';
import { VenueAPIModel, VenueAttributesAPIModel } from '../api/models/venue';
import { RewardActivityApiModel } from '../api/models/rewardables';
import { CodesApiModel } from '../api/models/codes';

export interface TableDTO {
  totalPages: number;
  totalItems: number;
  page: number;
  size: number;
  sort?: string;
  isLoading: boolean;
  error: boolean;
  lastUpdated: string;
}

export interface InfluencerDataDTO extends TableDTO {
  items: InfluencerApiModel[];
}

export interface SponsorCardsDTO extends TableDTO {
  items: SponsorCardApiModel[];
}

export interface SponsorsDTO extends TableDTO {
  items: SponsorsApiModel[];
}

export interface CampaignsDTO extends TableDTO {
  items: CampaignsApiModel[];
}

export interface VideosDTO extends TableDTO {
  items: VideosApiModel[];
}

export interface StoryVideosDTO extends TableDTO {
  items: StoryVideosApiModel[];
}

export interface CampaignsVideosDTO {
  [key: string]: StoryVideosDTO;
}

export interface RewardsDTO extends TableDTO {
  items: RewardApiModel[];
}
export interface VenueDTO extends VenueAPIModel {
  isLoading: boolean;
  error: boolean;
  lastUpdated: string;
}

export interface VenueAttributesDTO {
  isLoading: boolean;
  error: boolean;
  lastUpdated: string;
  attributes: VenueAttributesAPIModel;
}

export interface RewardablesDTO {
  items: RewardActivityApiModel[];
  isLoading: boolean;
  error: boolean;
}

export interface CodesDTO extends TableDTO {
  items: CodesApiModel[];
  errorUploadCodes: string;
}
