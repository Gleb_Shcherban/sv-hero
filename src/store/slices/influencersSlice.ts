import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { InfluencerDataDTO } from '../storeModels';
import {
  InfluencerApiModel,
  UserApiModel,
  UserPerformanceRecord,
  UserReward,
  GetUsersWithStatsRequest,
} from '../../api/models/users';
import { httpClient } from '../../services/httpClient/httpClient';
import {
  getApiUrlForId,
  getUpdateRewardUrl,
  StoryVideosEndpoints,
  UsersEndpoints,
} from '../../api/endpoints';

import {
  getSortDirectionFromSortString,
  getSortFieldFromSortString,
} from '../../services/utilities';

import { GetTableDataRequest, GetTableDataResponse } from '../../api/models/common';

import { tableLoadFulFilled, tableLoadPending, tableLoadRejected } from './utils';

import { defaultPagination, OrderDirection } from '../../common/constants/constants';

const initialState: InfluencerDataDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: defaultPagination.sortByLastCreated,
  lastUpdated: new Date().toISOString(),
};

export const getUsers = createAsyncThunk(
  'influencers/getUsers',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<GetTableDataRequest, GetTableDataResponse<UserApiModel>>({
        url: UsersEndpoints.GetUsers,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const getUserRewardsRequest = async (
  id: string,
  options: GetTableDataRequest
): Promise<GetTableDataResponse<UserReward>> => {
  return await httpClient.get({
    url: getApiUrlForId(UsersEndpoints.GetUserRewards, id),
    requiresToken: true,
    params: { ...options, sort: defaultPagination.sortByLastCreated },
  });
};

export const getUsersPerformanceRecords = async (
  id: string,
  options: GetTableDataRequest
): Promise<GetTableDataResponse<UserPerformanceRecord>> => {
  return httpClient.get({
    url: UsersEndpoints.GetUserPerformanceRecords,
    requiresToken: true,
    params: { userId: id, ...options, sort: defaultPagination.sortByLastCreated },
  });
};

export const getUserRefereeRedeemed = async (id: string): Promise<{ success: boolean }> => {
  return httpClient.post({
    url: getApiUrlForId(UsersEndpoints.GetUserRefereeRedeemed, id),
    requiresToken: true,
  });
};

interface AddPointsRequest {
  userId: string;
  rewardPoints: number | null;
  description: string;
}
export const addPointsManually = async (
  options: AddPointsRequest
): Promise<UserPerformanceRecord> => {
  return httpClient.post({
    url: UsersEndpoints.AddPointsManually,
    requiresToken: true,
    payload: options,
  });
};

export const verifyPerofmanceRecord = async (id: string): Promise<UserPerformanceRecord> => {
  return httpClient.put({
    url: getApiUrlForId(UsersEndpoints.VerifyPerformanceRecords, id),
    requiresToken: true,
  });
};

export const unverifyPerofmanceRecord = async (id: string): Promise<UserPerformanceRecord> => {
  return httpClient.put({
    url: getApiUrlForId(UsersEndpoints.UnverifyPerformanceRecords, id),
    requiresToken: true,
  });
};

export const updateUserReward = async (
  id: string,
  userId: string,
  action: string
): Promise<UserReward> => {
  return httpClient.put({
    url: getUpdateRewardUrl(id, userId, action),
    requiresToken: true,
  });
};

export const getStoryVideoById = async (id: string): Promise<{ uri: string }> => {
  return httpClient.get({
    url: getApiUrlForId(StoryVideosEndpoints.GetVideoById, id),
    requiresToken: true,
  });
};

export const getUsersWithStats = createAsyncThunk(
  'influencers/getUsersWithStats',
  async (options: GetUsersWithStatsRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetUsersWithStatsRequest,
        GetTableDataResponse<InfluencerApiModel>
      >({
        url: UsersEndpoints.GetUsersStats,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const getTableItemsCount = async (options: GetUsersWithStatsRequest) => {
  try {
    const result = await httpClient.get<
      GetUsersWithStatsRequest,
      GetTableDataResponse<InfluencerApiModel>
    >({
      url: UsersEndpoints.GetUsersStats,
      requiresToken: true,
      params: options,
    });
    const { totalItems } = result;
    return totalItems ? totalItems : 0;
  } catch (error) {
    console.log(error);
  }
};

const influencersSlice = createSlice({
  name: 'influencers',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setUserDataSorting(state, action: PayloadAction<string>) {
      const sortField = getSortFieldFromSortString(state.sort);
      const sortDirection = getSortDirectionFromSortString(state.sort);

      if (sortField && sortField === action.payload && sortDirection === OrderDirection.ASC) {
        state.sort = `${action.payload},${OrderDirection.DESC}`;
      } else if (
        sortField &&
        sortField === action.payload &&
        sortDirection === OrderDirection.DESC
      ) {
        state.sort = '';
      } else {
        state.sort = `${action.payload},${OrderDirection.ASC}`;
      }
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getUsersWithStats.rejected, (state) => {
      return tableLoadRejected<InfluencerDataDTO>(state);
    });
    reducersBuilder.addCase(getUsersWithStats.pending, (state) => {
      return tableLoadPending<InfluencerDataDTO>(state);
    });
    reducersBuilder.addCase(getUsersWithStats.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<InfluencerDataDTO, InfluencerApiModel>(state, payload);
    });
  },
});

export const {
  goToSelectedPage,
  setUserDataSorting,
  updateTable,
  reset,
} = influencersSlice.actions;
export default influencersSlice.reducer;
