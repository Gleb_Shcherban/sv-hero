import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SponsorsDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import { SponsorsEndpoints } from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import { SponsorsApiModel } from '../../api/models/sponsors';
import {
  setTableSorting,
  tableLoadFulFilled,
  tableLoadPending,
  tableLoadRejected,
} from './utils';
import { defaultPagination } from '../../common/constants/constants';

const initialState: SponsorsDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: defaultPagination.sort,
  lastUpdated: new Date().toISOString(),
};

export const getSponsors = createAsyncThunk(
  'sponsors/getSponsors',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetTableDataRequest,
        GetTableDataResponse<SponsorsApiModel>
      >({
        url: SponsorsEndpoints.GetSponsors,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const sponsorsSlice = createSlice({
  name: 'sponsors',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setSponsorsSorting(state, action: PayloadAction<string>) {
      return setTableSorting(state, action.payload);
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getSponsors.rejected, (state) => {
      return tableLoadRejected<SponsorsDTO>(state);
    });
    reducersBuilder.addCase(getSponsors.pending, (state) => {
      return tableLoadPending<SponsorsDTO>(state);
    });
    reducersBuilder.addCase(getSponsors.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<SponsorsDTO, SponsorsApiModel>(state, payload);
    });
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setSponsorsSorting,
  reset,
} = sponsorsSlice.actions;
export default sponsorsSlice.reducer;
