import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CampaignsVideosDTO, StoryVideosDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import { StoryVideosEndpoints } from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import { setTableSorting } from './utils';
import { StoryVideosApiModel } from '../../api/models/storyVideos';

interface PayloadWithCampaignId extends StoryVideosDTO {
  stockVideoCampaignId: string;
}

const initialState: CampaignsVideosDTO = {};

export const getCampaignVideosRequest = async (
  options: GetTableDataRequest
) => {
  return httpClient.get<
    GetTableDataRequest,
    GetTableDataResponse<StoryVideosApiModel>
  >({
    url: StoryVideosEndpoints.GetVideos,
    requiresToken: true,
    params: options,
  });
};

export const getCampaignStoryVideos = createAsyncThunk(
  'campaignsStoryVideos/getCampaignStoryVideos',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      const result = await getCampaignVideosRequest(options);
      return { ...result, stockVideoCampaignId: options.stockVideoCampaignId };
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const campaignsStoryVideosSlice = createSlice({
  name: 'campaignsStoryVideos',
  initialState,
  reducers: {
    goToSelectedPage(
      state,
      action: PayloadAction<{ page: number; id: string }>
    ) {
      const { id, page } = action.payload;
      state[id].page = page;
    },
    setCampaignsSorting(
      state,
      action: PayloadAction<{ sort: string; id: string }>
    ) {
      const { id, sort } = action.payload;
      const campaignVideos = state[id];
      state[id] = setTableSorting(campaignVideos, sort);
    },
    updateTable(state, action: PayloadAction<{ id: string }>) {
      const { id } = action.payload;
      state[id].lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(
      getCampaignStoryVideos.rejected,
      (_state, action) => {
        const payload = action.payload as PayloadWithCampaignId;
        _state[payload.stockVideoCampaignId].isLoading = false;
        _state[payload.stockVideoCampaignId].error = true;
      }
    );
    reducersBuilder.addCase(
      getCampaignStoryVideos.fulfilled,
      (_state, action) => {
        const payload = action.payload as PayloadWithCampaignId;
        const stateItems = _state[payload.stockVideoCampaignId]?.items || [];
        payload.items.forEach((item) => {
          if (!stateItems.some((stateItem) => stateItem.id === item.id)) {
            stateItems.push(item);
          }
        });
        _state[payload.stockVideoCampaignId] = {
          error: !payload.totalItems,
          isLoading: false,
          items: stateItems,
          totalItems: payload.totalItems,
          totalPages: payload.totalPages,
          page: 0,
          size: payload.size,
          sort: payload.sort,
          lastUpdated: new Date().toISOString(),
        };
        return _state;
      }
    );
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setCampaignsSorting,
  reset,
} = campaignsStoryVideosSlice.actions;
export default campaignsStoryVideosSlice.reducer;
