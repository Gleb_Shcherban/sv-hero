import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RewardsEndpoints, getApiUrlForId } from '../../api/endpoints';
import {
  DeleteItemRequest,
  DeleteItemResponse,
  GetTableDataResponse,
} from '../../api/models/common';
import { CreateRewardApiModel, RewardApiModel } from '../../api/models/rewards';
import { httpClient } from '../../services/httpClient/httpClient';
import { RewardsDTO } from '../storeModels';
import { tableLoadPending, tableLoadRejected, updateSliceLastUpdatedValue } from './utils';

const initialState: RewardsDTO = {
  items: [],
  totalPages: 0,
  totalItems: 0,
  page: 0,
  size: 10,
  error: false,
  isLoading: false,
  lastUpdated: new Date().toISOString(),
};

export interface GetRewardsDataRequest {
  page?: number;
  size?: number;
}

export const getRewards = createAsyncThunk(
  'rewards/getRewards',
  async (options: GetRewardsDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<GetRewardsDataRequest, GetTableDataResponse<RewardApiModel>>({
        url: RewardsEndpoints.GetRewards,
        requiresToken: true,
        params: options,
      });
    } catch (err) {
      return rejectWithValue(err.response.data.message);
    }
  }
);

export const createReward = createAsyncThunk(
  'rewards/createReward',
  async (payload: CreateRewardApiModel, { rejectWithValue }) => {
    try {
      return await httpClient.post<CreateRewardApiModel, RewardApiModel>({
        url: RewardsEndpoints.CreateReward,
        requiresToken: true,
        payload: payload,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const deleteReward = createAsyncThunk(
  'rewards/deleteReward',
  async (payload: DeleteItemRequest, { rejectWithValue }) => {
    try {
      await httpClient.delete<DeleteItemRequest, DeleteItemResponse>({
        url: getApiUrlForId(RewardsEndpoints.DeleteReward, payload.id),
        requiresToken: true,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const updateReward = createAsyncThunk(
  'rewards/updateReward',
  async (payload: RewardApiModel, { rejectWithValue }) => {
    try {
      return await httpClient.put<RewardApiModel, RewardApiModel>({
        url: getApiUrlForId(RewardsEndpoints.UpdateReward, payload.id),
        requiresToken: true,
        payload: payload,
      });
    } catch (err) {
      return rejectWithValue(err.response.data.message);
    }
  }
);

// TODO: READ THE DOCUMENTATION ON REDUX-DEVTOOLS AND IMMER, THIS CODE IS INCORRECT
const rewardsSlice = createSlice({
  name: 'rewards',
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getRewards.rejected, (state) => {
      return tableLoadRejected<RewardsDTO>(state);
    });
    reducersBuilder.addCase(getRewards.pending, (state) => {
      return tableLoadPending(state);
    });
    reducersBuilder.addCase(getRewards.fulfilled, (state, { payload }) => {
      //TODO: fix
      return {
        ...state,
        ...payload,
        isLoading: false,
        error: false,
      };
    });

    reducersBuilder.addCase(createReward.rejected, (state) => {
      return tableLoadRejected(state);
    });
    reducersBuilder.addCase(createReward.pending, (state) => {
      return tableLoadPending(state);
    });
    reducersBuilder.addCase(createReward.fulfilled, (state, { payload }) => {
      //TODO: fix
      return {
        ...state,
        isLoading: false,
        error: false,
        items: [...state.items, payload],
      };
    });

    reducersBuilder.addCase(updateReward.pending, (state) => {
      return tableLoadPending(state);
    });
    reducersBuilder.addCase(updateReward.rejected, (state) => {
      return tableLoadRejected(state);
    });
    reducersBuilder.addCase(updateReward.fulfilled, (state, { payload }) => {
      // TODO: refactor this mess
      const arr = [...state.items];
      const newItems = arr.filter((i) => i.id !== payload.id);
      newItems.push(payload);
      return {
        ...state,
        isLoading: false,
        error: false,
        items: newItems,
      };
    });

    reducersBuilder.addCase(deleteReward.pending, (state) => {
      return tableLoadPending(state);
    });
    reducersBuilder.addCase(deleteReward.rejected, (state) => {
      return tableLoadRejected(state);
    });
    reducersBuilder.addCase(deleteReward.fulfilled, (state, { payload }) => {
      return updateSliceLastUpdatedValue(state);
    });
  },
});

export const { reset } = rewardsSlice.actions;
export default rewardsSlice.reducer;
