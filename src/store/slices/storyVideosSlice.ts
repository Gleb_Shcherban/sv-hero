import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { StoryVideosDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import { StoryVideosEndpoints } from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import { StoryVideosApiModel } from '../../api/models/storyVideos';
import {
  setTableSorting,
  tableLoadFulFilled,
  tableLoadPending,
  tableLoadRejected,
} from './utils';
import { defaultPagination } from '../../common/constants/constants';

const initialState: StoryVideosDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: 'createdAt,desc',
  lastUpdated: new Date().toISOString(),
};

export const getStoryVideos = createAsyncThunk(
  'videos/getStoryVideos',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetTableDataRequest,
        GetTableDataResponse<StoryVideosApiModel>
      >({
        url: StoryVideosEndpoints.GetVideos,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const storyVideosSlice = createSlice({
  name: 'videos',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setVideosSorting(state, action: PayloadAction<string>) {
      return setTableSorting(state, action.payload);
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getStoryVideos.rejected, (state) => {
      return tableLoadRejected<StoryVideosDTO>(state);
    });
    reducersBuilder.addCase(getStoryVideos.pending, (state) => {
      return tableLoadPending<StoryVideosDTO>(state);
    });
    reducersBuilder.addCase(getStoryVideos.fulfilled, (state, { payload }) => {
      let items: StoryVideosApiModel[];
      if (payload.page === 0) {
        items = payload.items;
      } else {
        items = [...state.items, ...payload.items];
      }

      payload.items = items;

      return tableLoadFulFilled<StoryVideosDTO, StoryVideosApiModel>(
        state,
        payload
      );
    });
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setVideosSorting,
  reset,
} = storyVideosSlice.actions;
export default storyVideosSlice.reducer;
