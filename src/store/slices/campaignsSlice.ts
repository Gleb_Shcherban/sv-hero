import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CampaignsDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import { CampaignsEndpoints } from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import {
  CampaignsApiModel,
  GetCampaignsAPIModel,
} from '../../api/models/campaigns';
import {
  setTableSorting,
  tableLoadFulFilled,
  tableLoadPending,
  tableLoadRejected,
} from './utils';
import { defaultPagination } from '../../common/constants/constants';

const initialState: CampaignsDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: defaultPagination.sort,
  lastUpdated: new Date().toISOString(),
};

export const getCampaignsRequest = async (options?: GetTableDataRequest) => {
  return httpClient.get<
    GetTableDataRequest,
    GetTableDataResponse<GetCampaignsAPIModel>
  >({
    url: CampaignsEndpoints.GetCampaigns,
    requiresToken: true,
    params: options,
  });
};

export const getCampaigns = createAsyncThunk(
  'campaigns/getCampaigns',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await getCampaignsRequest(options);
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const createCampaignRequest = async (values: {
  title: string;
  description: string;
}) => {
  return httpClient.post<
    { title: string; description: string },
    { title: string; description: string }
  >({
    url: CampaignsEndpoints.CreateCampaigns,
    payload: values,
    requiresToken: true,
  });
};

const campaignsSlice = createSlice({
  name: 'campaigns',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setCampaignsSorting(state, action: PayloadAction<string>) {
      return setTableSorting(state, action.payload);
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getCampaigns.rejected, (state) => {
      return tableLoadRejected<CampaignsDTO>(state);
    });
    reducersBuilder.addCase(getCampaigns.pending, (state) => {
      return tableLoadPending<CampaignsDTO>(state);
    });
    reducersBuilder.addCase(getCampaigns.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<CampaignsDTO, CampaignsApiModel>(
        state,
        // @ts-ignore TODO: dirty ignore until update api modal in all places
        payload
      );
    });
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setCampaignsSorting,
  reset,
} = campaignsSlice.actions;
export default campaignsSlice.reducer;
