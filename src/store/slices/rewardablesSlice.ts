import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { getApiUrlForId, RewardablesEndpoints } from '../../api/endpoints';
import {
  RewardActivityApiModel,
  UpdateRewardableApiModel,
} from '../../api/models/rewardables';
import { httpClient } from '../../services/httpClient/httpClient';
import { RewardablesDTO } from '../storeModels';

const initialState: RewardablesDTO = {
  items: [],
  isLoading: false,
  error: false,
};

interface GetRewardablesDataRequest {}

export const getRewardables = createAsyncThunk(
  'rewardables/getRewardables',
  async (options: GetRewardablesDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetRewardablesDataRequest,
        RewardActivityApiModel[]
      >({
        url: RewardablesEndpoints.GetRewardables,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const updateRewardables = createAsyncThunk(
  'rewardables/updateRewardables',
  async (payload: UpdateRewardableApiModel, { rejectWithValue }) => {
    try {
      return await httpClient.put<
        UpdateRewardableApiModel,
        RewardActivityApiModel
      >({
        url: getApiUrlForId(RewardablesEndpoints.UpdateRewardable, payload.id),
        requiresToken: true,
        payload: payload,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const rewardablesSlice = createSlice({
  name: 'rewardables',
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getRewardables.pending, (state) => {
      return {
        ...state,
        isLoading: true,
        error: false,
      };
    });
    reducersBuilder.addCase(getRewardables.rejected, (state) => {
      return {
        ...state,
        isLoading: false,
        error: true,
      };
    });
    reducersBuilder.addCase(getRewardables.fulfilled, (state, { payload }) => {
      return {
        ...state,
        isLoading: false,
        error: false,
        items: payload,
      };
    });

    reducersBuilder.addCase(updateRewardables.pending, (state) => {
      return {
        ...state,
        isLoading: true,
        error: false,
      };
    });
    reducersBuilder.addCase(updateRewardables.rejected, (state) => {
      return {
        ...state,
        isLoading: false,
        error: true,
      };
    });
    reducersBuilder.addCase(
      updateRewardables.fulfilled,
      (state, { payload }) => {
        const arr = state.items.filter((i) => i.id !== payload.id);
        arr.push(payload);

        return {
          ...state,
          isLoading: false,
          error: false,
          items: arr,
        };
      }
    );
  },
});

export const { reset } = rewardablesSlice.actions;
export default rewardablesSlice.reducer;
