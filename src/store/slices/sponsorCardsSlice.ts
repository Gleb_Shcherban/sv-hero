import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SponsorCardsDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import { SponsorCardsEndpoints } from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import { SponsorCardApiModel } from '../../api/models/sponsorCards';
import {
  setTableSorting,
  tableLoadFulFilled,
  tableLoadPending,
  tableLoadRejected,
} from './utils';
import { defaultPagination } from '../../common/constants/constants';

const initialState: SponsorCardsDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: defaultPagination.sort,
  lastUpdated: new Date().toISOString(),
};

export const getSponsorCards = createAsyncThunk(
  'sponsorCards/getSponsorCards',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetTableDataRequest,
        GetTableDataResponse<SponsorCardApiModel>
      >({
        url: SponsorCardsEndpoints.GetSponsorCards,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const sponsorCardsSlice = createSlice({
  name: 'sponsorCards',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setSponsorCardsSorting(state, action: PayloadAction<string>) {
      return setTableSorting(state, action.payload);
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getSponsorCards.rejected, (state) => {
      return tableLoadRejected<SponsorCardsDTO>(state);
    });
    reducersBuilder.addCase(getSponsorCards.pending, (state) => {
      return tableLoadPending<SponsorCardsDTO>(state);
    });
    reducersBuilder.addCase(getSponsorCards.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<SponsorCardsDTO, SponsorCardApiModel>(
        state,
        payload
      );
    });
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setSponsorCardsSorting,
  reset,
} = sponsorCardsSlice.actions;
export default sponsorCardsSlice.reducer;
