import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { VideosDTO } from '../storeModels';
import { httpClient } from '../../services/httpClient/httpClient';
import {
  FiltersEndpoints,
  getApiUrlForId,
  StockVideosEndpoints,
} from '../../api/endpoints';
import {
  GetTableDataRequest,
  GetTableDataResponse,
} from '../../api/models/common';
import { VideosApiModel } from '../../api/models/videos';
import {
  setTableSorting,
  tableLoadFulFilled,
  tableLoadPending,
  tableLoadRejected,
} from './utils';
import {
  defaultPagination,
  // OrderDirection,
} from '../../common/constants/constants';
// import { getSortDirectionFromSortString } from '../../services/utilities';
import { FilterApiModel } from '../../api/models/filters';

const initialState: VideosDTO = {
  error: false,
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  sort: 'createdAt,desc',
  lastUpdated: new Date().toISOString(),
};

export const getVideos = createAsyncThunk(
  'videos/getVideos',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<
        GetTableDataRequest,
        GetTableDataResponse<VideosApiModel>
      >({
        url: StockVideosEndpoints.GetVideos,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const getVideosRequest = async (options?: GetTableDataRequest) => {
  return httpClient.get<
    GetTableDataRequest,
    GetTableDataResponse<VideosApiModel>
  >({
    url: StockVideosEndpoints.GetVideos,
    requiresToken: true,
    params: options,
  });
};

export const GetFilters = async (): Promise<
  GetTableDataResponse<FilterApiModel>
> => {
  return httpClient.get({
    url: FiltersEndpoints.GetFilters,
    requiresToken: true,
  });
};

export const GetFilterById = async (id: string): Promise<FilterApiModel> => {
  return httpClient.get({
    url: getApiUrlForId(FiltersEndpoints.GetFilterById, id),
    requiresToken: true,
  });
};

const videosSlice = createSlice({
  name: 'videos',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    setVideosSorting(state, action: PayloadAction<string>) {
      return setTableSorting(state, action.payload);
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getVideos.rejected, (state) => {
      return tableLoadRejected<VideosDTO>(state);
    });
    reducersBuilder.addCase(getVideos.pending, (state) => {
      return tableLoadPending<VideosDTO>(state);
    });
    reducersBuilder.addCase(getVideos.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<VideosDTO, VideosApiModel>(state, payload);
    });
  },
});

export const {
  goToSelectedPage,
  updateTable,
  setVideosSorting,
  reset,
} = videosSlice.actions;
export default videosSlice.reducer;
