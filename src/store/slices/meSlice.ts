import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MeApiModel } from '../../api/models/auth';
import { httpClient } from '../../services/httpClient/httpClient';
import { AuthEndpoints } from '../../api/endpoints';

const initialState: MeApiModel = {
  accessScopes: [],
  email: '',
  firstName: '',
  generatedPassword: false,
  id: '',
  lastName: '',
  venueId: '',
};

export const getMe = createAsyncThunk(
  'me/getMe',
  async (_options: undefined, { rejectWithValue }) => {
    try {
      return await httpClient.get<undefined, MeApiModel>({
        url: AuthEndpoints.GetMe,
        requiresToken: true,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

const meSlice = createSlice({
  name: 'me',
  initialState,
  reducers: {
    updateMe(state, action: PayloadAction<MeApiModel>) {
      state.accessScopes = action.payload.accessScopes;
      state.email = action.payload.email;
      state.firstName = action.payload.firstName;
      state.generatedPassword = action.payload.generatedPassword;
      state.id = action.payload.id;
      state.lastName = action.payload.lastName;
      state.venueId = action.payload.venueId;
    },
    reset: (state) => initialState,
  },
  extraReducers: (reducerBuilder) => {
    reducerBuilder.addCase(getMe.fulfilled, (state, { payload }) => {
      state.email = payload.email;
      state.accessScopes = payload.accessScopes;
      state.firstName = payload.firstName;
      state.lastName = payload.lastName;
      state.generatedPassword = payload.generatedPassword;
      state.id = payload.id;
      state.venueId = payload.venueId;
    });
  },
});

export const { updateMe, reset } = meSlice.actions;
export default meSlice.reducer;
