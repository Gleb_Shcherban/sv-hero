import { Draft } from 'immer';
import { TableDTO } from '../storeModels';
import { GetTableDataResponse } from '../../api/models/common';
import { InfluencerApiModel, UserApiModel } from '../../api/models/users';
import { SponsorCardApiModel } from '../../api/models/sponsorCards';
import { SponsorsApiModel } from '../../api/models/sponsors';
import { CampaignsApiModel } from '../../api/models/campaigns';
import { VideosApiModel } from '../../api/models/videos';
import { StoryVideosApiModel } from '../../api/models/storyVideos';
import { CodesApiModel } from '../../api/models/codes';
import { getSortDirectionFromSortString } from '../../services/utilities';
import { OrderDirection } from '../../common/constants/constants';

type TablePayload =
  | UserApiModel
  | SponsorCardApiModel
  | SponsorsApiModel
  | CampaignsApiModel
  | VideosApiModel
  | StoryVideosApiModel
  | InfluencerApiModel
  | CodesApiModel;

export function tableLoadRejected<T extends TableDTO>(state: Draft<T>): Draft<T> {
  state.isLoading = false;
  state.error = true;
  return state;
}

export function tableLoadPending<T extends TableDTO>(state: Draft<T>): Draft<T> {
  state.isLoading = true;
  state.error = false;
  return state;
}

export function tableLoadFulFilled<T extends TableDTO, U extends TablePayload>(
  state: Draft<T>,
  payload: GetTableDataResponse<U>
): Draft<T> {
  // TODO: this line requires a fancy TS typing magic,
  //  but we know for sure that its gonna work
  // @ts-ignore
  state.items = payload.items;
  state.isLoading = false;
  state.totalItems = payload.totalItems;
  state.totalPages = payload.totalPages;
  state.page = payload.page;
  return state;
}

export const getVideoThumbnail = (uri: string) =>
  uri.replace('s3://', 'https://s3.amazonaws.com/') + '_t.jpg';

export function setTableSorting<T extends TableDTO>(state: Draft<T>, payload: string): Draft<T> {
  const sortDirection = getSortDirectionFromSortString(state.sort);

  if (sortDirection === OrderDirection.ASC) {
    state.sort = `${payload},${OrderDirection.DESC}`;
  } else {
    state.sort = `${payload},${OrderDirection.ASC}`;
  }
  return state;
}

export function updateSliceLastUpdatedValue<T extends TableDTO>(state: Draft<T>): Draft<T> {
  state.lastUpdated = new Date().toISOString();
  return state;
}
