import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CodesDTO } from '../storeModels';
import { CodesApiModel } from '../../api/models/codes';
import { httpClient } from '../../services/httpClient/httpClient';
import { CodesEndpoints } from '../../api/endpoints';
import { GetTableDataRequest, GetTableDataResponse } from '../../api/models/common';
import { tableLoadFulFilled, tableLoadPending, tableLoadRejected } from './utils';
import { defaultPagination } from '../../common/constants/constants';

const initialState: CodesDTO = {
  error: false,
  errorUploadCodes: '',
  isLoading: false,
  items: [],
  page: defaultPagination.page,
  size: defaultPagination.size,
  totalItems: defaultPagination.totalItems,
  totalPages: defaultPagination.totalPages,
  lastUpdated: new Date().toISOString(),
};

export const getCodes = createAsyncThunk(
  'codes/getCodes',
  async (options: GetTableDataRequest, { rejectWithValue }) => {
    try {
      return await httpClient.get<GetTableDataRequest, GetTableDataResponse<CodesApiModel>>({
        url: CodesEndpoints.GetCodes,
        requiresToken: true,
        params: options,
      });
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const updateCodes = createAsyncThunk(
  'codes/updateCodes',
  async (payload: string[], { rejectWithValue }) => {
    try {
      return await httpClient.post<string[], any>({
        url: CodesEndpoints.UpdateCodes,
        requiresToken: true,
        payload: payload,
      });
    } catch (err) {
      return rejectWithValue(err.response.data.message as string);
    }
  }
);

const codesSlice = createSlice({
  name: 'codes',
  initialState,
  reducers: {
    goToSelectedPage(state, action: PayloadAction<number>) {
      state.page = action.payload;
    },
    updateTable(state) {
      state.lastUpdated = new Date().toISOString();
    },
    reset: () => initialState,
  },
  extraReducers: (reducersBuilder) => {
    reducersBuilder.addCase(getCodes.rejected, (state) => {
      return tableLoadRejected<CodesDTO>(state);
    });
    reducersBuilder.addCase(getCodes.pending, (state) => {
      return tableLoadPending<CodesDTO>(state);
    });
    reducersBuilder.addCase(getCodes.fulfilled, (state, { payload }) => {
      return tableLoadFulFilled<CodesDTO, CodesApiModel>(state, payload);
    });
    reducersBuilder.addCase(updateCodes.rejected, (state, { payload }) => {
      state.errorUploadCodes = payload as string;
    });
    reducersBuilder.addCase(updateCodes.fulfilled, (state, { payload }) => {
      state.errorUploadCodes = '';
    });
  },
});

export const { goToSelectedPage, updateTable, reset } = codesSlice.actions;
export default codesSlice.reducer;
