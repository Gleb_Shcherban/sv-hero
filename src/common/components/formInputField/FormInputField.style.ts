import styled from 'styled-components';

export const FormField = styled.div`
  display: block;
  text-align: left;
`;

interface FormFieldControlP {
  state: boolean;
}

export const FormFieldControl = styled.div<FormFieldControlP>`
  position: relative;
  padding: 0 5px 0 4px;
  width: 100%;
  overflow: hidden;

  ${(props) => {
    if (props.state) {
      return `
      input {
        color: var(--error);
        border-bottom: 1px solid var(--error);
      }
      
      label {
        color: var(--error);
      }
    `;
    }
  }};

  &:focus-within {
    .label {
      color: var(--black);
    }
  }
`;

export const FormFieldLabel = styled.label`
  display: block;
  width: 100%;
  margin-bottom: 12px;
  margin-left: 7px;
  font-size: 16px;
  font-weight: 700;
  color: var(--formColor);
`;

export const FormFieldInput = styled.input`
  display: block;
  width: 100%;
  padding: 0 12px 11px 6px;
  font-size: 24px;
  font-weight: 800;
  appearance: none;
  background: transparent;
  border: 0;
  border-bottom: 1px solid var(--black);
  color: var(--formColor);
  outline: 0;

  ::placeholder {
    color: var(--formColor);
  }
`;
