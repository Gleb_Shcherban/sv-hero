import React, { useMemo } from 'react';

import {
  FormField,
  FormFieldControl,
  FormFieldInput,
  FormFieldLabel,
} from './FormInputField.style';

interface FormInputFieldProps {
  label?: string;
  name: string;
  value: string | number;
  error: string;
  type?: string;
  placeholder: string;
  onChange: () => void;
  onBlur: () => void;
  disabled?: boolean;
  onlyInput?: boolean;
}

export const FormInputField: React.FC<FormInputFieldProps> = (props) => {
  const {
    label,
    name,
    value,
    error,
    type = 'text',
    placeholder,
    onChange,
    onBlur = () => {},
    disabled = false,
    onlyInput = false,
  } = props;

  const labelValue = useMemo(() => {
    return error ? error : label;
  }, [error, label]);

  const labelBlock = useMemo(() => {
    return (
      (labelValue || label) && (
        <FormFieldLabel htmlFor={name} className="label">
          {labelValue}
        </FormFieldLabel>
      )
    );
  }, [label, labelValue, name]);

  const InputBlock = () => {
    if (onlyInput) {
      return (
        <input
          disabled={disabled}
          id={name}
          type={type}
          value={value}
          name={name}
          onChange={onChange}
          onBlur={onBlur}
          placeholder={placeholder}
        />
      );
    }

    return (
      <FormField>
        <FormFieldControl state={!!error}>
          {labelBlock}
          <FormFieldInput
            disabled={disabled}
            id={name}
            type={type}
            value={value}
            name={name}
            onChange={onChange}
            onBlur={onBlur}
            placeholder={placeholder}
          />
        </FormFieldControl>
      </FormField>
    );
  };

  return <>{InputBlock()}</>;
};
