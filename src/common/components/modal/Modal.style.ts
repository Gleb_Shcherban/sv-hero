import styled from 'styled-components';

export const ButtonClose = styled.button`
  position: absolute;
  top: 8px;
  right: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 28px;
  background-color: black;
  border-radius: 50%;
`;

export const ModalInner = styled.div`
  @media (min-width: 768px) {
    min-width: 420px;
  }

  @media (max-width: 767px) {
    min-width: 240px;
    width: 100%;
  }
`;

export const ModalTitle = styled.h2`
  display: flex;
  justify-content: center;
  color: var(--black);
  font-size: 20px;
  font-weight: 800;
`;

export const ModalText = styled.p`
  font-size: 15px;
  margin-top: 12px;
`;

export const ModalVideoContainer = styled.div`
  display: flex;
`;

export const ButtonsGroup = styled.div`
  display: flex;
  align-items: center;
`;

export const ClearTextButton = styled.button`
  margin-top: 24px;
  padding: 7px 8px 8px;
  font-weight: 600;

  @media (min-width: 768px) {
    flex: 1;
  }

  @media (max-width: 767px) {
    order: 1;
  }

  &:hover,
  &:focus {
    color: var(--black);
  }
`;

export const PrimaryButton = styled.button`
  margin-top: 24px;
  padding: 7px 8px 8px;
  font-weight: 600;

  @media (min-width: 768px) {
    flex: 1;
  }

  @media (max-width: 767px) {
    order: 1;
  }

  &:hover,
  &:focus {
    color: var(--black);
  }
`;
