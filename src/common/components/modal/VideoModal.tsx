import React from 'react';

import Modal from 'react-modal';

import { useIsTablet } from '../../../services/hooks/useIsTablet';

interface VideoModalProps {
  children: React.ReactNode;
  modalIsOpen: boolean;
  onClose: () => void;
}

export const VideoModal: React.FC<VideoModalProps> = ({
  children,
  modalIsOpen,
  onClose,
}) => {
  const isTablet = useIsTablet();

  const modalStyles = {
    overlay: {
      backgroundColor: 'var(--blackRGBABgrModalVideo)',
      zIndex: 10,
    },
    content: {
      top: '50%',
      right: 'initial',
      bottom: 'initial',
      left: '50%',
      maxWidth: isTablet ? '100%' : '80vh',
      minHeight: '205px',
      border: '1px black',
      borderRadius: 'var(--commonBorderRadius)',
      backgroundColor: 'var(--black)',
      transform: 'translate(-50%, -50%)',
      padding: 'unset',
    },
  };

  const handleModalClose = () => onClose();

  return (
    <Modal
      shouldCloseOnOverlayClick
      onRequestClose={handleModalClose}
      isOpen={modalIsOpen}
      ariaHideApp={false}
      style={modalStyles}
    >
      {children}
    </Modal>
  );
};
