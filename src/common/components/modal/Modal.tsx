import React from 'react';

import Modal from 'react-modal';

import { useIsTablet } from '../../../services/hooks/useIsTablet';

import CloseIcon from '../../assets/CloseIcon';
import { ButtonClose, ModalInner } from './Modal.style';

interface IFSModal {
  children: React.ReactNode;
  modalIsOpen: boolean;
  onClose: () => void;
}

export const FSModal: React.FC<IFSModal> = ({
  children,
  modalIsOpen,
  onClose,
}) => {
  const isTablet = useIsTablet();

  const modalStyles = {
    overlay: {
      backgroundColor: 'var(--blackRGBABgr)',
      zIndex: 10,
    },
    content: {
      padding: '40px 20px 20px',
      top: '50%',
      right: 'initial',
      bottom: 'initial',
      left: '50%',
      minWidth: '288px',
      width: isTablet ? '100%' : '80vh',
      minHeight: '205px',
      backgroundColor: 'var(--white)',
      border: 'none',
      borderRadius: 'var(--commonBorderRadius)',
      boxShadow: 'var(--modalShadow)',
      transform: 'translate(-50%, -50%)',
    },
  };

  const handleModalClose = () => onClose();

  return (
    <Modal
      shouldCloseOnOverlayClick
      onRequestClose={handleModalClose}
      isOpen={modalIsOpen}
      ariaHideApp={false}
      style={modalStyles}
    >
      <ButtonClose
        type="button"
        aria-label="Close modal"
        onClick={handleModalClose}
      >
        <CloseIcon />
      </ButtonClose>
      <ModalInner>{children}</ModalInner>
    </Modal>
  );
};
