import styled from 'styled-components';

interface InputBoxContainerProps {
  headerText?: string;
  errorText?: string;
}
export const InputBoxContainer = styled.div<InputBoxContainerProps>`
  display: flex;
  width: 100%;
  min-height: ${(props) => {
    function valueExists(val: string | undefined): boolean {
      return !!val || val === '';
    }
    if (valueExists(props.headerText) && valueExists(props.errorText)) {
      return '74px';
    } else if (valueExists(props.headerText)) {
      return '50px';
    } else if (valueExists(props.errorText)) {
      return '50px';
    } else return '26px';
  }};
  flex-direction: column;
`;

export const InputHeader = styled.div`
  display: flex;
  width: 100%;
  font-size: 12px;
  color: var(--grey);
  margin-bottom: 6px;
`;

export const ErrorText = styled(InputHeader)`
  color: var(--error);
`;

export const InputFieldContainer = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  flex-direction: column;
`;

export const InputFieldStyled = styled.input`
  display: inline-block;
  width: 100%;
  height: 32px;
  outline: none;
  border: 1px solid var(--grey);
  border-radius: 6px;
  font-size: 14px;
  font-weight: 600;
  line-height: 1.5;
  &:focus {
    border-color: #dc3545;
  }
`;
