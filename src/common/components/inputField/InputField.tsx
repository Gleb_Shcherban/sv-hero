import React from 'react';
import {
  InputBoxContainer,
  InputFieldContainer,
  InputFieldStyled,
  ErrorText,
  InputHeader,
} from './InputField.style';

interface InputFieldProps {
  name: string;
  value: string | number | null;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  type?: string;
  headerText?: string;
  errorText?: string;
  disabled?: boolean;
}

export const InputField: React.FC<InputFieldProps> = ({
  name,
  value = '',
  onChange,
  placeholder = 'Fill the void',
  type = 'text',
  headerText,
  errorText,
  disabled,
}) => {
  const inputValue = value === null ? '' : value;
  
  return (
    <InputBoxContainer headerText={headerText} errorText={errorText}>
      {headerText && <InputHeader>{headerText}</InputHeader>}
      <InputFieldContainer>
        <InputFieldStyled
          name={name}
          type={type}
          placeholder={placeholder}
          value={inputValue}
          onChange={onChange}
          disabled={disabled}
        />
      </InputFieldContainer>
      {errorText && <ErrorText>{errorText}</ErrorText>}
    </InputBoxContainer>
  );
};
