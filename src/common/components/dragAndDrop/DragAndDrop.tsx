import React, { Dispatch } from 'react';
import {
  ChosenFile,
  DNDAction,
  DNDActionTypes,
  IDragAndDropState,
} from './DragAndDropState';
import {
  DragAndDropContainer,
  InnerText,
  PhotosUploadingContainer,
  PhotoContainer,
  UploaderWithoutFiles,
  UploadManually,
  SelectedImage,
  SelectedImageName,
  CheckIcon,
} from './DragAndDrop.style';

interface DragAndDropProps {
  data: IDragAndDropState;
  dispatch: Dispatch<DNDAction>;
  multiple?: boolean;
  resendMediaFile: (chosenFile: ChosenFile) => void;
}

export const DragAndDrop: React.FC<DragAndDropProps> = ({
  data,
  dispatch,
  multiple = false,
  resendMediaFile,
}) => {
  const onDrop = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    const files = e.dataTransfer.files.length ? [...e.dataTransfer.files] : [];
    dispatch({
      type: multiple
        ? DNDActionTypes.ADD_TO_LIST
        : DNDActionTypes.INSERT_TO_LIST,
      payload: multiple ? files : [files[0]],
    });
    dispatch({ type: DNDActionTypes.SET_OVER_DROPZONE, payload: false });
  };
  const onDragOver = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    if (!data.inDropZone) {
      dispatch({ type: DNDActionTypes.SET_OVER_DROPZONE, payload: true });
    }
  };
  const onDragLeave = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    if (data.inDropZone) {
      dispatch({ type: DNDActionTypes.SET_OVER_DROPZONE, payload: false });
    }
  };

  const addFilesByClick = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const files =
      e.target.files && e.target.files.length ? [...e.target.files] : [];
    dispatch({
      type: multiple
        ? DNDActionTypes.ADD_TO_LIST
        : DNDActionTypes.INSERT_TO_LIST,
      payload: multiple ? files : [files[0]],
    });
  };

  const viewWithoutAddedFiles = () => (
    <UploaderWithoutFiles>
      <InnerText>Drop files here</InnerText>
      <UploadManually htmlFor="file-input">
        Or click here to choose files
      </UploadManually>
      <input
        id="file-input"
        type="file"
        multiple
        onChange={addFilesByClick}
        style={{ display: 'none' }}
      />
    </UploaderWithoutFiles>
  );

  const viewWithFiles = () => (
    <UploaderWithoutFiles>
      <InnerText>
        Drop files here{' '}
        {multiple ? 'to add the to list' : 'if you want to change it'}
      </InnerText>
      <UploadManually htmlFor="file-input">
        Or click here to choose files
      </UploadManually>
      <input
        id="file-input"
        type="file"
        multiple={multiple}
        onChange={addFilesByClick}
        style={{ display: 'none' }}
      />
    </UploaderWithoutFiles>
  );

  const onClickResendMediaFile = (
    e: React.MouseEvent<HTMLDivElement>,
    chosenFile: ChosenFile
  ) => {
    e.preventDefault();
    if (chosenFile.isUploadError) {
      resendMediaFile(chosenFile);
    }
  };

  const renderUploadingPhotos = () => {
    const getUploadingProgressIcon = (chosenFile: ChosenFile) => {
      if (chosenFile.isUploadError) return 'fas fa-exclamation-triangle';
      if (chosenFile.isUploaded) return 'fas fa-check-circle';
      if (chosenFile.isUploading) return 'fas fa-spinner fa-pulse';
      else return undefined;
    };
    return (
      <PhotosUploadingContainer>
        {data.fileList.map((chosenFile, index) => (
          <PhotoContainer key={`${chosenFile.name}${index}`}>
            <SelectedImage
              src={chosenFile.localUrl}
              alt={`${chosenFile.name}`}
            />
            <SelectedImageName>
              <p>{chosenFile.name}</p>
              <p>{chosenFile.size}</p>
            </SelectedImageName>
            <CheckIcon
              error={chosenFile.isUploadError}
              onClick={(e) => onClickResendMediaFile(e, chosenFile)}
              className={getUploadingProgressIcon(chosenFile)}
            />
          </PhotoContainer>
        ))}
      </PhotosUploadingContainer>
    );
  };

  return (
    <>
      <DragAndDropContainer
        onDragOver={onDragOver}
        onDragLeave={onDragLeave}
        onDrop={onDrop}
      >
        {data.fileList.length > 0 ? viewWithFiles() : viewWithoutAddedFiles()}
      </DragAndDropContainer>
      {data.fileList.length > 0 && renderUploadingPhotos()}
    </>
  );
};
