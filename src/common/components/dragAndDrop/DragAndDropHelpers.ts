import React from 'react';
import { ChosenFile, DNDAction, DNDActionTypes } from './DragAndDropState';
import { httpClient } from '../../../services/httpClient/httpClient';
import { MediaEndpoints } from '../../../api/endpoints';
import { MediaFilePrefix } from '../../../api/models/common';

export const uploadMediaFile = (
  _file: ChosenFile,
  mediaPrefix: MediaFilePrefix,
  dispatch: React.Dispatch<DNDAction>
) => {
  dispatch({
    type: DNDActionTypes.SET_IS_UPLOADING,
    payload: { localUrl: _file.localUrl, isUploading: true },
  });
  httpClient
    .uploadFile({
      url: MediaEndpoints.UploadFile,
      requiresToken: true,
      payload: { prefix: MediaFilePrefix.Client, mediaFile: _file },
    })
    .then((result) => {
      dispatch({
        type: DNDActionTypes.SET_IS_UPLOADED,
        payload: { remoteUrl: result.url, localUrl: _file.localUrl },
      });
    })
    .catch((err) => {
      dispatch({
        type: DNDActionTypes.SET_IS_UPLOAD_ERROR,
        payload: { localUrl: _file.localUrl },
      });
    });
};
