import React, { memo, forwardRef, useMemo } from 'react';

import { FixLaterAnyType } from '../../commonTypes';

import { ErrorText } from '../inputField/InputField.style';

import { IconArrowDown, IconCheckMark } from '../../assets/ArrowIcons';
import {
  FSDropdown,
  FSDropdownItem,
  DropdownContainer,
  DropdownTitle,
} from './SimpleDropDown.style';

const { Toggle, Menu } = FSDropdown;

type Props = {
  children: React.ReactNode;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

const CustomToggle = forwardRef((props: Props, ref: FixLaterAnyType) => {
  const { children, onClick } = props;

  return (
    <button
      ref={ref}
      type="button"
      className="dropdown-btn"
      onClick={(event: React.MouseEvent<HTMLButtonElement>) => onClick(event)}
    >
      {children}
    </button>
  );
});

export interface ISimpleDropDownItem {
  id: string;
  name: string;
}

export interface SimpleDropDownProps {
  name: string;
  value: string | undefined;
  title?: string;
  errorText?: string;
  label?: string;
  alignRight?: boolean;
  menuList: Array<ISimpleDropDownItem>;
  onChange: (name: string, id: string) => void;
}

export const SimpleDropDown: React.FC<SimpleDropDownProps> = memo((props) => {
  const { label, menuList, onChange, name, errorText, title, value } = props;

  const activeIndex = useMemo(() => {
    let result = null;
    menuList.forEach((item, index) => {
      if (item.id === value) {
        result = index;
      }
    });

    return result;
  }, [menuList, value]);

  const onChangeHandler = (value: string) => {
    const numberValue = Number(value);
    onChange(name, menuList[numberValue].id);
  };

  const renderDropdownItemsList = () =>
    menuList.map((menuItem: ISimpleDropDownItem, index: number) => {
      return (
        <FSDropdownItem
          mode={activeIndex === index ? 'active' : null}
          key={index}
          eventKey={index}
        >
          {activeIndex === index && <IconCheckMark color="currentColor" />}
          <span>{menuItem.name}</span>
        </FSDropdownItem>
      );
    });

  const selectedTitle =
    activeIndex !== null ? menuList[activeIndex].name : label;

  return (
    <DropdownContainer>
      <DropdownTitle>{title}</DropdownTitle>
      <FSDropdown onSelect={onChangeHandler}>
        <Toggle as={CustomToggle}>
          {selectedTitle}
          <IconArrowDown color="var(--accentColor)" />
        </Toggle>

        <Menu>{renderDropdownItemsList()}</Menu>
      </FSDropdown>
      {errorText && <ErrorText>{errorText}</ErrorText>}
    </DropdownContainer>
  );
});
