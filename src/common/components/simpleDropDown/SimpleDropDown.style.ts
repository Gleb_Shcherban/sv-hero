import styled from 'styled-components';

import { Dropdown } from 'react-bootstrap';

const { Item } = Dropdown;

export const FSDropdown = styled(Dropdown)`
  display: flex;

  button {
    width: 100%;
    border: 1px solid #707070;
    padding: 10px 15px;
    border-radius: var(--selectMenuBorderRadius);
    color: var(--black);
    font-size: 16px;
  }

  .dropdown {
    &-btn {
      display: flex;
      justify-content: space-between;
      align-items: center;
      font-weight: 500;

      .icon-arrow-down {
        width: 11px;
        height: 7px;
        margin-left: 3px;
      }
    }

    &-menu {
      padding-top: 12px;
      padding-bottom: 12px;
      font-size: 12px;
      color: var(--black);
      background-color: var(--white);
      border: none;
      border-radius: var(--selectMenuBorderRadius);
    }
  }

  &.show {
    .icon-arrow-down {
      transform: rotate(180deg);
    }
  }
`;

interface IFSDropdownItem {
  mode: string;
}

export const FSDropdownItem = styled(Item)<IFSDropdownItem>`
  display: flex;
  justify-content: left;
  align-items: center;
  padding: 8px 16px;
  font-weight: 500;
  font-size: 15px;
  color: var(--black);

  ${(props) =>
    props.mode === 'active' ? 'background-color: var(--itemHover);' : null}

  &:hover,
  &:focus {
    background-color: var(--itemHover);
  }

  svg {
    margin-right: 10px;
  }

  span {
    margin-left: ${(props) => (props.mode !== 'active' ? '20px' : 0)};
  }
`;

export const DropdownContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 24px 0 0;
`;

export const DropdownTitle = styled.div`
  width: 100%;
  font-weight: 800;
  font-size: 14px;
  margin: 8px 0;
`;
