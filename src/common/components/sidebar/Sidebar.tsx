import React, { useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { CloseIcon } from '../../assets/CloseIcon';
import { BarsIcon } from '../../assets/BarsIcon';
import { Content } from '../../assets/Content';
import { Rewards } from '../../assets/Rewards';
import { BurgerButton } from '../burgerButton/BurgerButton';

import {
  InfoWrapper,
  SidebarBgr,
  SidebarStyled,
  Tabs,
  Tab,
  TabIcon,
  LogoutButton,
  BurgerButtonContainer,
  BurgerButtonWrapper,
  CloseIconWrapper,
  LogoIcon,
  SidebarInfo,
  SidebarName,
  SidebarEmail,
} from './Sidebar.style';
import { UnprotectedRoutes, WebsiteRoutes, getRouteById } from '../../constants/routes';
import { SidebarStrings } from '../../localization/en';
import { httpClient } from '../../../services/httpClient/httpClient';
import { useIsTablet } from '../../../services/hooks/useIsTablet';
import Influ from '../../assets/Influ';
import { Filters } from '../../assets/Filters';
import { VideosIcon } from '../../assets/VideosIcon';
import { SettingsIcon } from '../../assets/SettingsIcon';
import { CodesIcon } from '../../assets/CodesIcon';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { reset as resetMe } from '../../../store/slices/meSlice';
import { reset as resetInfluencers } from '../../../store/slices/influencersSlice';
import { reset as resetSponsorCards } from '../../../store/slices/sponsorCardsSlice';
import { reset as resetSponsors } from '../../../store/slices/sponsorsSlice';
import { reset as resetCampaigns } from '../../../store/slices/campaignsSlice';
import { reset as resetCampaignsStoryVideos } from '../../../store/slices/campaignsStoryVideosSlice';
import { reset as resetVideos } from '../../../store/slices/videosSlice';
import { reset as resetStoryVideos } from '../../../store/slices/storyVideosSlice';
import { reset as resetRewards } from '../../../store/slices/rewardsSlice';
import { reset as resetVenue } from '../../../store/slices/venueSlice';
import { reset as resetRewardables } from '../../../store/slices/rewardablesSlice';
import { reset as resetCodes } from '../../../store/slices/codes';

const MainTabs = [
  {
    icon: <BarsIcon />,
    text: SidebarStrings.Dashboard,
    link: WebsiteRoutes.Dashboard,
  },
  {
    icon: <Influ />,
    text: SidebarStrings.Influencers,
    link: getRouteById(WebsiteRoutes.Influencers, ''),
  },
  {
    icon: <Content />,
    text: SidebarStrings.Content,
    link: WebsiteRoutes.Content,
  },
  {
    icon: <VideosIcon />,
    text: SidebarStrings.Videos,
    link: WebsiteRoutes.Videos,
  },
  {
    icon: <Filters />,
    text: SidebarStrings.Filters,
    link: WebsiteRoutes.Filters,
  },
  {
    icon: <Rewards />,
    text: SidebarStrings.Rewards,
    link: WebsiteRoutes.Rewards,
  },
  {
    icon: <CodesIcon />,
    text: SidebarStrings.Codes,
    link: WebsiteRoutes.Codes,
  },
  {
    icon: <SettingsIcon />,
    text: SidebarStrings.Settings,
    link: WebsiteRoutes.Settings,
  },
];

export const Sidebar: React.FC = () => {
  const { pathname } = useLocation();
  const history = useHistory();
  const isTablet = useIsTablet();
  const dispatch = useAppDispatch();
  const { firstName, email } = useTypedSelector((state) => state.me);

  const [isSidebar, setIsSidebar] = useState<boolean>(false);

  const logOut = () => {
    httpClient.logout().then(() => {
      dispatch(resetMe());
      dispatch(resetInfluencers());
      dispatch(resetSponsorCards());
      dispatch(resetSponsors());
      dispatch(resetCampaigns());
      dispatch(resetCampaignsStoryVideos());
      dispatch(resetVideos());
      dispatch(resetStoryVideos());
      dispatch(resetRewards());
      dispatch(resetVenue());
      dispatch(resetRewardables());
      dispatch(resetRewardables());
      dispatch(resetCodes());
      history.push(UnprotectedRoutes.Login);
    });
  };

  const checkIsActiveTab = (link: string) => {
    return pathname.indexOf(link) >= 0;
  };

  const onClickSidebarManager = () => {
    setIsSidebar(!isSidebar);
  };

  const redirectToPath = (path: string) => {
    onClickSidebarManager();
    history.push(path);
  };

  const textActiveTab = () => {
    let textTab = '';
    MainTabs.forEach((tab) => {
      if (checkIsActiveTab(tab.link)) {
        textTab = tab.text;
      }
    });

    return textTab;
  };

  return (
    <>
      {isTablet && (
        <BurgerButtonContainer>
          <BurgerButtonWrapper onClick={onClickSidebarManager}>
            <BurgerButton />
          </BurgerButtonWrapper>

          {textActiveTab()}
        </BurgerButtonContainer>
      )}

      {isTablet && isSidebar && <SidebarBgr onClick={onClickSidebarManager} />}

      <SidebarStyled isSidebar={isSidebar}>
        {isTablet && (
          <CloseIconWrapper onClick={onClickSidebarManager}>
            <CloseIcon />
          </CloseIconWrapper>
        )}

        <InfoWrapper>
          <LogoIcon />
          {email && (
            <SidebarInfo>
              <SidebarName>{firstName}</SidebarName>
              <SidebarEmail>{email}</SidebarEmail>
            </SidebarInfo>
          )}
        </InfoWrapper>
        <Tabs>
          {MainTabs.map((tab) => (
            <Tab
              key={tab.link}
              active={checkIsActiveTab(tab.link)}
              onClick={() => redirectToPath(tab.link)}
            >
              {typeof tab.icon === 'string' ? (
                <TabIcon className={tab.icon} style={{ height: 23, width: 23 }} />
              ) : (
                tab.icon
              )}

              <p>{tab.text}</p>
            </Tab>
          ))}
        </Tabs>

        <LogoutButton onClick={logOut}>
          <TabIcon className="fas fa-sign-out-alt" />
          {SidebarStrings.Logout}
        </LogoutButton>
      </SidebarStyled>
    </>
  );
};
