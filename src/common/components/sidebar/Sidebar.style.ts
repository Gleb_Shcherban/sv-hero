import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

interface SidebarStyledProps {
  isSidebar: boolean;
}
export const SidebarStyled = styled.div<SidebarStyledProps>`
  display: flex;
  position: relative;
  width: 250px;
  height: 100vh;
  justify-content: flex-start;
  flex-direction: column;
  border-radius: 0 0 40px 0;
  background: linear-gradient(180deg, #2C51F5 0%, #2548E6 100%);

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    padding-top: 40px;
    position: fixed;
    height: calc(var(--vh, 1vh) * 100);
    overflow-y: auto;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    visibility: hidden;
    z-index: 31;
    transform: translateX(-100%);
    transition: transform 0.2s ease-in, visibility 0s 0.3s;

    ${(props) =>
      props.isSidebar &&
      `
        visibility: visible;
        transform: translateX(0);
        transition: transform 0.2s ease-out, visibility 0s 0s;
      `}
    }
`;

export const InfoWrapper = styled.div`
  display: flex;
  width: 100%;
  box-sizing: border-box;
  padding:  50px 20px;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    display: none;
  }
`;

interface LogoIconProps {
  imageUrl?: string;
}

export const LogoIcon = styled.div<LogoIconProps>`
  display: inline-block;
  min-height: 46px;
  min-width: 46px;
  border-radius: 14px;
  background-color: #D8D8D8;
  background-position: center center;
  background-size: contain;
  margin-right: 15px;
  ${(props) =>
    props.imageUrl && `
      background-image: url(${props.imageUrl});
    `
  }
`;

export const SidebarName = styled.div`
  font-size: 16px;
  color: #fff;
`;

export const SidebarEmail = styled.div`
  font-size: 14px;
  color: #fff;
  opacity: 0.6;
`;

export const SidebarInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow: hidden;
`;

export const MediaContent = styled.ul`
  width: 100%;
  margin-top: 30px;
  padding-left: 10px;
  display: flex;
  flex-direction: column;
`;
interface MediaTabProps {
  background: string;
  color: string;
}
export const MediaTab = styled.li<MediaTabProps>`
  display: flex;
  align-items: center;
  width: 145px;
  height: 30px;
  margin-bottom: 10px;
  padding-left: 10px;
  background: ${(props) => props.background};
  border-radius: var(--commonBorderRadius);
  cursor: pointer;

  p {
    font-size: 14px;
    font-weight: 600;
    color: ${(props) => props.color};
  }

  > div {
    color: ${(props) => props.color};
  }
`;

export const Tabs = styled.ul`
  width: 100%;
  padding-left: 20px;
  display: flex;
  flex-direction: column;
`;

export const TabIcon = styled.div`
  margin-right: 8px;
  font-size: 16px;
  color: var(--white);
`;

interface TabProps {
  active: boolean;
}
export const Tab = styled.li<TabProps>`
  display: flex;
  align-items: center;
  width: 100%;
  height: 40px;
  cursor: pointer;
  opacity: 0.6;
  padding: 26px 0;

  &:hover {
    opacity: 1;
  }

  p {
    font-size: 16px;
    font-weight: 500;
    color: var(--white);
  }

  > svg {
    margin-right: 8px;
  }

  ${(props) =>
    props.active &&
    `
      opacity: 1;
    `};
`;

export const LogoutButton = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  margin-left: 20px;
  bottom: 24px;
  left: 0;
  font-size: 14px;
  font-weight: 600;
  color: var(--white);
  cursor: pointer;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    position: static;
    margin-top: 30px;
  }
`;

export const BurgerButtonContainer = styled.div`
  position: fixed;
  width: 100%;
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 20px;
  color: white;
  font-size: 18px;
  background: linear-gradient(
    0deg,
    rgba(136, 136, 136, 1) 0%,
    rgba(86, 86, 86, 1) 100%
  );
  background-color: rgba(136, 136, 136, 1);
  z-index: 1;

  > svg {
    margin-top: 3px;
    width: 60px;
  }
`;

export const BurgerButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  border-radius: var(--commonBorderRadius);
  background-color: var(--activeColor);
`;
export const CloseIconWrapper = styled.div`
  position: absolute;
  right: 10px;
  top: 10px;
`;

export const SidebarBgr = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1;
`;
