import styled from 'styled-components';

export const Button = styled.button`
  width: 100%;
  height: 40px;
  border-radius: var(--commonBorderRadius);
  color: var(--white);
  line-height: 3;
  letter-spacing: 0.2px;
  text-align: center;
  font-size: 14px;
`;

export const ButtonDropdownContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 24px 0 0;
`;

export const ButtonDropdownTitle = styled.div`
  width: 100%;
  font-weight: 800;
  font-size: 14px;
  margin: 8px 0;
`;
