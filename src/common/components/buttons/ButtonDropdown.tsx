import React from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';

import { ButtonDropdownContainer, ButtonDropdownTitle } from './Buttons.style';
import { ErrorText } from '../inputField/InputField.style';
import { DropdownOption } from '../../commonTypes';
import './ButtonDropdown.css';
interface ButtonDropdownProps {
  name: string;
  title: string;
  errorText?: string;
  onChange: (name: string, id: string) => void;
  options: DropdownOption[]; // TODO: most definitely this data will come from redux and options will disappear
}

export const ButtonDropdown: React.FC<ButtonDropdownProps> = ({
  name,
  title,
  errorText,
  options,
  onChange,
}) => {
  const clickOption = (id: string) => {
    onChange(name, id);
  };

  return (
    <ButtonDropdownContainer>
      <ButtonDropdownTitle>{title}</ButtonDropdownTitle>
      <DropdownButton title={title} id="assign-campaigns" variant="wide-input">
        {options.map((option) => (
          <Dropdown.Item key={option.id} onClick={() => clickOption(option.id)}>
            {option.name}
          </Dropdown.Item>
        ))}
      </DropdownButton>
      {errorText && <ErrorText>{errorText}</ErrorText>}
    </ButtonDropdownContainer>
  );
};
