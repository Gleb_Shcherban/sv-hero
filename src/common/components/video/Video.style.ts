import styled from 'styled-components';
import { Spinner } from '../../assets/Spinner';
import { CtaIcon } from '../../styles/commonStyles.style';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

export const StyledSpinner = styled(Spinner)`
  width: 80px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Videos = styled.ul`
  margin-bottom: 0;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    justify-content: space-around;
  }
`;

export const VideosNew = styled.ul`
  margin-bottom: 0;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    justify-content: space-around;
  }
`;

export const VideoWrapper = styled.li`
  position: relative;
  display: flex;
  height: 432px;
  width: 265px;
  margin-bottom: 45px;
  background-color: #222;
  border-radius: 14px;
  :focus {
    outline: none;
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    height: 236px;
    width: 152px;
    margin-bottom: 25px;
  }
`;

export const VideoWrapperNew = styled(VideoWrapper)`
  height: 239px;
  width: 167px;
  margin-right: 20px;

  > video {
    border-radius: 14px;
  }
`;

export const VideoDownloadButton = styled.h3`
  font-size: 14px;
  color: #fff;
  width: calc(100% - 30px);
  position: absolute;
  bottom: 15px;
  left: 15px;
  background-color: rgba(0, 0, 0, 0.8);
  padding: 12px;
  border-radius: 14px;
  text-align: center;
  text-transform: capitalize;
  cursor: pointer;
`;

export const VideoStatisticsBar = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 9px;
  box-sizing: border-box;
  bottom: 5px;
  left: 5px;
  width: 157px;
  height: 46px;
  border-radius: 14px;
  background-color: #ffffff;
`;

export const VideoDownloadButtonNew = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 6px;
  background-color: var(--brightGreen);
  cursor: pointer;
`;

export const DownloadIcon = styled(CtaIcon)`
  width: 46px;
  height: 46px;
  align-self: flex-end;
  margin-left: 10px;
`;

export const RatingText = styled.div`
  color: var(--brightGreen);
  font-size: 24px;
  font-weight: 500;
`;

export const StyledVideo = styled.video`
  max-height: 60vh;
  width: auto;
`;

export const InfoIconWrapper = styled.img`
  position: absolute;
  top: 8px;
  right: 8px;
  width: 19px;
  height: 19px;
  cursor: pointer;
`;
