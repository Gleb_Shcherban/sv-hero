import React, { useCallback, useRef, useState, useMemo } from 'react';

// import downloadIcon from '../../assets/download.svg';
import { downloadMedia } from '../../../services/utilities';

import PlayIcon from '../../assets/playIcon.png';
import InfoIcon from '../../assets/infoIcon.png';

import { PlayVideoImg } from '../../styles/commonStyles.style';
import {
  StyledSpinner,
  VideoWrapperNew,
  // VideoStatisticsBar,
  // VideoDownloadButtonNew,
  DownloadIcon,
  // RatingText,
  StyledVideo,
  InfoIconWrapper,
} from './Video.style';
import { VideoModal } from '../modal/VideoModal';
import { ModalVideoContainer } from '../modal/Modal.style';

interface VideoProps {
  userId?: string;
  videoUrl: string;
  isInfoIcon?: boolean;
  isDownloadVideo?: boolean;
  onClickInfoButton?: (userId: string) => void;
}

export const Video: React.FC<VideoProps> = ({
  userId,
  videoUrl,
  isInfoIcon = false,
  isDownloadVideo = false,
  onClickInfoButton = () => {},
}) => {
  const refVideo = useRef<HTMLVideoElement>(null);

  const [isLoading, setIsLoading] = useState(true);
  const [isModal, setIsModal] = useState(false);

  const handleCanPlay = () => {
    setIsLoading(false);
  };

  const downloadFile = useCallback(() => {
    downloadMedia(videoUrl);
  }, [videoUrl]);

  const onClickModalHandler = useCallback(() => {
    setIsModal(!isModal);
  }, [isModal]);

  const onClickInfoButtonHandler = useCallback(() => {
    onClickInfoButton && userId && onClickInfoButton(userId);
  }, [onClickInfoButton, userId]);

  const InfoIconBlock = useMemo(() => {
    return isInfoIcon ? (
      <InfoIconWrapper onClick={onClickInfoButtonHandler} src={InfoIcon} />
    ) : null;
  }, [onClickInfoButtonHandler, isInfoIcon]);

  const DownloadBlock = useMemo(() => {
    return isDownloadVideo ? (
      <DownloadIcon className="fas fa-download" onClick={downloadFile} />
    ) : null;
  }, [isDownloadVideo, downloadFile]);

  return (
    <>
      <VideoModal modalIsOpen={isModal} onClose={onClickModalHandler}>
        <ModalVideoContainer>
          <StyledVideo
            ref={refVideo}
            autoPlay
            playsInline
            loop
            onCanPlay={handleCanPlay}
            onLoadedMetadata={handleCanPlay}
          >
            <source src={videoUrl} type="video/mp4" />
            Your browser does not support the video tag.
          </StyledVideo>
        </ModalVideoContainer>
      </VideoModal>
      <VideoWrapperNew>
        <video
          ref={refVideo}
          onClick={onClickModalHandler}
          muted
          playsInline
          loop
          onCanPlay={handleCanPlay}
          onLoadedMetadata={handleCanPlay}
        >
          <source src={videoUrl} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        {/*<VideoStatisticsBar>
          <VideoDownloadButtonNew onClick={downloadFile}>
            <DownloadIcon alt="" src={downloadIcon} />
          </VideoDownloadButtonNew>
          <RatingText>$15.12</RatingText>
        </VideoStatisticsBar>*/}

        {isLoading ? (
          <StyledSpinner />
        ) : (
          <>
            {InfoIconBlock}
            {DownloadBlock}
            <PlayVideoImg src={PlayIcon} />
          </>
        )}
      </VideoWrapperNew>
    </>
  );
};
