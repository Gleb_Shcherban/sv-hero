import styled from 'styled-components';

export const Line = styled.div``;

export const BurgerWrapper = styled.div`
  > div {
    height: 2px;
    background-color: var(--white);
    border-radius: 10px;
  }

  div:nth-child(1) {
    width: 15px;
    margin-bottom: 4px;
  }
  div:nth-child(2) {
    width: 18px;
    margin-bottom: 4px;
  }
  div:nth-child(3) {
    width: 23px;
  }
`;
