import React from 'react';

import { BurgerWrapper, Line } from './BurgerButton.style';

export const BurgerButton: React.FC = () => {
  return (
    <BurgerWrapper>
      <Line />
      <Line />
      <Line />
    </BurgerWrapper>
  );
};
