import styled from 'styled-components';

export const Button = styled.div`
  position: absolute;
  top: -23px;
  right: 5%;
  width: 48px;
  height: 48px;
  color: white;
  text-align: center;
  line-height: 45px;
  font-size: 35px;
  font-weight: 800;
  background-color: var(--activeBgr);
  border-radius: var(--commonBorderRadius);
  cursor: pointer;
`;