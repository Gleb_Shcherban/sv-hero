import React, { useCallback, useState } from 'react';

import { FSModal } from '../modal/Modal';
import { ModalTitle } from '../modal/Modal.style';
import { ModalFormWrapper } from '../../styles/commonStyles.style';

import { Button } from './AddButton.style';

interface AddButtonProps {
  children: any;
  title: string;
}

export const AddButton: React.FC<AddButtonProps> = ({ children, title }) => {
  const [isModal, setIsModal] = useState(false);

  const onClickModalHandler = useCallback(() => {
    setIsModal(!isModal);
  }, [isModal]);
  return (
    <>
      <FSModal modalIsOpen={isModal} onClose={onClickModalHandler}>
        <ModalTitle>{title}</ModalTitle>
        <ModalFormWrapper>
          {React.cloneElement(children, { setIsModal: onClickModalHandler })}
        </ModalFormWrapper>
      </FSModal>
      <Button onClick={onClickModalHandler}>+</Button>
    </>
  );
};
