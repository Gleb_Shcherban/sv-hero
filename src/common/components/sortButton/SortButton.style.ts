import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: baseline;
  font-size: 18px;
  cursor: pointer;
`;

export const SortIcon = styled.div`
  margin-right: 7px;
  color: var(--activeColor);
`;
