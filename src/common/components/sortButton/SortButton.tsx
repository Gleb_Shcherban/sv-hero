import React, { memo } from 'react';

import { Container, SortIcon } from './SortButton.style';
import { OrderDirection } from '../../constants/constants';

import { getSortDirectionFromSortString } from '../../../services/utilities';

interface SortButtonProps {
  order: string;
  onClick: () => void;
}

export const SortButton: React.FC<SortButtonProps> = memo((props) => {
  const { order, onClick } = props;
  const orderDirection = getSortDirectionFromSortString(order);

  const Icon =
    OrderDirection.DESC === orderDirection ? (
      <SortIcon className="fas fa-sort-amount-up-alt" />
    ) : (
      <SortIcon className="fas fa-sort-amount-down-alt" />
    );

  return <Container onClick={onClick}>{Icon}Sort</Container>;
});
