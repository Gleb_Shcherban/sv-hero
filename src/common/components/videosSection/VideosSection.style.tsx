import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

export const Container = styled.section`
  margin-top: 17px;
  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    padding: 0 20px;
  }
`;

export const ErrorMessage = styled.div`
  display: flex;
  margin: 24px 0;
`;
