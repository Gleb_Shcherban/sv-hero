import React, { memo, useMemo } from 'react';
import { Spinner } from '../../assets/Spinner';
import { Video } from '../video/Video';
import { VideosNew } from '../video/Video.style';
import { Container, ErrorMessage } from './VideosSection.style';
import { VideosApiModel, isVideosApiModel } from '../../../api/models/videos';
import { StoryVideosApiModel, isStoryVideosApiModel } from '../../../api/models/storyVideos';
import { useTypedSelector } from '../../../store';
import { getVideoImgUrlFromS3 } from '../../../services/utilities';

interface VideosSectionNewProps {
  isLoading: boolean;
  error: boolean;
  items: VideosApiModel[] | StoryVideosApiModel[];
  page: number;
  size: number;
  sort: string;
  lastUpdated: string;
  isInfoIcon?: boolean;
  isDownloadVideo?: boolean;
  onClickInfoButton?: (userId: string) => void;
}

export const VideosSection: React.FC<VideosSectionNewProps> = memo((props) => {
  const { isLoading, items, error, onClickInfoButton, isInfoIcon, isDownloadVideo } = props;

  const {
    venueAttributes: { attributes },
  } = useTypedSelector((state) => state.venue);

  const videosItems = useMemo(() => {
    const videos: VideosApiModel[] = [];
    items.forEach((item: any) => {
      if (isVideosApiModel(item)) {
        videos.push(item);
      }
    });

    return videos;
  }, [items]);

  const storyVideosItems = useMemo(() => {
    const storyVideos: StoryVideosApiModel[] = [];
    items.forEach((item: any) => {
      if (isStoryVideosApiModel(item)) {
        storyVideos.push(item);
      }
    });

    return storyVideos;
  }, [items]);

  const VideosBlock = useMemo(() => {
    if (isLoading) {
      return <Spinner color="var(--spinnerColor)" />;
    }

    if (error) {
      return <ErrorMessage>No videos</ErrorMessage>;
    }

    return (
      <VideosNew>
        {[...videosItems, ...storyVideosItems].map((video: any, index: number) => {
          const videoUrl = getVideoImgUrlFromS3(video.uri, attributes['webapp.client-cdn']);
          return (
            <Video
              key={index}
              videoUrl={videoUrl}
              userId={video.userId}
              isInfoIcon={isInfoIcon}
              isDownloadVideo={isDownloadVideo}
              onClickInfoButton={onClickInfoButton}
            />
          );
        })}
      </VideosNew>
    );
  }, [
    isLoading,
    error,
    videosItems,
    storyVideosItems,
    attributes,
    isInfoIcon,
    isDownloadVideo,
    onClickInfoButton,
  ]);

  return <Container>{VideosBlock}</Container>;
});
