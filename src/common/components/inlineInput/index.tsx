import React, { useState } from 'react';

const defaultTagStyles = {
  fontSize: 16,
  fontFamily: 'Arial',
  cursor: 'pointer',
  padding: 8,
};

const defaultInputStyles = {
  fontSize: 16,
  fontFamily: 'Arial',
  cursor: 'pointer',
  padding: 8,
  border: '2px solid #007aff',
  borderRadius: 8,
};

interface InlineInputProps {
  value: string;
  placeholder?: string;
  setValue(value: string): void;
  tag?: keyof JSX.IntrinsicElements;
  tagStyles?: React.CSSProperties;
  inputStyles?: React.CSSProperties;
  onChangedCallback?: () => void;
}

type keyboardEvent = React.KeyboardEvent<HTMLInputElement>;

export const InlineInput = ({
  value,
  placeholder,
  setValue,
  tag: Wrapper = 'span',
  tagStyles,
  inputStyles,
}: InlineInputProps) => {
  const [isEdit, setEdit] = useState(false);

  const inputRef = React.useRef<HTMLInputElement | null>(null);

  const handleCLick = () => {
    setEdit(true);
  };

  const handleBlur = () => {
    setEdit(false);
  };

  const handlePress = (e: keyboardEvent) => {
    let key: string | number = e.key || e.keyCode;
    if (inputRef.current && key === 'Enter') {
      inputRef.current.blur();
    }
    if (inputRef.current && key === 13) {
      inputRef.current.blur();
    }
  };

  return (
    <label onClick={handleCLick} onBlur={handleBlur}>
      {!isEdit ? (
        <Wrapper
          style={{
            ...defaultTagStyles,
            ...tagStyles,
          }}
        >
          {value ? value : placeholder}
        </Wrapper>
      ) : (
        <input
          onKeyDown={handlePress}
          onChange={(e) => setValue(e.target.value)}
          ref={inputRef}
          style={{
            ...defaultInputStyles,
            ...inputStyles,
          }}
          type="text"
          value={value}
        />
      )}
    </label>
  );
};
