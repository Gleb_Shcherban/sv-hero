import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

interface CommonTableContainerP {
  padding: string;
}

export const CommonTableContainer = styled.div<CommonTableContainerP>`
  display: flex;
  position: relative;
  flex-direction: column;
  width: 100%;
  min-height: 120px;
  box-sizing: border-box;
  padding: ${(props) => props.padding};
  margin-bottom: 24px;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    padding: 0 var(--mobileSectionPadding);
  }
`;

export const EditButtonContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 56px;
  height: 56px;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    left: 0;
    border-left: 2px solid var(--lightGrey);
    height: 100%;
  }
`;

export const EditButtonTitle = styled.p`
  font-size: 12px;
  font-weight: 600;
  color: var(--darkGreyColor);
`;

export const EditButtonTitleIcon = styled.i`
  margin-top: 8px;
  font-size: 18px;
  color: var(--grey);
`;

export const SmallImageContainer = styled.div`
  position: relative;
  width: 57px;
  height: 57px;
  border-radius: 12px;
  margin-left: 24px;
`;

export const SmallImage = styled.img`
  object-fit: contain;
`;

export const TableHeaderStyled = styled.thead`
  font-size: 10px;
  font-weight: bold;
  background-color: var(--lightGrey);

  th {
    white-space: nowrap;
  }
`;

interface TableRowProps {
  highlightRow: boolean;
}
export const TableRowStyled = styled.tr<TableRowProps>`
  color: var(--black);
  ${(props) =>
    props.highlightRow &&
    '&:hover { background-color: var(--lightBlue); color: var(--white);}'}
`;

export const ThSortable = styled.th`
  cursor: pointer;
`;

interface TdStyledProps {
  small: boolean;
}
export const TdStyled = styled.td<TdStyledProps>`
  ${(props) =>
    props.small
      ? `
    font-size: 12px;
    font-weight: 600;
    `
      : `font-size: 14px;
    font-weight: bold;
     height: 56px;
     padding: 6px !important;
    `}
`;

export const SortableIcon = styled.i`
  font-size: 10px;
`;

export const SpinnerContainer = styled.div`
  display: flex;
  position: absolute;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 200px;
`;

export const PaginationContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  padding: 10px 12px;
`;
