import React, { memo } from 'react';

// import { SortButton } from '../../sortButton/SortButton';

import { AddButton } from '../../addButton/AddButton';

import { Container, Title, HeaderSection } from './TableHeaderWrapper.style';

interface TableHeaderProps {
  title: string;
  modalTitle: string;
  orderDirection: string;
  onClickSort: () => void;
  children: React.ReactNode;
  addButtonComponent: React.ReactNode;
}

export const TableHeaderWrapper: React.FC<TableHeaderProps> = memo((props) => {
  const {
    title,
    modalTitle,
    // orderDirection,
    // onClickSort,
    children,
    addButtonComponent,
  } = props;

  return (
    <Container>
      <HeaderSection>
        <Title>{title}</Title>
        {/* <SortButton order={orderDirection} onClick={onClickSort} /> */}
        <AddButton title={modalTitle}>{addButtonComponent}</AddButton>
      </HeaderSection>
      {children}
    </Container>
  );
});
