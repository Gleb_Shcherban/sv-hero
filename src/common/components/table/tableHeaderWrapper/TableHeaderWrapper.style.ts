import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
`;

export const HeaderSection = styled.div`
  height: 62px;
  display: flex;
  align-items: center;
  flex-shrink: 0;
`;

export const Title = styled.div`
  margin-right: 40px;
  font-size: 20px;
  font-weight: 800;
  white-space: nowrap;
`;
