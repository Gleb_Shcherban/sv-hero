import React from 'react';
import { Pagination as BootstrapPagination } from 'react-bootstrap';
import { PaginationContainer } from './CommonTable.style';

interface PaginatorProps {
  totalPages: number;
  page: number;
  onPageClick: (page: number) => void;
}

export const Pagination: React.FC<PaginatorProps> = ({
  page,
  totalPages,
  onPageClick,
}) => {
  const renderFirstPageWithEllipsis = () => {
    return page > 2 ? (
      <>
        <BootstrapPagination.Item onClick={() => onPageClick(1)}>
          1
        </BootstrapPagination.Item>
        {totalPages > 3 && <BootstrapPagination.Ellipsis />}
      </>
    ) : null;
  };

  const renderLastPageWithEllipsis = () => {
    return page < totalPages - 1 ? (
      <>
        {totalPages > 3 && <BootstrapPagination.Ellipsis />}
        <BootstrapPagination.Item onClick={() => onPageClick(totalPages)}>
          {totalPages}
        </BootstrapPagination.Item>
      </>
    ) : null;
  };

  const renderCurrentPageWithNeighbouringPages = () => {
    return (
      <>
        {page > 1 && (
          <BootstrapPagination.Item onClick={() => onPageClick(page - 1)}>
            {page - 1}
          </BootstrapPagination.Item>
        )}
        <BootstrapPagination.Item active>{page}</BootstrapPagination.Item>
        {page < totalPages && (
          <BootstrapPagination.Item onClick={() => onPageClick(page + 1)}>
            {page + 1}
          </BootstrapPagination.Item>
        )}
      </>
    );
  };

  return (
    <PaginationContainer>
      <BootstrapPagination>
        {renderFirstPageWithEllipsis()}
        {renderCurrentPageWithNeighbouringPages()}
        {renderLastPageWithEllipsis()}
      </BootstrapPagination>
    </PaginationContainer>
  );
};
