import React, { ReactElement, useCallback, useState } from 'react';
import {
  EditButtonContainer,
  EditButtonTitle,
  EditButtonTitleIcon,
} from './CommonTable.style';
import { ModalFormWrapper } from '../../styles/commonStyles.style';
import { ModalTitle } from '../modal/Modal.style';
import { FSModal } from '../modal/Modal';

interface EditButtonProps {
  title?: string;
  children: any;
}

export const EditButton: React.FC<EditButtonProps> = ({ children, title }) => {
  const [isModal, setIsModal] = useState(false);

  const onClickModalHandler = useCallback(() => {
    setIsModal(!isModal);
  }, [isModal]);
  return (
    <>
      <FSModal modalIsOpen={isModal} onClose={onClickModalHandler}>
        {title && <ModalTitle>{title}</ModalTitle>}
        <ModalFormWrapper>
          {React.cloneElement(children as ReactElement<any>, { setIsModal })}
        </ModalFormWrapper>
      </FSModal>
      <EditButtonContainer onClick={onClickModalHandler}>
        <EditButtonTitle>Edit</EditButtonTitle>
        <EditButtonTitleIcon className="fas fa-pen" />
      </EditButtonContainer>
    </>
  );
};
