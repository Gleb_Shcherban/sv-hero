import React from 'react';

import { SmallImage, SmallImageContainer } from './CommonTableNew.style';

interface ImageComponentProps {
  imageRef: string;
  alt?: string;
}

export const ImageComponent: React.FC<ImageComponentProps> = ({
  imageRef,
  alt = '',
}) => {
  
  return (
    <SmallImageContainer>
      <SmallImage alt={alt} src={imageRef} />
    </SmallImageContainer>
  );
};
