import React from 'react';
import {
  CommonTableContainer,
  SortableIcon,
  TableHeaderStyled,
  TableRowStyled,
  TdStyled,
  ThSortable,
  SpinnerContainer,
} from './CommonTable.style';
import { Table } from 'react-bootstrap';
import { TableCellWithParams, TableContent, TableRow } from '../../commonTypes';
import { Pagination } from './Pagination';
import { Spinner } from '../../assets/Spinner';
import {
  OrderDirection,
  defaultPagination,
} from '../../constants/constants';
import { TableStrings } from '../../localization/en';
import {
  getSortDirectionFromSortString,
  getSortFieldFromSortString,
} from '../../../services/utilities';

interface CommonTableProps {
  content: TableContent<TableRow>;
  isLoading?: boolean;
  noContent?: boolean;
  tablePadding?: string;
  small?: boolean;
  highlightRow?: boolean;
  onSortHeaderClick?: (name: string) => void;
  page: number;
  totalItems: number;
  totalPages: number;
  sort?: string;
  goToPage: (page: number) => void;
}

/*
 *  Table accepts TableContent but constraint to the schema, that we pass in parent component.
 *  So, the data, that you pass as content should look like this:
 *
 *  const myContent: TableContent<IMyContent> = { rows: [{...}], ... }
 * */
export const CommonTable: React.FC<CommonTableProps> = ({
  content,
  tablePadding = '0 var(--sectionPadding)',
  small = false,
  highlightRow = false,
  onSortHeaderClick,
  page,
  totalItems,
  totalPages,
  sort,
  isLoading = false,
  noContent = false,
  goToPage,
}) => {
  const renderTableHeader = () => {
    const headerCells: JSX.Element[] = [];
    const tableHeader = content.header!;
    for (const columnName of Object.keys(tableHeader!)) {
      const cell = tableHeader[columnName];
      if (cell.sortable && onSortHeaderClick) {
        let sortIcon = '';
        if (getSortFieldFromSortString(sort) === columnName) {
          if (getSortDirectionFromSortString(sort) === OrderDirection.DESC) {
            sortIcon = 'fas fa-sort-down';
          }
          if (getSortDirectionFromSortString(sort) === OrderDirection.ASC) {
            sortIcon = 'fas fa-sort-up';
          }
        } else {
          sortIcon = 'fas fa-sort';
        }
        headerCells.push(
          <ThSortable
            key={columnName}
            onClick={() => onSortHeaderClick(columnName)}
          >
            {cell.name} &nbsp;
            <SortableIcon className={sortIcon} />
          </ThSortable>
        );
      } else {
        headerCells.push(<th key={columnName}>{cell.name}</th>);
      }
    }
    return (
      <TableHeaderStyled>
        <tr>{headerCells}</tr>
      </TableHeaderStyled>
    );
  };

  const renderTableRowCells = (row: TableRow) => {
    const cells: JSX.Element[] = [];
    for (const columnName of Object.keys(row)) {
      const cell = row[columnName] as TableCellWithParams<any>;
      let rowWidth: string | undefined;
      if (cell.width) {
        rowWidth = cell.width;
      } else if (columnName === 'edit') {
        rowWidth = '12px';
      }
      cells.push(
        <TdStyled
          small={small}
          style={rowWidth ? { width: rowWidth } : undefined}
          key={columnName}
        >
          {cell.render}
        </TdStyled>
      );
    }
    return <>{cells}</>;
  };

  return (
    <CommonTableContainer padding={tablePadding}>
      {isLoading && (
        <SpinnerContainer>
          <Spinner color="var(--spinnerColor)" />
        </SpinnerContainer>
      )}
      {noContent && (
        <SpinnerContainer>{TableStrings.NoContent}</SpinnerContainer>
      )}
      {!noContent && !isLoading && (
        <Table responsive style={{ border: '1px solid var(--tableBorder)' }}>
          {content.header && renderTableHeader()}

          <tbody>
            {content.rows &&
              content.rows.map((row, index) => (
                <TableRowStyled highlightRow={highlightRow} key={index}>
                  {renderTableRowCells(row)}
                </TableRowStyled>
              ))}
          </tbody>
        </Table>
      )}
      {totalItems > defaultPagination.size && !isLoading && (
        <Pagination
          totalPages={totalPages}
          page={page}
          onPageClick={goToPage}
        />
      )}
    </CommonTableContainer>
  );
};
