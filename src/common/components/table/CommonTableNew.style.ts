import styled from 'styled-components';

interface CommonTableContainerP {
  padding: string;
}

export const CommonTableContainer = styled.div<CommonTableContainerP>`
  display: flex;
  position: relative;
  flex-direction: column;
  width: 100%;
  min-height: 120px;
  box-sizing: border-box;
  padding: ${(props) => props.padding};
  margin-bottom: 24px;

  table {
    width: 100%;
    border: 0px;
    border-collapse: separate;
    border-spacing: 0px 10px;

    td,
    th {
      border: none;
    }
  }
`;

export const EditButtonContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 56px;
  height: 56px;
  cursor: pointer;

  &:before {
    content: '';
    position: absolute;
    left: 0;
    border-left: 2px solid var(--lightGrey);
    height: 100%;
  }
`;

export const EditButtonTitle = styled.p`
  font-size: 12px;
  font-weight: 600;
  color: var(--darkGreyColor);
`;

export const EditButtonTitleIcon = styled.i`
  margin-top: 8px;
  font-size: 18px;
  color: var(--grey);
`;

export const SmallImageContainer = styled.div`
  position: relative;
  width: 34px;
  height: 34px;
  border-radius: 6px;
  overflow: hidden;
  margin-right: 14px;
`;

export const SmallImage = styled.img``;

export const TableHeaderStyled = styled.thead`
  font-size: 10px;
  font-weight: bold;
  color: var(--textGrey);

  th {
    white-space: nowrap;
  }
`;

interface TableRowProps {
  selected: boolean;
}
export const TableRowStyled = styled.tr<TableRowProps>`
  color: var(--tableTextColor);

  td:first-child {
    border-radius: 6px 0 0 6px;
    border-left: 3px solid transparent;
  }

  td:last-child {
    border-radius: 0 6px 6px 0;
    border-right: 3px solid transparent;
  }

  td {
    vertical-align: middle;
    border-top: 3px solid transparent !important;
    border-bottom: 3px solid transparent !important;
    background-color: var(--white);
    cursor: pointer;
  }
  ${(props) =>
    props.selected &&
    `td {
    border-top: 3px solid #274be9 !important;
    border-bottom: 3px solid #274be9 !important;
  }
  td:first-child {
    border-left: 3px solid #274be9 !important;
  }
  td:last-child {
    border-right: 3px solid #274be9 !important;
  }`}
`;

export const ThStyled = styled.th`
  font-size: 14px;
  font-weight: normal;
  padding: 0 12px 6px 0 !important;
`;

export const ThSortable = styled(ThStyled)`
  cursor: pointer;
`;

interface TdStyledProps {
  small: boolean;
}
export const TdStyled = styled.td<TdStyledProps>`
  ${(props) =>
    props.small
      ? `
    font-size: 12px;
    font-weight: 600;
    `
      : `font-size: 14px;
    font-weight: bold;
     height: 56px;
     padding: 6px !important;
    `}
`;

export const SortableIcon = styled.i`
  font-size: 10px;
`;

export const SpinnerContainer = styled.div`
  display: flex;
  position: absolute;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 200px;
`;

export const PaginationContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  padding: 10px 12px;
`;

export const ImagesText = styled.div`
  font-weight: bold;
  font-size: 18px;
`;

export const DefaultImage = styled.div`
  width: 34px;
  height: 34px;
  border-radius: 6px;
  background-color: #22AAE1;
  margin-right: 14px;
`;
