import React from 'react';
import GoogleMapReact from 'google-map-react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

import { RegistrationIp } from '../../../../api/models/users';

import {
  GoogleMapWrapper,
  GoogleMapItemInfo,
  GoogleMapContent,
  GoogleMapCircle,
} from './LocationSection.style';

interface IGoogleMapItem {
  lat: number;
  lng: number;
  properties: RegistrationIp;
}

const defaultMapProps = {
  zoom: 10,
  fullscreenControl: true,
};

const GoogleMapItem: React.FC<IGoogleMapItem> = ({ properties }) => {
  const DescriberGoogleMapItem = Object.entries(properties).map(
    ([key, value]) => {
      return (
        <GoogleMapItemInfo key={key}>
          {key}: {value}
        </GoogleMapItemInfo>
      );
    }
  );

  return (
    <GoogleMapContent>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="button-tooltip-2">{DescriberGoogleMapItem}</Tooltip>
        }
      >
        <GoogleMapCircle />
      </OverlayTrigger>
    </GoogleMapContent>
  );
};

interface ILocationSection {
  properties: RegistrationIp;
}
export const LocationSection: React.FC<ILocationSection> = ({ properties }) => {
  if (!properties || !properties.latitude || !properties.longitude) {
    return (
      <div>
        No user location provided
      </div>
    )
  }
  
  return (
    <GoogleMapWrapper>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: 'AIzaSyCntMZpI6v5Hs4Tk0l70xBJisxwlRGxZSY',
        }}
        defaultCenter={{
          lat: properties.latitude,
          lng: properties.longitude,
        }}
        defaultZoom={defaultMapProps.zoom}
        options={{ fullscreenControl: defaultMapProps.fullscreenControl }}
      >
        <GoogleMapItem
          lat={properties.latitude}
          lng={properties.longitude}
          properties={properties}
        />
      </GoogleMapReact>
    </GoogleMapWrapper>
  );
};
