import styled from 'styled-components';

export const GoogleMapWrapper = styled.div`
  height: 328px;
  width: 100%;
  border-radius: var(--commonBorderRadius);
  overflow: hidden;
  margin-top: 10px;
`;

export const GoogleMapContent = styled.div``;

export const GoogleMapItemInfo = styled.div`
  text-align: left;
`;

export const GoogleMapCircle = styled.div`
  position: relative;
  width: 40px;
  height: 40px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: white;
  border: 2px solid #5E77FF;
  border-radius: 100%;
  transform: translate(-50%, -50%);
  background: rgba(94, 119, 255, 0.3);

  :before {
    content: '';
    position: absolute;
    width: 8px;
    height: 8px;
    background-color: #5E77FF;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    border-radius: 100%;
  }
`;
