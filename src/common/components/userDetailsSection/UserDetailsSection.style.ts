import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

export const Container = styled.section`
  margin-left: 10px;
  min-width: 460px;
  width: 630px;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    margin-bottom: 30px;
    margin-left: 0;
    min-width: unset;
    width: 100%;
  }
`;
