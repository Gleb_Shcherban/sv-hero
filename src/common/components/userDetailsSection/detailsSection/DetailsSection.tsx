import React, { useEffect, useCallback, useRef } from 'react';
import InfiniteScroll from 'react-infinite-scroller';

import { useAppDispatch, useTypedSelector } from '../../../../store';

import { UserApiModel } from '../../../../api/models/users';

import { getStringValue } from '../../../../services/utilities';

import { getStoryVideos, goToSelectedPage } from '../../../../store/slices/storyVideosSlice';

import { VideosSection } from '../../videosSection/VideosSection';

import { Text, Title } from '../../../styles/commonStyles.style';
import { Container, UserFields, UserField, ContentSection } from './DetailsSection.style';

interface IDetailsSection {
  user: UserApiModel;
}

export const DetailsSection: React.FC<IDetailsSection> = ({ user }) => {
  const scrollParentRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const { isLoading, items, totalPages, error, page, size, sort, lastUpdated } = useTypedSelector(
    (state) => state.storyVideos
  );
  const userRegistrationIp = user.registrationIp || {};

  useEffect(() => {
    dispatch(getStoryVideos({ page, sort, size, userId: user.id }));
  }, [dispatch, page, sort, size, lastUpdated, user.id]);

  const renderUserField = (title: string, text: string | null) => {
    return (
      <>
        <Title>{title}</Title>
        <Text>{getStringValue(text)}</Text>
      </>
    );
  };

  const loadMoreHandler = useCallback(
    (page) => {
      dispatch(goToSelectedPage(page));
    },
    [dispatch]
  );

  const hasMoreHandler = useCallback(() => {
    const isNextPage = totalPages - page;
    return Boolean(isNextPage && page && !isLoading);
  }, [isLoading, page, totalPages]);

  return (
    <>
      <Text>Details</Text>
      <Container>
        <UserFields>
          <UserField>{renderUserField('USER', user.firstName)}</UserField>
          <UserField>{renderUserField('CITY', userRegistrationIp.city)}</UserField>
          <UserField>{renderUserField('COUNTRY', userRegistrationIp.country)}</UserField>
          <UserField>{renderUserField('EMAIL', user.email)}</UserField>
          <UserField>{renderUserField('PHONE', user.phoneNumber)}</UserField>
          <UserField>{renderUserField('CODE', user.referralCode)}</UserField>
          <UserField>{renderUserField('TOTAL POINTS', user.rewardPoints + '')}</UserField>
        </UserFields>

        <ContentSection ref={scrollParentRef}>
          <Title>CONTENT</Title>
          <InfiniteScroll
            useWindow={false}
            pageStart={page}
            initialLoad={false}
            loadMore={loadMoreHandler}
            hasMore={hasMoreHandler()}
            getScrollParent={() => scrollParentRef.current}
          >
            <VideosSection
              isDownloadVideo
              isLoading={isLoading}
              error={error}
              items={items}
              page={page}
              size={size}
              sort={sort || ''}
              lastUpdated={lastUpdated}
            />
          </InfiniteScroll>
        </ContentSection>
      </Container>
    </>
  );
};
