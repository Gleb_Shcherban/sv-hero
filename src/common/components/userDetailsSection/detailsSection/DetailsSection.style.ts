import styled from 'styled-components';
import { MOBILE_MAX_WIDTH } from '../../../constants/constants';

export const Container = styled.div`
  margin-top: 10px;
  padding: 20px 0 0 20px;
  height: 480px;
  width: 100%;
  display: flex;
  border-radius: var(--commonBorderRadius);
  background-color: var(--white);
  overflow: hidden;

  @media (max-width: ${MOBILE_MAX_WIDTH}px) {
    height: 800px;
    flex-direction: column;
  }
`;

export const UserFields = styled.div`
  flex-basis: 50%;
  word-break: break-all;

  @media (max-width: ${MOBILE_MAX_WIDTH}px) {
    flex-basis: 35%;
  }
`;

export const UserField = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-bottom: 20px;

  > div:first-child {
    margin-bottom: 5px;
  }
`;

export const ContentSection = styled.div`
  height: 100%;
  overflow-y: auto;
  flex-basis: 65%;
`;
