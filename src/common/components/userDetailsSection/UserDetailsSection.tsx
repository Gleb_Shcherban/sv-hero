import React, { useEffect, useState } from 'react';

import { httpClient } from '../../../services/httpClient/httpClient';

import { UsersEndpoints, getApiUrlForId } from '../../../api/endpoints';

import { UserApiModel } from '../../../api/models/users';

import { DetailsSection } from './detailsSection/DetailsSection';
import { LocationSection } from './locationSection/LocationSection';

import { Spinner } from '../../assets/Spinner';

import { Container } from './UserDetailsSection.style';
import { CtaIcon } from '../../../pages/videoTool/videoTool.style';

interface IUserDetailsSection {
  userId?: string;
  closeSection?: () => void;
}

export const UserDetailsSection: React.FC<IUserDetailsSection> = ({
  userId,
  closeSection,
}) => {
  const [user, setUser] = useState<UserApiModel | null>(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (userId) {
      setIsLoading(true);
      httpClient
        .get<null, UserApiModel>({
          url: getApiUrlForId(UsersEndpoints.GetUser, userId),
          requiresToken: true,
        })
        .then((response) => {
          setUser(response);
          setIsLoading(false);
        });
    }
  }, [userId]);

  if (!user || isLoading) {
    return (
      <Container>
        <Spinner color="var(--spinnerColor)" />
      </Container>
    );
  }

  return (
    <Container>
      {closeSection && (
        <CtaIcon
          style={{
            display: 'flex',
            justifyContent: 'center',
            width: '44px',
            alignSelf: 'flex-end',
          }}
          className="fa fa-times"
          onClick={closeSection}
        />
      )}
      <DetailsSection user={user} />
      <LocationSection properties={user.registrationIp} />
    </Container>
  );
};
