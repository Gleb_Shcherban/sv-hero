import styled from 'styled-components';

export const SearchPanelWrapper = styled.div`
  position: relative;
  margin-top: 10px;
  height: 40px;
  width: 100%;
`;

export const Container = styled.div`
  width: 100%;
`;

export const CloseIconWrapper = styled.div`
  position: absolute;
  right: 13px;
  top: 8px;
  cursor: pointer;

  path {
    fill: #b2b2b2;
  }
`;

export const SearchPanelStyled = styled.input`
  height: 100%;
  border-radius: 8px;
  background-color: var(--white);
  padding-left: 40px;
  width: 100%;
`;

export const SearchIcon = styled.img`
  position: absolute;
  left: 13px;
  top: 11px;
  height: 18px;
  width: 18px;
  cursor: pointer;
`;
