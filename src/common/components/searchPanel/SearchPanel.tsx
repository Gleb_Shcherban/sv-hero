import React, { useState, useEffect } from 'react';

import { Text } from '../../styles/commonStyles.style';

import searchIcon from '../../assets/search.svg';
import CloseIcon from '../../assets/CloseIcon';
import {
  SearchPanelWrapper,
  SearchPanelStyled,
  SearchIcon,
  CloseIconWrapper,
  Container,
} from './SearchPanel.style';

interface ISearchPanel {
  onClickSearch: (value: string) => void;
  value: string;
}

export const SearchPanel: React.FC<ISearchPanel> = ({
  onClickSearch,
  value = '',
}) => {
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const onClickCloseHandler = () => {
    setInputValue('');
    onClickSearch('');
  };

  const onClickSearchHandler = () => {
    onClickSearch(inputValue);
  };

  const onKeyboardHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      onClickSearchHandler();
    }
  };

  return (
    <Container>
      <Text>Search</Text>
      <SearchPanelWrapper>
        <SearchPanelStyled
          type="text"
          placeholder="Enter search request here"
          value={inputValue}
          onChange={handleInput}
          onKeyPress={onKeyboardHandler}
        />
        <SearchIcon onClick={onClickSearchHandler} alt="" src={searchIcon} />
        {inputValue && (
          <CloseIconWrapper onClick={onClickCloseHandler}>
            <CloseIcon />
          </CloseIconWrapper>
        )}
      </SearchPanelWrapper>
    </Container>
  );
};
