import React, { memo, forwardRef, useMemo } from 'react';
import { IconArrowDown, IconCheckMark } from '../../assets/ArrowIcons';
import calendarIcon from '../../assets/calendar.svg';
import { FSDropdown, FSDropdownItem } from './PageFilter.style';

import { FixLaterAnyType } from '../../commonTypes';

const { Toggle, Menu } = FSDropdown;

type CustomToggleProps = {
  children: React.ReactNode;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
};
const CustomToggle = forwardRef(
  (props: CustomToggleProps, ref: FixLaterAnyType) => {
    const { children, onClick } = props;

    return (
      <button
        ref={ref}
        type="button"
        className="dropdown-btn"
        onClick={(event: React.MouseEvent<HTMLButtonElement>) => onClick(event)}
      >
        {children}
      </button>
    );
  }
);

export interface SimpleDropDownItem {
  id: string;
  name: string;
}

export interface PageFilterProps {
  name: string;
  value: string | undefined;
  label?: string;
  alignRight?: boolean;
  menuList: Array<SimpleDropDownItem>;
  onChange: (name: string, id: string) => void;
}

export const PageFilter: React.FC<PageFilterProps> = memo((props) => {
  const { label, menuList, onChange, name, value } = props;

  const activeIndex = useMemo(() => {
    let result = null;
    menuList.forEach((item, index) => {
      if (item.id === value) {
        result = index;
      }
    });

    return result;
  }, [menuList, value]);

  const onChangeHandler = (value: string) => {
    const numberValue = Number(value);
    onChange(name, menuList[numberValue].id);
  };

  const renderDropdownItemsList = () =>
    menuList.map((menuItem: SimpleDropDownItem, index: number) => {
      return (
        <FSDropdownItem
          mode={activeIndex === index ? 'active' : null}
          key={index}
          eventKey={index}
        >
          {activeIndex === index && <IconCheckMark color="currentColor" />}
          <span>{menuItem.name}</span>
        </FSDropdownItem>
      );
    });

  const selectedTitle =
    activeIndex !== null ? menuList[activeIndex].name : label;

  return (
    <FSDropdown onSelect={onChangeHandler}>
      <Toggle as={CustomToggle}>
        <img src={calendarIcon} alt="" />
        {selectedTitle}
        <IconArrowDown color="var(--accentColor)" />
      </Toggle>

      <Menu>{renderDropdownItemsList()}</Menu>
    </FSDropdown>
  );
});
