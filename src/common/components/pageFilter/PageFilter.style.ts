import styled from 'styled-components';
import { Dropdown } from 'react-bootstrap';

import { TABLET_MAX_WIDTH } from '../../constants/constants';

const { Item } = Dropdown;

export const FSDropdown = styled(Dropdown)`
  display: flex;
  position: absolute;
  top: 22px;
  left: 52px;

  button {
    width: 180px;
    height: 40px;
    border-radius: 8px;
    padding: 10px 20px;
    color: var(--black);
    font-size: 14px;
    background-color: var(--white);
  }

  img {
    height: unset;
    width: unset;
  }

  .dropdown {
    &-btn {
      display: flex;
      justify-content: space-between;
      align-items: center;
      font-weight: 500;

      .icon-arrow-down {
        width: 11px;
        height: 7px;
        margin-left: 3px;
      }
    }

    &-menu {
      padding-top: 12px;
      padding-bottom: 12px;
      font-size: 12px;
      color: var(--black);
      background-color: var(--white);
      border: none;
      border-radius: var(--selectMenuBorderRadius);
    }
  }

  &.show {
    .icon-arrow-down {
      transform: rotate(180deg);
    }
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    top: 92px;
  }
`;

interface FSDropdownItemProps {
  mode: string;
}

export const FSDropdownItem = styled(Item)<FSDropdownItemProps>`
  display: flex;
  justify-content: left;
  align-items: center;
  padding: 8px 16px;
  font-weight: 500;
  font-size: 15px;
  color: var(--black);

  ${(props) =>
    props.mode === 'active' ? 'background-color: var(--itemHover);' : null}

  &:hover,
  &:focus {
    background-color: var(--itemHover);
  }

  svg {
    margin-right: 10px;
  }

  span {
    margin-left: ${(props) => (props.mode !== 'active' ? '20px' : 0)};
  }
`;
