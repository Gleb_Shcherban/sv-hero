import styled from 'styled-components';

export const IconContainer = styled.div`
  position: absolute;
  top: 42px;
  right: 42px;
  justify-content: center;
  align-items: center;
  height: 24px;
  width: 24px;
  cursor: pointer;

  > img {
    width: unset;
    height: unset;
  }
`;
