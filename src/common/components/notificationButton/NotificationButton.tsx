import React from 'react';
import bellIcon from '../../assets/bell.svg';
import { IconContainer } from './NotificationButton.style';

export const NotificationButton: React.FC = () => {
  const handleNotificationsClick = () => {
    console.log('Spawn notifications panel!');
  };
  return (
    <IconContainer onClick={() => handleNotificationsClick()}>
      <img alt="" src={bellIcon} />
    </IconContainer>
  );
};
