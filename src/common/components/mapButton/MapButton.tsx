import React, { memo, useState, useCallback, useEffect } from 'react';
import GoogleMapReact from 'google-map-react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

import { httpClient } from '../../../services/httpClient/httpClient';

import { AnalyticsEndpoints } from '../../../api/endpoints';

import {
  UsersGeoApiModel,
  IUsersGeoFeatures,
  IUsersGeoFeaturesProperties,
} from '../../../api/models/analytics';

import { useIsTablet } from '../../../services/hooks/useIsTablet';

import { FSModal } from '../modal/Modal';

import {
  Container,
  SearchIcon,
  GoogleMapWrapper,
  GoogleMapCircle,
  GoogleMapContent,
  GoogleMapItemInfo,
} from './MapButton.style';

interface IGoogleMapItem {
  lat: number;
  lng: number;
  properties: IUsersGeoFeaturesProperties;
}

const defaultMapProps = {
  center: {
    lat: 38.5,
    lng: -98.0,
  },
  zoom: 4,
  fullscreenControl: false,
};

const GoogleMapItem: React.FC<IGoogleMapItem> = ({ properties }) => {
  const DescriberGoogleMapItem = Object.entries(properties).map(
    ([key, value]) => {
      return (
        <GoogleMapItemInfo key={key}>
          {key}: {value}
        </GoogleMapItemInfo>
      );
    }
  );

  return (
    <GoogleMapContent>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="button-tooltip-2">{DescriberGoogleMapItem}</Tooltip>
        }
      >
        <GoogleMapCircle rank={properties.value} />
      </OverlayTrigger>
    </GoogleMapContent>
  );
};

export const MapButton: React.FC = memo(() => {
  const [isModal, setIsModal] = useState(false);
  const [usersGeo, setUsersGeo] = useState<Array<IUsersGeoFeatures> | []>([]);

  const isTablet = useIsTablet();

  useEffect(() => {
    if (isModal) {
      httpClient
        .get<null, UsersGeoApiModel>({
          url: AnalyticsEndpoints.GetUsersGeoJson,
          requiresToken: true,
        })
        .then((response) => {
          setUsersGeo(response.features);
        });
    }
  }, [isModal]);

  const onClickModalHandler = useCallback(() => {
    setIsModal(!isModal);
  }, [isModal]);

  return (
    <>
      <FSModal modalIsOpen={isModal} onClose={onClickModalHandler}>
        <GoogleMapWrapper>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: 'AIzaSyCntMZpI6v5Hs4Tk0l70xBJisxwlRGxZSY',
            }}
            defaultCenter={defaultMapProps.center}
            defaultZoom={defaultMapProps.zoom}
            options={{ fullscreenControl: defaultMapProps.fullscreenControl }}
          >
            {(usersGeo as IUsersGeoFeatures[]).map(
              (userGeo: IUsersGeoFeatures) => {
                const lng = userGeo.geometry.coordinates[0];
                const lat = userGeo.geometry.coordinates[1];
                return (
                  <GoogleMapItem
                    key={`${lat}-${lng}`}
                    lat={lat}
                    lng={lng}
                    properties={userGeo.properties}
                  />
                );
              }
            )}
          </GoogleMapReact>
        </GoogleMapWrapper>
      </FSModal>
      <Container onClick={onClickModalHandler}>
        <SearchIcon className="fas fa-search" />
        {!isTablet && 'Influencer '}
        Map
      </Container>
    </>
  );
});
