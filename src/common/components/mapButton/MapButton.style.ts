import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: baseline;
  font-size: 12px;
  font-weight: 600;
  font-style: normal;
  text-transform: uppercase;
  color: #616161;
  cursor: pointer;
`;

export const SearchIcon = styled.i`
  margin-right: 7px;
  color: var(--lightBlue);
`;

export const GoogleMapWrapper = styled.div`
  height: 80vh;
  width: 100%;
`;

export const GoogleMapContent = styled.div``;

export const GoogleMapItemInfo = styled.div`
  text-align: left;
`;

interface IGoogleMapCircle {
  rank: number;
}
export const GoogleMapCircle = styled.div<IGoogleMapCircle>`
  position: relative;
  width: 40px;
  height: 40px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: white;
  border: 2px solid var(--lightBlue);
  border-radius: 100%;
  transform: translate(-50%, -50%);
  ${(props) => `background-color: rgba(184, 0, 0, ${props.rank / 100});`}

  :before {
    content: '';
    position: absolute;
    width: 8px;
    height: 8px;
    background-color: var(--lightBlue);
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    border-radius: 100%;
  }
`;
