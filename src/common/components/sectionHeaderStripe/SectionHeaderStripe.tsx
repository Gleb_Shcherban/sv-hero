import React from 'react';

import { SortButton } from '../sortButton/SortButton';

import { OrderDirection } from '../../constants/constants';

import { AddButton } from '../addButton/AddButton';
import {
  SimpleDropDown,
  SimpleDropDownProps,
} from '../simpleDropDown/SimpleDropDown';

import {
  SectionHeaderStripeContainer,
  PanelTitle,
  SortContainer,
  FilterContainer,
} from './SectionHeaderStripe.style';
interface SectionHeaderStripeProps {
  title: string;
  modalTitle?: string;
  orderDirection?: string;
  children?: React.ReactNode;
  sorting?: () => void;
  filterData?: SimpleDropDownProps;
}

export const SectionHeaderStripe: React.FC<SectionHeaderStripeProps> = ({
  title = '',
  modalTitle = '',
  orderDirection = OrderDirection.DESC,
  sorting,
  children,
  filterData,
}) => {
  const containerMobileClass =
    sorting && filterData ? 'mobile-stipe-container' : '';

  return (
    <SectionHeaderStripeContainer className={containerMobileClass}>
      <PanelTitle>{title}</PanelTitle>
      {sorting && (
        <SortContainer>
          <SortButton order={orderDirection} onClick={sorting} />
        </SortContainer>
      )}

      {filterData && (
        <FilterContainer>
          <SimpleDropDown
            name={filterData.name}
            value={filterData.value}
            label={filterData.label}
            menuList={filterData.menuList}
            onChange={filterData.onChange}
          />
        </FilterContainer>
      )}

      {children && <AddButton title={modalTitle}>{children}</AddButton>}
    </SectionHeaderStripeContainer>
  );
};
