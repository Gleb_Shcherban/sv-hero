import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../constants/constants';

export const SectionHeaderStripeContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  height: 58px;
  background-color: var(--sectionStripeGrey);
  box-sizing: border-box;
  padding: 0 var(--sectionPadding) 0;
  margin-bottom: 24px;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    justify-content: flex-start;
    margin-bottom: 14px;
    padding: 0 var(--mobileSectionPadding) 14px;
    white-space: nowrap;

    &.mobile-stipe-container {
      flex-wrap: wrap;
      justify-content: space-between;
      height: 100px;

      > div:nth-child(1) {
        flex: 0 0 100%;
        justify-content: center;
      }

      > div:nth-child(2) {
        margin-left: 0;
      }
    }
  }
`;

export const PanelTitle = styled.div`
  display: flex;
  justify-content: flex-start;
  color: var(--black);
  font-size: 20px;
  font-weight: 800;
`;

export const SortContainer = styled.div`
  display: flex;
  margin-left: 40px;
  justify-content: flex-start;
  font-size: 18px;
  font-weight: 600;
  cursor: pointer;
`;

export const FilterContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  margin-left: auto;
  font-size: 18px;
  font-weight: 600;
  cursor: pointer;

  > div {
    margin: 0;
    min-width: 177px;
  }
  
  > div > div:first-child {
    margin: 0;
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    margin-left: 0;
  }
`;
