import React, { useEffect, useRef } from 'react';
import { VideosSection } from '../videosSection/VideosSection';
import { useAppDispatch, useTypedSelector } from '../../../store';
import {
  getCampaignStoryVideos,
  goToSelectedPage,
} from '../../../store/slices/campaignsStoryVideosSlice';
import { defaultPagination } from '../../constants/constants';
import InfiniteScroll from 'react-infinite-scroller';
import { FlexRow } from '../../../pages/content/contentSection/ContentSection.style';

interface CampaignVideosProps {
  campaignId: string;
  isInfoIcon?: boolean;
  onClickInfoButton?: (userId: string) => void;
}

export const CampaignVideos: React.FC<CampaignVideosProps> = ({
  campaignId,
  isInfoIcon,
  onClickInfoButton,
}) => {
  const scrollParentRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const currentCampaignVideos = useTypedSelector((state) => state.campaignsStoryVideos[campaignId]);
  const {
    isLoading = false,
    error = true,
    items = [],
    page = 0,
    size = 0,
    sort = '',
    lastUpdated = '',
    totalPages = 0,
  } = currentCampaignVideos || {};

  useEffect(() => {
    dispatch(
      getCampaignStoryVideos({
        page: page,
        size: defaultPagination.videosSectionSize,
        stockVideoCampaignId: campaignId,
        sort: defaultPagination.sortByLastCreated,
      })
    );
  }, [dispatch, campaignId, page]);

  const loadMoreHandler = (page: number) => {
    if (page <= totalPages) {
      dispatch(goToSelectedPage({ page, id: campaignId }));
    }
  };

  // TODO: refactor, this function is Generic
  const hasMoreHandler = () => {
    const isNextPage = totalPages - page * size > 0;
    return Boolean(isNextPage && !isLoading);
  };

  return (
    <FlexRow ref={scrollParentRef} className="fuck-you">
      <InfiniteScroll
        useWindow={false}
        pageStart={0}
        initialLoad={false}
        loadMore={loadMoreHandler}
        hasMore={hasMoreHandler()}
        getScrollParent={() => scrollParentRef.current}
      >
        <VideosSection
          isLoading={isLoading}
          error={error}
          items={items}
          page={page}
          size={size}
          sort={sort}
          lastUpdated={lastUpdated}
          isInfoIcon={isInfoIcon}
          onClickInfoButton={onClickInfoButton}
        />
      </InfiniteScroll>
    </FlexRow>
  );
};
