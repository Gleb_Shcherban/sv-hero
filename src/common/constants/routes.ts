export enum WebsiteRoutes {
  Content = '/content',
  Monetization = '/monetization',
  Dashboard = '/dashboard',
  Influencers = '/influencers/:id?',
  Overview = '/overview',
  Rewards = '/rewards',
  UserData = '/user-data',
  Settings = '/settings',
  WebApp = '/web-app',
  OperateGame = '/operate-game',
  VenueContent = '/venue-content',
  Videos = '/videos',
  Test = '/test',
  Filters = '/filters',
  Codes = '/codes',
}

export enum MonetizationPageRoutes {
  CallToAction = '/monetization/call-to-action',
  SponsorLogos = '/monetization/sponsor-logos',
}

export enum UnprotectedRoutes {
  Login = '/login',
}

export const getRouteById = (route: string, id: string): string => {
  return route.replace(':id?', id);
};
