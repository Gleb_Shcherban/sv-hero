import { OrderDirection } from './constants/constants';

export interface DropdownOption {
  id: string;
  name: string;
}

// Assume we have only these types to be used as Cell.
// If some other appear, add it here
export type TableCell = JSX.Element | string | number;

export interface TableCellWithParams<T extends TableCell> {
  render: T;
  width?: string;
}

export interface TableRow {
  [key: string]: TableCellWithParams<any> | string;
}

export interface TableHeaderParams {
  name: string;
  sortable: boolean;
  sortOrder?: OrderDirection;
}

export interface TableContent<T extends TableRow> {
  rows: T[];
  header?: { [key: string]: TableHeaderParams };
}

/*
 TODO: Temporary solution for ANY types in project. All occurrences of
 this type should be changed to proper typing
*/
export type FixLaterAnyType = any;
