export enum SidebarStrings {
  Overview = 'Overview',
  Dashboard = 'Dashboard',
  Influencers = 'Influencers',
  WebApp = 'Web App',
  Content = 'Content',
  ContentNew = 'Content Redesigned',
  Monetization = 'Monetization',
  Rewards = 'Rewards',
  Settings = 'Settings',
  Communications = 'Communications',
  UserData = 'User Data',
  OperateGame = 'Operate Game',
  VenueContent = 'Venue Content',
  Logout = 'Logout',
  Videos = 'Videos',
  Filters = 'Filters',
  Codes = 'Codes',
}

export enum WebAppPageStrings {
  AddContent = 'Add New Content',
  UploadVideo = 'Upload Video',
  TitlePlaceholder = 'Enter Title',
  ManageCategories = 'Manage Categories',
  ManageVideos = 'Manage Videos',
  FilterByCategory = 'Filter By Category',
}

export enum MonetizationPageStrings {
  Title = 'Add Sponsors',
  CallToActionCards = 'Call-to-Action Cards',
  SponsorLogos = 'Sponsor Logos',
  UploadCTACard = 'Upload CTA Card',
  ChangeCTACard = 'Drop a new file to change Card image',
  ChangeLogo = 'Drop a new file to change Sponsor Logo',
  UploadLogo = 'Upload Logo',
  CardName = 'Card Name',
  ActionLink = 'Action Link',
  EnterName = 'Enter Name',
  EnterUrl = 'Enter URL',
  SponsorName = 'Sponsor Name',
  EnterSponsorName = 'Enter Sponsor Name',
  CreateCTACard = 'Create CTA Card',
  UpdateCTACard = 'Update CTA Card',
  DeleteCTACard = 'Delete CTA Card',
  CreateSponsor = 'Create Sponsor',
  ManageCallToActionCards = 'Manage Call-to-Action Cards',
  ManageSponsorLogos = 'Manage Sponsor Logos',
}

export enum ROISectionStrings {
  ROI = 'ROI',
  EnterAverageFollowers = 'Enter Avg # of Followers',
  Calculate = 'Calculate',
  Reset = 'Reset',
  TotalValue = 'TOTAL VALUE',
  EarnedMediaValue = 'EARNED MEDIA VALUE',
  EWOMValue = 'eWOM VALUE',
  ExtendedAudience = 'EXTENDED AUDIENCE',
  ActivatedAudience = 'Activated Audience',
  AverageFollowers = 'Average Followers',
  ContentCreated = 'Content Created',
  ContentShares = 'Content Shares',
  Views = 'VIEWS',
}

export enum ButtonDropdownStrings {
  AssignCategory = 'Assign Category',
  AssignSponsor = 'Assign Sponsor',
}

export enum LoginPageStrings {
  Title = 'Welcome to AMP dashboard',
  UserNameLabel = 'Username:',
  Email = 'Email',
  PasswordLabel = 'Password:',
  Password = 'Password',
  AuthError = 'Error logging in',
  EmailValidationError = 'Enter the correct email',
  PasswordValidationError = 'Password should be longer then 5 symbols',
}

export enum RewardsPageStrings {
  Title = 'Create Rewards For Your Fans To Shoot For',
  AddModalTitle = 'Add New Reward',
  SubTitle = 'Drive Your Goals with Rewards',
  ManageRewards = 'Manage Rewards',
  Leaderboards = 'Leaderboards',
  MaximumRewardsReached = 'The maximum limit of rewards (4) is reached.',
  DeleteRewardPrompt = 'Are you sure, that you want to remove {title} reward?',
  DeleteReward = 'Delete Reward',
  EnterRewardNamePlaceholder = 'Enter Reward Name',
  EnterRewardValuePlaceholder = '$0',
  EnterRedemptionPointsPlaceholder = '',
  Loading = 'Loading...',
  CreateReward = 'Create Reward',
  UpdateReward = 'Update Reward',
}

export enum CommunicationsPageStrings {
  Title = 'Campaigns',
  AddModalTitle = 'Add New Communication',
  Sent = 'Sent',
  DateSent = 'Date Sent',
  Sends = 'Sends',
  Opens = 'Opens',
  Clicks = 'Clicks',
  Bounces = 'Bounces',
}

export enum InfluencersPageStrings {
  Title = 'User Data',
  AddModalTitle = 'Add New User',
  Search = 'Search',
  Influencers = 'Influencers',
  RegistrationDate = 'Join date',
  Score = 'Score(10)',
  Followers = 'Followers',
  Shares = 'Shares',
  Impressions = 'Impressions',
  Engagements = 'Engagements',
  Redemptions = 'Redemptions',
  LastName = 'Last Name',
  Email = 'Email',
  PhoneNumber = 'Phone Number',
  CreatedAt = 'Created At',
  Creations = 'Creations',
  Friends = 'Friends',
  Code = 'Code',
  Points = 'Total Points',
  PointsRewardsPointsForm = 'Points',
  DescriptionRewardsPointsForm = 'Description',
  RewardsPointsPlaceholder = 'Enter points amount',
  RewardsDescriptionPlaceholder = 'Enter description',
}

export enum TableStrings {
  NoContent = 'No Content',
}

export enum HelpersStrings {
  NotAvailable = 'N/A',
}

export enum ContentPageStrings {
  PresentedBy = 'Presented by',
  ContentCreated = 'CONTENT CREATED',
  ContentViews = 'CONTENT VIEWS',
  ContentValue = 'CONTENT VALUE',
  Header = 'Content created',
  SubHeader = 'With an associated estimated value',
}

export enum CampaignsFormsStrings {
  EditFormTitle = 'Edit Campaign',
  Code = 'Campaign code',
  CodePlaceHolder = 'Enter campaign code',
  Title = 'Campaign title',
  TitlePlaceHolder = 'Enter campaign title',
  Description = 'Campaign description',
  DescriptionPlaceHolder = 'Enter campaign description',
  SponsorTitle = 'Sponsor list',
  SponsorLabel = 'Choose sponsor',
  CreateCampaigns = 'Create Campaign',
  UpdateCampaign = 'Update Campaign',
}

export enum DashboardPageStrings {
  Influencers = 'INFLUENCERS',
  Audience = 'AUDIENCE',
  Shares = 'SHARES',
  Impressions = 'IMPRESSIONS',
  Engagements = 'ENGAGEMENTS',
  Redemptions = 'REDEMPTIONS',
  InyYourCommunity = 'In your community',
  TotalFollowers = 'Total followers of your influencers',
  AssetValue = 'asset value',
  CalculatedOver = 'Calculated over 5 ye',
  UGC = 'UGC that has been created & shared',
  EstimatedUGCViews = 'Estimated views of the UGC',
  AmountCommentsLikesUGC = 'Amount of comments & likes on the UGC',
  TotalValue = 'TOTAL VALUE',
  AmountCodesForSales = 'Amount of codes used for new sales',
}

export enum VideosPageStrings {
  ModalEditVideoTitle = 'Edit video tile',
  ModalEditCategoryTitle = 'Edit category title',
  ModalEditCategoryDescription = 'Edit category description',
  SaveChanges = 'Save changes',
  DeleteVideo = 'Delete video',
  DeleteLogo = 'Delete logo',
  DeleteCategory = 'Delete category',
  ShareCopyTitle = 'Share Copy',
}

export enum FiltersPageStrings {
  DeleteFilter = 'Delete filter',
  EditFilter = 'Edit filter',
}
