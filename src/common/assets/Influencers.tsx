import * as React from 'react';

export const Influencers: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="23"
      height="17"
      fill="none"
      viewBox="0 0 23 17"
    >
      <circle cx="7.5" cy="3.5" r="3.5" fill="#fff"></circle>
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M1 16c0-3.314 2.91-6 6.5-6s6.5 2.686 6.5 6"
      ></path>
      <circle cx="16" cy="3" r="3" fill="#fff"></circle>
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M11 10.937C11.896 9.197 13.77 8 15.939 8c3.037 0 5.5 2.35 5.5 5.25"
      ></path>
    </svg>
  );
};
