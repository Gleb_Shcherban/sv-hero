import * as React from 'react';

export const CloseIcon: React.FC = () => {
  return (
    <svg width={14} height={14} viewBox="0 0 18 18" fill="none">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M17.544 15.34a1.558 1.558 0 01-2.203 2.204L9 11.203l-6.34 6.34A1.553 1.553 0 011.557 18a1.558 1.558 0 01-1.101-2.66L6.796 9 .457 2.66A1.558 1.558 0 012.658.455L9 6.797l6.34-6.34a1.558 1.558 0 012.204 2.202L11.203 9l6.34 6.34z"
        fill='var(--white)'
      />
    </svg>
  );
};

export default CloseIcon;
