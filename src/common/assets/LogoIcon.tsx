import React from 'react';

export const LogoIcon: React.FC = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="86.974"
    height="62.858"
    data-name="SV og"
    viewBox="0 0 86.974 62.858"
  >
    <path
      fill="#fff"
      d="M508.729 244.6l24.34 21.375 24.312-24.718h11.4l-35.712 37-36.4-33.655z"
      data-name="Контур 363"
      transform="translate(-487.301 -215.398)"
    ></path>
    <path
      fill="#ffce00"
      d="M620.841-50l-18.98 19.032 5.838 5.854h76.755l4.381-5.867-2.644-3.132h-69.134l7.716-7.737h42.189l3.3 3.9h12.737l-10.555-11.77z"
      data-name="Контур 364"
      transform="translate(-601.861 50)"
    ></path>
  </svg>
);
