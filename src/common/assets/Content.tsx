import * as React from 'react';

export const Content: React.FC = () => {
  return (
    <svg
      width={23}
      height={23}
      viewBox="0 0 23 23"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>{"content_icon"}</title>
      <g fill="none" fillRule="evenodd">
        <path d="M0 0h23v23H0z" />
        <path
          stroke="#FFF"
          strokeWidth={2}
          fill="none"
          d="M3 2h9.176v6.889H3z"
        />
        <path fill="#FFF" d="M2 12.111h5.588V21H2z" />
        <path
          stroke="#FFF"
          strokeWidth={2}
          fill="none"
          d="M10.824 13.111H20V20h-9.176z"
        />
        <path fill="#FFF" d="M15.412 1H21v8.889h-5.588z" />
      </g>
    </svg>
  )
};