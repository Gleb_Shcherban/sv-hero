import * as React from 'react';

export const BarsIcon: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="23"
      height="19"
      fill="none"
      viewBox="0 0 23 19"
    >
      <rect width="5" height="8" y="10" fill="#fff" rx="2"></rect>
      <rect width="5" height="12" x="7" y="6" fill="#fff" rx="2"></rect>
      <rect width="5" height="17" x="15" y="1" fill="#fff" rx="2"></rect>
    </svg>
  );
};
