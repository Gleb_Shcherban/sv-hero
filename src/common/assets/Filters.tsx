import * as React from 'react';

export const Filters: React.FC = () => {
  return (
    <svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>{"filters_icon"}</title>
      <g fill="none" fillRule="evenodd">
        <rect
          stroke="#FFF"
          strokeWidth={2}
          x={1}
          y={1}
          width={18}
          height={18}
          rx={2}
        />
        <path stroke="#FFF" strokeWidth={2} d="M2 8h16v1H2z" />
        <circle fill="#FFF" cx={10.5} cy={4.5} r={1.5} />
        <circle fill="#FFF" cx={15.5} cy={4.5} r={1.5} />
      </g>
    </svg>
  )
};