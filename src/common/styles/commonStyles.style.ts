import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../constants/constants';

interface ContentSectionContainerProps {
  largeFill?: boolean;
  solidFill?: boolean;
  minHeight?: number;
}
export const ContentSectionContainer = styled.div<ContentSectionContainerProps>`
  display: flex;
  flex-direction: column;
  position: relative;
  align-items: center;
  justify-content: center;
  width: 100%;
  min-height: ${(props) =>
    props.minHeight ? `${props.minHeight}px` : '550px'};
  margin-bottom: 50px;
  ${(props) =>
    props.largeFill
      ? 'background: linear-gradient(var(--pageHeaderBgr) 550px, var(--contentBackgroundColor) 0)'
      : 'background: linear-gradient(var(--pageHeaderBgr) 443px, var(--contentBackgroundColor) 0)'};

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    min-height: fit-content;
    margin-bottom: 0;
  }
`;

export const KindaBook = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 53px;
  border-radius: 14px 14px 0 0;
  width: 800px;
  height: 500px;
  background-color: var(--white);

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    width: 100%;
    height: fit-content;
  }
`;

export const KindaBookHeader = styled.div`
  display: flex;
  align-items: flex-end;
  padding: 12px;
  box-sizing: border-box;
  font-weight: 800;
  font-size: 20px;
`;

export const CardsContainer = styled.div`
  display: flex;
  box-sizing: border-box;
  padding: 0 36px;
  margin-top: 24px;
  justify-content: space-between;
  width: 100%;
  height: 100%;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    margin-bottom: 20px;
    padding: 0;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
  }
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  width: 320px;
  min-height: 200px;
  box-sizing: border-box;
  padding: 0 14px;
  box-shadow: -8px -6px 13px -13px var(--pageHeaderBgr),
    8px -6px 13px -13px var(--pageHeaderBgr);

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    :nth-child(2) {
      margin-top: 20px;
    }
  }
`;

export const CardHeader = styled.div`
  display: flex;
  align-items: flex-end;
  height: 32px;
  margin-bottom: 21px;
  font-weight: bold;
  font-size: 16px;
`;

export const SubmitButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 48px;
`;

export const BodySection = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  width: 100%;
  box-sizing: border-box;
  top: 52px;
`;

export const TableWrapper = styled.div`
  margin: 0 28px 10px;
  min-height: 650px;
  width: 918px;
  border-radius: var(--commonBorderRadius) var(--commonBorderRadius) 0 0;
  background-color: var(--white);
  box-shadow: 0px -18px 13px -2px var(--pageHeaderBgr);

  > div > div:first-child {
    padding-left: 25px;
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    width: 100%;
  }
`;

export const CheckMark = styled.i`
  color: var(--activeColor);
  font-size: 12px;
`;

export const Search = styled.i`
  color: var(--white);
  font-size: 12px;
`;

export const ModalFormWrapper = styled.div`
  form > div {
    margin-top: 30px;
  }
`;

export const PageContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  width: 100%;
  padding: 80px 50px 30px;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    padding: 30px 15px;
  }
`;

export const Title = styled.div`
  font-size: 12px;
  color: var(--titleGrey);
  text-transform: uppercase;
  font-weight: 600;
`;

export const Text = styled.div`
  font-size: 14px;
  color: var(--textGrey);
  font-weight: 500;
`;

export const PlayVideoImg = styled.img`
  position: absolute;
  width: 50px;
  height: 50px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  pointer-events: none;
`;

export const CtaIcon = styled.i`
  display: inline-block;
  padding: 13px;
  margin-left: 24px;
  color: #222;
  font-size: 18px;
  border-radius: 50%;
  background-color: #fff;
  cursor: pointer;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.25);
  transition: all 0.2s ease-in-out;
  &:hover {
    color: #fff;
    background-color: #222;
  }
`;
