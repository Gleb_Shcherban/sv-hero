import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import { ToastProvider } from 'react-toast-notifications';

import store from './store';
import { AuthHandler } from './common/components/AuthHandler';
import { Sidebar } from './common/components/sidebar/Sidebar';
import {
  MonetizationPageRoutes,
  UnprotectedRoutes,
  WebsiteRoutes,
} from './common/constants/routes';
import { Login } from './pages/login/Login';
import { Dashboard } from './pages/dashboard/Dashboard';
import { Influencers } from './pages/influencers/Influencers';
import { Content } from './pages/content/Content';
import { Monetization } from './pages/monetization/Monetization';
import { Rewards } from './pages/rewards/Rewards';
import { VideoTool } from './pages/videoTool';
import { TestPage } from './pages/testPage';
import { FiltersPage } from './pages/filters';
import { CodesPage } from './pages/codes/Codes';

import { RootContainer, AppContainer, ContentWrapper } from './App.style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Settings } from './pages/settings/Settings';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <ToastProvider autoDismiss autoDismissTimeout={3500}>
      <Router>
        <AuthHandler>
          <RootContainer>
            <AppContainer>
              <Switch>
                <Route exact path={UnprotectedRoutes.Login}>
                  <Login />
                </Route>

                <Route>
                  <Sidebar />
                  <ContentWrapper className="content-wrapper">
                    <Switch>
                      <Route exact path={WebsiteRoutes.Dashboard}>
                        <Dashboard />
                      </Route>
                      <Route path={WebsiteRoutes.Influencers}>
                        <Influencers />
                      </Route>
                      <Route exact path={WebsiteRoutes.Content}>
                        <Content />
                      </Route>
                      <Route path={WebsiteRoutes.Monetization}>
                        <Switch>
                          <Route
                            exact
                            path={MonetizationPageRoutes.CallToAction}
                          >
                            <Monetization
                              page={MonetizationPageRoutes.CallToAction}
                            />
                          </Route>
                          <Route
                            exact
                            path={MonetizationPageRoutes.SponsorLogos}
                          >
                            <Monetization
                              page={MonetizationPageRoutes.SponsorLogos}
                            />
                          </Route>
                          <Redirect to={MonetizationPageRoutes.CallToAction} />
                        </Switch>
                      </Route>
                      <Route exact path={WebsiteRoutes.Rewards}>
                        <Rewards />
                      </Route>
                      <Route exact path={WebsiteRoutes.Videos}>
                        <VideoTool />
                      </Route>
                      <Route exact path={WebsiteRoutes.Filters}>
                        <FiltersPage />
                      </Route>
                      <Route exact path={WebsiteRoutes.Codes}>
                        <CodesPage />
                      </Route>
                      <Route exact path={WebsiteRoutes.Settings}>
                        <Settings />
                      </Route>
                      <Route exact path={WebsiteRoutes.Test}>
                        <TestPage />
                      </Route>
                      <Redirect to={WebsiteRoutes.Dashboard} />
                    </Switch>
                  </ContentWrapper>
                </Route>
              </Switch>
            </AppContainer>
          </RootContainer>
        </AuthHandler>
      </Router>
      </ToastProvider>
    </Provider>
  );
};

export default App;
