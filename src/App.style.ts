import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from './common/constants/constants';

export const RootContainer = styled.div`
  display: flex;
  width: 100%;
  min-height: 100vh;
  justify-content: center;
  background-color: var(--contentBackgroundColor);
`;
export const AppContainer = styled.div`
  display: flex;
  width: 100%;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    .content-wrapper {
      padding-top: 70px;
    }
  }
`;
export const ContentWrapper = styled.div`
  padding-bottom: 60px;
  position: relative;
  width: 100%;
  height: 100vh;
  overflow: auto;
  background-color: var(--contentBackgroundColor);
`;
