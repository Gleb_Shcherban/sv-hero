import React from 'react';
import { TableCellWithParams, TableContent, TableRow } from '../../common/commonTypes';
import { HelpersStrings, InfluencersPageStrings } from '../../common/localization/en';

import { getLocaleNumberValue, getStringValue } from '../utilities';

import { EditButton } from '../../common/components/table/EditButton';
import { InfluencerApiModel } from '../../api/models/users';
import { SponsorCardApiModel } from '../../api/models/sponsorCards';
import { SponsorsApiModel } from '../../api/models/sponsors';
import { VideosApiModel } from '../../api/models/videos';
import { CodesApiModel } from '../../api/models/codes';
import { ImageComponent } from '../../common/components/table/TableImage';
import { EditCTACard } from '../../pages/monetization/editItemForms/EditCTACard';
import { EditLogo } from '../../pages/monetization/editItemForms/EditLogo';
import { ImagesText, DefaultImage } from '../../common/components/table/CommonTableNew.style';

const getLocalDateString = (val: string | null): string => {
  if (val) {
    const dateObject = new Date(val);
    return dateObject.toLocaleDateString('en-US').replace(/\//g, '-');
  } else {
    return HelpersStrings.NotAvailable;
  }
};

interface MapperProps<T> {
  items: T[];
}

export interface InfluencersDataContent extends TableRow {
  id: string;
  firstName: TableCellWithParams<JSX.Element>;
  createdAt: TableCellWithParams<string>;
  // score: TableCellWithParams<string>;
  audience: TableCellWithParams<string>;
  shares: TableCellWithParams<string>;
  impressions: TableCellWithParams<string>;
  engagement: TableCellWithParams<string>;
  redemptions: TableCellWithParams<string>;
  code: TableCellWithParams<string>;
  rewardPoints: TableCellWithParams<string>;
}

interface ManageCallToActionCardsContent extends TableRow {
  image: TableCellWithParams<JSX.Element>;
  cardName: TableCellWithParams<string>;
  edit: TableCellWithParams<JSX.Element>;
}

interface ManageSponsorLogosContent extends TableRow {
  image: TableCellWithParams<JSX.Element>;
  cardName: TableCellWithParams<string>;
  edit: TableCellWithParams<JSX.Element>;
}

interface ManageVideosContent extends TableRow {
  name: TableCellWithParams<string>;
  actionLabel: TableCellWithParams<string>;
  edit: TableCellWithParams<JSX.Element>;
}

interface ManageCodesContent extends TableRow {
  image: TableCellWithParams<JSX.Element>;
  code: TableCellWithParams<string>;
}

export const createUserDataTableContent = (
  props: MapperProps<InfluencerApiModel>
): TableContent<InfluencersDataContent> => {
  const { items } = props;
  const tableContent: TableContent<InfluencersDataContent> = {
    header: {
      firstName: {
        name: InfluencersPageStrings.Influencers,
        sortable: true,
      },
      createdAt: {
        name: InfluencersPageStrings.RegistrationDate,
        sortable: true,
      },
      // score: {
      //   name: InfluencersPageStrings.Score,
      //   sortable: false,
      // },
      audience: {
        name: InfluencersPageStrings.Followers,
        sortable: true,
      },
      shares: {
        name: InfluencersPageStrings.Shares,
        sortable: true,
      },
      impressions: {
        name: InfluencersPageStrings.Impressions,
        sortable: true,
      },
      engagement: {
        name: InfluencersPageStrings.Engagements,
        sortable: true,
      },
      redemptions: {
        name: InfluencersPageStrings.Redemptions,
        sortable: false,
      },
      code: {
        name: InfluencersPageStrings.Code,
        sortable: false,
      },
      rewardPoints: {
        name: InfluencersPageStrings.Points,
        sortable: true,
      },
    },
    rows: [],
  };

  if (items.length === 0) {
    return tableContent;
  }

  tableContent.rows = items.map((item) => {
    return {
      id: item.id,
      firstName: {
        render: (
          <span style={{ display: 'flex', alignItems: 'center' }}>
            {item.photoUrl ? (
              <ImageComponent imageRef={item.photoUrl} alt="Sponsor card" />
            ) : (
              <DefaultImage />
            )}
            <ImagesText>{getStringValue(item.firstName)}</ImagesText>
          </span>
        ),
      },
      createdAt: {
        render: getLocalDateString(item.createdAt),
      },
      // score: {
      //   render: randomIntFromInterval(1, 10) /*getStringValue(item.score)*/,
      // },
      audience: {
        render: getLocaleNumberValue(item.audience),
      },
      shares: {
        render: getLocaleNumberValue(item.shares),
      },
      impressions: {
        render: getLocaleNumberValue(item.impressions),
      },
      engagement: {
        render: getLocaleNumberValue(item.engagements),
      },
      redemptions: {
        render: getLocaleNumberValue(item.redemptions),
      },
      code: {
        render: getStringValue(item.referralCode),
      },
      rewardPoints: {
        render: getLocaleNumberValue(item.rewardPoints),
      },
    };
  });
  return tableContent;
};

export const createSponsorCardsTableContent = (
  props: MapperProps<SponsorCardApiModel>
): TableContent<ManageCallToActionCardsContent> => {
  const { items } = props;
  const tableContent: TableContent<ManageCallToActionCardsContent> = {
    rows: [],
  };

  if (items.length === 0) {
    return tableContent;
  }

  tableContent.rows = items.map((item) => {
    return {
      image: {
        width: '300px',
        render: <ImageComponent imageRef={item.imageUri} alt="Sponsor card" />,
      },
      cardName: {
        render: getStringValue(item.name),
      },
      edit: {
        render: (
          <EditButton>
            <EditCTACard id={item.id} />
          </EditButton>
        ),
      },
    };
  });
  return tableContent;
};

export const createSponsorLogosTableContent = (
  props: MapperProps<SponsorsApiModel>
): TableContent<ManageSponsorLogosContent> => {
  const { items } = props;
  const tableContent: TableContent<ManageSponsorLogosContent> = {
    rows: [],
  };

  if (items.length === 0) {
    return tableContent;
  }

  tableContent.rows = items.map((item) => {
    return {
      image: {
        width: '300px',
        render: <ImageComponent imageRef={item.imageUri} alt="Sponsor logo" />,
      },
      cardName: {
        render: getStringValue(item.name),
      },
      edit: {
        render: (
          <EditButton>
            <EditLogo id={item.id} />
          </EditButton>
        ),
      },
    };
  });
  return tableContent;
};

export const createVideosTableContent = (
  props: MapperProps<VideosApiModel>
): TableContent<ManageVideosContent> => {
  const { items } = props;
  const tableContent: TableContent<ManageVideosContent> = {
    rows: [],
  };

  if (items.length === 0) {
    return tableContent;
  }

  tableContent.rows = items.map((item) => {
    return {
      name: {
        render: getStringValue(item.name),
      },
      actionLabel: {
        render: getStringValue(item.actionLabel),
      },
      edit: {
        render: (
          <EditButton>
            <EditLogo id={item.id} />
          </EditButton>
        ),
      },
    };
  });
  return tableContent;
};

export const createCodesTableContent = (
  props: MapperProps<CodesApiModel>
): TableContent<ManageCodesContent> => {
  const { items } = props;
  const tableContent: TableContent<ManageCodesContent> = {
    rows: [],
  };

  if (items.length === 0) {
    return tableContent;
  }

  tableContent.rows = items.map((item) => {
    return {
      image: {
        width: '50px',
        render: <DefaultImage />,
      },
      code: {
        render: getStringValue(item.code),
      },
    };
  });
  return tableContent;
};
