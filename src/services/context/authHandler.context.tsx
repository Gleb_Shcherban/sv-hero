import React from 'react';

type ContextValue = boolean;

export const AuthHandlerContext = React.createContext<ContextValue>(false);
