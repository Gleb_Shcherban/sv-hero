import {
  UploadMediaFileRequest,
  UploadMediaFileResponse,
} from '../../api/models/common';

export interface IHttpClientRequestParameters<T> {
  url: string;
  requiresToken: boolean;
  payload?: T;
  params?: T;
}

export interface IHttpClient {
  login<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  logout(): Promise<any>;
  get<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  post<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  patch<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  put<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  delete<T, U>(parameters: IHttpClientRequestParameters<T>): Promise<U>;
  uploadFile(
    parameters: IHttpClientRequestParameters<UploadMediaFileRequest>
  ): Promise<UploadMediaFileResponse>;
  handleExpiredJwt(): Promise<any>;
  getAccessToken(): string | null;
  getRefreshToken(): string | null;
  isLoggedIn(): boolean;
}
