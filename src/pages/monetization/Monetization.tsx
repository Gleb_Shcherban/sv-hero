import React from 'react';
import { TopSection } from './topSection/TopSection';
import { TableSection } from './tableSection/TableSection';
import { MonetizationPageRoutes } from '../../common/constants/routes';

interface MonetizationProps {
  page: MonetizationPageRoutes;
}

export const Monetization: React.FC<MonetizationProps> = ({ page }) => {
  return (
    <>
      <TopSection page={page} />
      <TableSection page={page} />
    </>
  );
};
