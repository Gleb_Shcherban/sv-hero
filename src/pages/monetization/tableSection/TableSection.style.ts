import styled from 'styled-components';

export const TableSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: var(--white);
  width: 100%;
`;
