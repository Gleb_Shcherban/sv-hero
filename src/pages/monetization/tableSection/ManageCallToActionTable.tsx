import React, { useEffect } from 'react';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { CommonTable } from '../../../common/components/table/CommonTable';
import {
  getSponsorCards,
  goToSelectedPage,
} from '../../../store/slices/sponsorCardsSlice';
import { createSponsorCardsTableContent } from '../../../services/helpers/tableMappers';
import {
  convertApiPageToFrontEndPage,
  convertFrontEndPageToApiPage,
} from '../../../services/utilities';

export const ManageCallToActionTable: React.FC = () => {
  const dispatch = useAppDispatch();
  const {
    isLoading,
    error,
    totalItems,
    items,
    totalPages,
    page,
    sort,
    size,
    lastUpdated,
  } = useTypedSelector((state) => state.sponsorCards);

  useEffect(() => {
    dispatch(getSponsorCards({ page, sort, size }));
  }, [dispatch, page, sort, lastUpdated, size]);

  const manageCallToActionCardsContent = createSponsorCardsTableContent({
    items,
  });

  const onGoToPage = (targetPage: number) => {
    dispatch(goToSelectedPage(convertFrontEndPageToApiPage(targetPage)));
  };

  return (
    <CommonTable
      content={manageCallToActionCardsContent}
      totalItems={totalItems}
      totalPages={totalPages}
      page={convertApiPageToFrontEndPage(page)}
      noContent={error}
      isLoading={isLoading}
      goToPage={onGoToPage}
    />
  );
};
