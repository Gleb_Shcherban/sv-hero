import React from 'react';

import { useAppDispatch, useTypedSelector } from '../../../store';

import { setSponsorCardsSorting } from '../../../store/slices/sponsorCardsSlice';
import { setSponsorsSorting } from '../../../store/slices/sponsorsSlice';

import { MonetizationPageRoutes } from '../../../common/constants/routes';
import { SectionHeaderStripe } from '../../../common/components/sectionHeaderStripe/SectionHeaderStripe';
import { MonetizationPageStrings } from '../../../common/localization/en';
import { TableSectionContainer } from './TableSection.style';
import { ManageCallToActionTable } from './ManageCallToActionTable';
import { ManageSponsorsLogosTable } from './ManageSponsorLogosTable';

interface TableSectionProps {
  page: MonetizationPageRoutes;
}

export const TableSection: React.FC<TableSectionProps> = ({ page }) => {
  const dispatch = useAppDispatch();
  const { sort: sponsorCardsSort } = useTypedSelector(
    (state) => state.sponsorCards
  );
  const { sort: sponsorsSort } = useTypedSelector((state) => state.sponsors);

  const getPageData = () => {
    switch (page) {
      case MonetizationPageRoutes.CallToAction:
        return {
          title: MonetizationPageStrings.ManageCallToActionCards,
          setSortingAction: setSponsorCardsSorting,
          sort: sponsorCardsSort,
        };
      case MonetizationPageRoutes.SponsorLogos:
        return {
          title: MonetizationPageStrings.ManageSponsorLogos,
          setSortingAction: setSponsorsSorting,
          sort: sponsorsSort,
        };
    }
  };

  const renderTable = () => {
    switch (page) {
      case MonetizationPageRoutes.CallToAction:
        return <ManageCallToActionTable />;
      case MonetizationPageRoutes.SponsorLogos:
        return <ManageSponsorsLogosTable />;
    }
  };

  const setSortingHandler = () => {
    dispatch(getPageData().setSortingAction('name'));
  };

  return (
    <TableSectionContainer>
      <SectionHeaderStripe
        sorting={setSortingHandler}
        title={getPageData().title}
        orderDirection={getPageData().sort}
      />
      {renderTable()}
    </TableSectionContainer>
  );
};
