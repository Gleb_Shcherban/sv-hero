import React, { useEffect } from 'react';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { CommonTable } from '../../../common/components/table/CommonTable';
import {
  getSponsors,
  goToSelectedPage,
} from '../../../store/slices/sponsorsSlice';
import { createSponsorLogosTableContent } from '../../../services/helpers/tableMappers';
import {
  convertApiPageToFrontEndPage,
  convertFrontEndPageToApiPage,
} from '../../../services/utilities';

export const ManageSponsorsLogosTable: React.FC = () => {
  const dispatch = useAppDispatch();
  const {
    isLoading,
    error,
    totalItems,
    items,
    totalPages,
    page,
    sort,
    size,
    lastUpdated,
  } = useTypedSelector((state) => state.sponsors);

  useEffect(() => {
    dispatch(getSponsors({ page, sort, size }));
  }, [dispatch, page, sort, size, lastUpdated]);

  const manageSponsorLogosContent = createSponsorLogosTableContent({
    items,
  });

  const onGoToPage = (targetPage: number) => {
    dispatch(goToSelectedPage(convertFrontEndPageToApiPage(targetPage)));
  };

  return (
    <CommonTable
      content={manageSponsorLogosContent}
      totalItems={totalItems}
      totalPages={totalPages}
      page={convertApiPageToFrontEndPage(page)}
      noContent={error}
      isLoading={isLoading}
      goToPage={onGoToPage}
    />
  );
};
