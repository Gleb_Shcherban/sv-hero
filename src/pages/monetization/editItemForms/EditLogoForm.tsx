import React from 'react';
import {
  IValuesSponsorLogos,
  validationSchemaSponsorLogos,
} from '../topSection/topSectionFormsHelper';
import { CardHeader } from '../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../common/localization/en';
import { InputField } from '../../../common/components/inputField/InputField';
import { ButtonsContainer } from './EditForm.style';
import { Button } from 'react-bootstrap';
import { useFormik } from 'formik';
import * as Yup from 'yup';

interface EditLogoFormProps {
  onSubmit: (values: IValuesSponsorLogos) => void;
  initialValues: IValuesSponsorLogos;
  isUpdating: boolean;
}

export const EditLogoForm: React.FC<EditLogoFormProps> = ({
  onSubmit,
  initialValues,
  isUpdating,
}) => {
  const EditLogoFormik = useFormik({
    initialValues,
    enableReinitialize: true,
    validateOnChange: false,
    validateOnBlur: false,
    validationSchema: Yup.object().shape<IValuesSponsorLogos>(
      validationSchemaSponsorLogos
    ),
    onSubmit: (values) => {
      onSubmit(values);
    },
  });
  const { handleSubmit, values, handleChange, errors } = EditLogoFormik;

  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  return (
    <form style={{ width: '100%' }} onSubmit={handleSubmit}>
      <CardHeader style={{ marginTop: '20px' }}>
        {MonetizationPageStrings.SponsorName}
      </CardHeader>
      <InputField
        name="contentTitle"
        value={values.contentTitle}
        onChange={handleChange}
        placeholder={MonetizationPageStrings.EnterUrl}
        errorText={errors.contentTitle}
      />
      <ButtonsContainer>
        <Button type="submit" variant={getButtonVariant('primary')}>
          {MonetizationPageStrings.UpdateCTACard}
        </Button>
      </ButtonsContainer>
    </form>
  );
};
