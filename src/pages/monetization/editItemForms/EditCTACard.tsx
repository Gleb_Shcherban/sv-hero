import React, { useEffect, useState } from 'react';
import {
  EditCTACardContainer,
  FormContainer,
  UploaderContainer,
} from './EditForm.style';
import { httpClient } from '../../../services/httpClient/httpClient';
import { getApiUrlForId, SponsorCardsEndpoints } from '../../../api/endpoints';
import {
  GetSponsorCardByIdRequest,
  PatchCTACardApiModel,
  SponsorCardApiModel,
} from '../../../api/models/sponsorCards';
import { updateTable as updateSponsorCardsTable } from '../../../store/slices/sponsorCardsSlice';
import { CardHeader } from '../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../common/localization/en';
import {
  initialValuesCallToAction,
  IValuesCallToAction,
} from '../topSection/topSectionFormsHelper';
import { Spinner } from '../../../common/assets/Spinner';
import { useDragAndDropReducer } from '../../../common/components/dragAndDrop/DragAndDropState';
import { DragAndDrop } from '../../../common/components/dragAndDrop/DragAndDrop';
import { uploadMediaFile } from '../../../common/components/dragAndDrop/DragAndDropHelpers';
import { MediaFilePrefix } from '../../../api/models/common';
import { useAppDispatch } from '../../../store';
import { EditCTAForm } from './EditCTAForm';

interface EditCTACardProps {
  id: string;
  setIsModal?: (isModal: boolean) => void;
}
export const EditCTACard: React.FC<EditCTACardProps> = ({ id, setIsModal }) => {
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [isUpdating, setIsUpdating] = useState(false);
  const [initialValues, setInitialValues] = useState(initialValuesCallToAction);
  const [uploadCTACardData, uploadCTACardDispatch] = useDragAndDropReducer();
  const requestUrl = getApiUrlForId(
    SponsorCardsEndpoints.GetSponsorCardById,
    id
  );

  useEffect(() => {
    httpClient
      .get<GetSponsorCardByIdRequest, SponsorCardApiModel>({
        url: requestUrl,
        requiresToken: true,
      })
      .then((result) => {
        setIsLoading(false);
        setInitialValues({
          cardName: result.name,
          cardUrl: result.ctaUrl,
        });
      });
  }, [requestUrl]);

  useEffect(() => {
    uploadCTACardData.fileList.forEach((_file) => {
      if (!_file.isUploading && !_file.isUploaded && !_file.isUploadError) {
        uploadMediaFile(_file, MediaFilePrefix.Sponsors, uploadCTACardDispatch);
      }
    });
  }, [uploadCTACardData.fileList, uploadCTACardDispatch]);

  const deleteCard = () => {
    setIsUpdating(true);

    httpClient
      .delete<never, SponsorCardApiModel>({
        url: getApiUrlForId(SponsorCardsEndpoints.DeleteSponsorCard, id),
        requiresToken: true,
      })
      .then(() => {
        dispatch(updateSponsorCardsTable());
        setIsModal!(false);
      })
      .catch((error) => {
        setIsUpdating(false);
        console.log('Error during card update', error);
      });
  };

  const updateCardData = (values: IValuesCallToAction) => {
    if ((!values.cardName && !values.cardUrl) || isUpdating) {
      return;
    }

    setIsUpdating(true);

    const payload: PatchCTACardApiModel = {
      name: values.cardName,
      ctaUrl: values.cardUrl,
    };

    if (uploadCTACardData.fileList[0]?.remoteUrl) {
      payload.imageUri = uploadCTACardData.fileList[0]?.remoteUrl;
    }

    httpClient
      .patch<PatchCTACardApiModel, SponsorCardApiModel>({
        url: getApiUrlForId(SponsorCardsEndpoints.PatchSponsorCard, id),
        requiresToken: true,
        payload,
      })
      .then(() => {
        dispatch(updateSponsorCardsTable());
        setIsModal!(false);
      })
      .catch((error) => {
        setIsUpdating(false);
        console.log('Error during card update', error);
      });
  };

  if (isLoading) {
    return (
      <EditCTACardContainer>
        <Spinner color="var(--spinnerColor)" />
      </EditCTACardContainer>
    );
  }
  return (
    <EditCTACardContainer>
      <FormContainer>
        <EditCTAForm
          onSubmit={updateCardData}
          initialValues={initialValues}
          isUpdating={isUpdating}
          onDelete={deleteCard}
        />
      </FormContainer>
      <UploaderContainer>
        <CardHeader>{MonetizationPageStrings.ChangeCTACard}</CardHeader>
        <DragAndDrop
          resendMediaFile={(file) =>
            uploadMediaFile(
              file,
              MediaFilePrefix.Sponsors,
              uploadCTACardDispatch
            )
          }
          data={uploadCTACardData}
          dispatch={uploadCTACardDispatch}
        />
      </UploaderContainer>
    </EditCTACardContainer>
  );
};
