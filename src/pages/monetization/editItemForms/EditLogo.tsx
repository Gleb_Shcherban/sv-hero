import React, { useEffect, useState } from 'react';
import {
  EditCTACardContainer,
  FormContainer,
  UploaderContainer,
} from './EditForm.style';
import { httpClient } from '../../../services/httpClient/httpClient';
import { getApiUrlForId, SponsorsEndpoints } from '../../../api/endpoints';
import { SponsorCardApiModel } from '../../../api/models/sponsorCards';
import { updateTable as updateSponsorsTable } from '../../../store/slices/sponsorsSlice';
import { CardHeader } from '../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../common/localization/en';
import {
  initialValuesSponsorLogos,
  IValuesSponsorLogos,
} from '../topSection/topSectionFormsHelper';
import { Spinner } from '../../../common/assets/Spinner';
import { useDragAndDropReducer } from '../../../common/components/dragAndDrop/DragAndDropState';
import { DragAndDrop } from '../../../common/components/dragAndDrop/DragAndDrop';
import { uploadMediaFile } from '../../../common/components/dragAndDrop/DragAndDropHelpers';
import { MediaFilePrefix } from '../../../api/models/common';
import { useAppDispatch } from '../../../store';
import {
  SponsorsApiModel,
  PatchApmlfiTemplateApiModel,
} from '../../../api/models/sponsors';
import { EditLogoForm } from './EditLogoForm';

interface EditLogoProps {
  id: string;
  setIsModal?: (isModal: boolean) => void;
}
export const EditLogo: React.FC<EditLogoProps> = ({ id, setIsModal }) => {
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [isUpdating, setIsUpdating] = useState(false);
  const [initialValues, setInitialValues] = useState(initialValuesSponsorLogos);
  const [uploadLogoData, uploadLogoDispatch] = useDragAndDropReducer();
  const requestUrl = getApiUrlForId(SponsorsEndpoints.GetSponsorById, id);

  useEffect(() => {
    httpClient
      .get<PatchApmlfiTemplateApiModel, SponsorsApiModel>({
        url: requestUrl,
        requiresToken: true,
      })
      .then((result) => {
        setIsLoading(false);
        setInitialValues({
          contentTitle: result.name,
        });
      });
  }, [requestUrl]);

  useEffect(() => {
    uploadLogoData.fileList.forEach((_file) => {
      if (!_file.isUploading && !_file.isUploaded && !_file.isUploadError) {
        uploadMediaFile(_file, MediaFilePrefix.Client, uploadLogoDispatch);
      }
    });
  }, [uploadLogoData.fileList, uploadLogoDispatch]);

  const updateLogoData = (values: IValuesSponsorLogos) => {
    if (!values.contentTitle || isUpdating) {
      return;
    }

    setIsUpdating(true);

    const payload: PatchApmlfiTemplateApiModel = {
      name: values.contentTitle,
    };

    if (uploadLogoData.fileList[0]?.remoteUrl) {
      payload.imageUri = uploadLogoData.fileList[0]?.remoteUrl;
    }

    httpClient
      .patch<PatchApmlfiTemplateApiModel, SponsorCardApiModel>({
        url: getApiUrlForId(SponsorsEndpoints.PatchAmplfiTemplate, id),
        requiresToken: true,
        payload,
      })
      .then(() => {
        dispatch(updateSponsorsTable());
        setIsModal!(false);
      })
      .catch((error) => {
        setIsUpdating(false);
        console.log('Error during card update', error);
      });
  };

  if (isLoading) {
    return (
      <EditCTACardContainer>
        <Spinner color="var(--spinnerColor)" />
      </EditCTACardContainer>
    );
  }
  return (
    <EditCTACardContainer>
      <FormContainer>
        <EditLogoForm
          isUpdating={isUpdating}
          onSubmit={updateLogoData}
          initialValues={initialValues}
        />
      </FormContainer>
      <UploaderContainer>
        <CardHeader>{MonetizationPageStrings.ChangeLogo}</CardHeader>
        <DragAndDrop
          resendMediaFile={(file) =>
            uploadMediaFile(file, MediaFilePrefix.Client, uploadLogoDispatch)
          }
          data={uploadLogoData}
          dispatch={uploadLogoDispatch}
        />
      </UploaderContainer>
    </EditCTACardContainer>
  );
};
