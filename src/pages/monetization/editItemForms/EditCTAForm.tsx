import React from 'react';
import {
  IValuesCallToAction,
  validationSchemaCallToAction,
} from '../topSection/topSectionFormsHelper';
import { CardHeader } from '../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../common/localization/en';
import { InputField } from '../../../common/components/inputField/InputField';
import { ButtonsContainer } from './EditForm.style';
import { Button } from 'react-bootstrap';
import { useFormik } from 'formik';
import * as Yup from 'yup';

interface EditCTAFormProps {
  onSubmit: (values: IValuesCallToAction) => void;
  initialValues: IValuesCallToAction;
  isUpdating: boolean;
  onDelete: () => void;
}

export const EditCTAForm: React.FC<EditCTAFormProps> = ({
  onSubmit,
  initialValues,
  isUpdating,
  onDelete,
}) => {
  const EditCTAFormik = useFormik({
    initialValues,
    enableReinitialize: true,
    validateOnChange: false,
    validateOnBlur: false,
    validationSchema: Yup.object().shape<IValuesCallToAction>(
      validationSchemaCallToAction
    ),
    onSubmit: (values) => {
      onSubmit(values);
    },
  });
  const { handleSubmit, values, handleChange, errors } = EditCTAFormik;

  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const deleteCard = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    onDelete();
  };

  return (
    <form style={{ width: '100%' }} onSubmit={handleSubmit}>
      <CardHeader style={{ marginTop: '20px' }}>
        {MonetizationPageStrings.CardName}
      </CardHeader>
      <InputField
        name="cardName"
        value={values.cardName}
        onChange={handleChange}
        placeholder={MonetizationPageStrings.EnterUrl}
        errorText={errors.cardUrl}
      />
      <CardHeader style={{ marginTop: '20px' }}>
        {MonetizationPageStrings.ActionLink}
      </CardHeader>
      <InputField
        name="cardUrl"
        value={values.cardUrl}
        onChange={handleChange}
        placeholder={MonetizationPageStrings.EnterUrl}
        errorText={errors.cardUrl}
      />
      <ButtonsContainer>
        <Button type="submit" variant={getButtonVariant('primary')}>
          {MonetizationPageStrings.UpdateCTACard}
        </Button>
        <Button variant={getButtonVariant('danger')} onClick={deleteCard}>
          {MonetizationPageStrings.DeleteCTACard}
        </Button>
      </ButtonsContainer>
    </form>
  );
};
