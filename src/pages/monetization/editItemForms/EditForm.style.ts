import styled from 'styled-components';

export const EditCTACardContainer = styled.div`
  display: flex;
  height: 400px;
  width: 100%;
`;

export const FormContainer = styled.div`
  display: flex;
  flex: 1;
`;

export const UploaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 0 24px;
  box-sizing: border-box;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  margin-top: 60px;
  justify-content: space-between;
`;
