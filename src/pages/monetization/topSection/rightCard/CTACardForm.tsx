import React from 'react';
import { FormikProps } from 'formik';
import {
  Card,
  CardHeader,
  SubmitButtonContainer,
} from '../../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../../common/localization/en';
import { InputField } from '../../../../common/components/inputField/InputField';
import { Button } from 'react-bootstrap';

import { IValuesCallToAction } from '../topSectionFormsHelper';

interface ICTACardForm {
  formik: FormikProps<IValuesCallToAction>;
}

export const CTACardForm: React.FC<ICTACardForm> = ({ formik }) => {
  const { handleSubmit, values, handleChange, errors } = formik;

  return (
    <Card>
      <CardHeader>{MonetizationPageStrings.CardName}</CardHeader>
      <form aria-label="Callback" onSubmit={handleSubmit}>
        <InputField
          name="cardName"
          value={values.cardName}
          onChange={handleChange}
          placeholder={MonetizationPageStrings.EnterName}
          errorText={errors.cardName}
        />
        <CardHeader style={{ marginTop: '20px' }}>
          {MonetizationPageStrings.ActionLink}
        </CardHeader>
        <InputField
          name="cardUrl"
          value={values.cardUrl}
          onChange={handleChange}
          placeholder={MonetizationPageStrings.EnterUrl}
          errorText={errors.cardUrl}
        />
        <SubmitButtonContainer>
          <Button type="submit" variant="danger">
            {MonetizationPageStrings.CreateCTACard}
          </Button>
        </SubmitButtonContainer>
      </form>
    </Card>
  );
};
