import React from 'react';
import { FormikProps } from 'formik';
import {
  Card,
  CardHeader,
  SubmitButtonContainer,
} from '../../../../common/styles/commonStyles.style';
import {
  // ButtonDropdownStrings,
  MonetizationPageStrings,
} from '../../../../common/localization/en';
import { InputField } from '../../../../common/components/inputField/InputField';
import { Button } from 'react-bootstrap';
// import { SimpleDropDown } from '../../../../common/components/simpleDropDown/SimpleDropDown';
// import { DropdownOption } from '../../../../common/commonTypes';

import { IValuesSponsorLogos } from '../topSectionFormsHelper';

/*const tempOptions: DropdownOption[] = [
  {
    id: '1',
    name: 'Sponsored by AT&T',
  },
  {
    id: '2',
    name: 'Game Day Fashion',
  },
  {
    id: '3',
    name: 'Top 5 Fan Creations',
  },
];*/

interface ISponsorLogoForm {
  formik: FormikProps<IValuesSponsorLogos>;
}

export const SponsorLogoForm: React.FC<ISponsorLogoForm> = (props) => {
  const {
    handleSubmit,
    values,
    handleChange,
    errors,
    // setFieldValue,
  } = props.formik;

  return (
    <Card>
      <CardHeader>{MonetizationPageStrings.SponsorName}</CardHeader>
      <form aria-label="Callback" onSubmit={handleSubmit}>
        <InputField
          name="contentTitle"
          value={values.contentTitle}
          onChange={handleChange}
          placeholder={MonetizationPageStrings.EnterSponsorName}
          errorText={errors.contentTitle}
        />
        {/*<SimpleDropDown
          name="category"
          title={ButtonDropdownStrings.AssignCategory}
          label={ButtonDropdownStrings.AssignCategory}
          menuList={tempOptions}
          onChange={setFieldValue}
          errorText={errors.category}
        />*/}
        <SubmitButtonContainer>
          <Button type="submit" variant="danger">
            {MonetizationPageStrings.CreateSponsor}
          </Button>
        </SubmitButtonContainer>
      </form>
    </Card>
  );
};
