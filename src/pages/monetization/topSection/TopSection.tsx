import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { MonetizationPageRoutes } from '../../../common/constants/routes';

import { MonetizationPageStrings } from '../../../common/localization/en';

import {
  CardsContainer,
  ContentSectionContainer,
  KindaBook,
  KindaBookHeader,
} from '../../../common/styles/commonStyles.style';
import {
  KindaBookHeaderTabContainer,
  KindaBookHeaderWithTabs,
} from './TopSection.style';

import { UploadCTACard } from './leftCard/UploadCTACard';
import { UploadLogo } from './leftCard/UploadLogo';
import { CTACardForm } from './rightCard/CTACardForm';
import { SponsorLogoForm } from './rightCard/SponsorLogoForm';
import {
  DNDActionTypes,
  useDragAndDropReducer,
} from '../../../common/components/dragAndDrop/DragAndDropState';
import { httpClient } from '../../../services/httpClient/httpClient';
import {
  CreateCTACardApiModel,
  SponsorCardApiModel,
} from '../../../api/models/sponsorCards';
import {
  SponsorCardsEndpoints,
  SponsorsEndpoints,
} from '../../../api/endpoints';
import { useAppDispatch } from '../../../store';
import { updateTable as updateSponsorCardsTable } from '../../../store/slices/sponsorCardsSlice';
import { updateTable as updateSponsorsTable } from '../../../store/slices/sponsorsSlice';
import {
  CreateSponsorApiModel,
  SponsorsApiModel,
} from '../../../api/models/sponsors';
import {
  initialValuesCallToAction,
  initialValuesSponsorLogos,
  IValuesCallToAction,
  IValuesSponsorLogos,
  validationSchemaCallToAction,
  validationSchemaSponsorLogos,
} from './topSectionFormsHelper';

interface TopSectionProps {
  page: MonetizationPageRoutes;
}

export const TopSection: React.FC<TopSectionProps> = ({ page }) => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const { pathname } = useLocation();
  const [uploadCTACardData, uploadCTACardDispatch] = useDragAndDropReducer();
  const [uploadLogoData, uploadLogoDispatch] = useDragAndDropReducer();

  const onTabClick = (tabRedirect: MonetizationPageRoutes) => {
    history.push(tabRedirect);
  };

  const createCallToActionCard = (values: IValuesCallToAction) => {
    if (
      values.cardName &&
      values.cardUrl &&
      uploadCTACardData.fileList[0]?.remoteUrl
    ) {
      httpClient
        .post<CreateCTACardApiModel, SponsorCardApiModel>({
          url: SponsorCardsEndpoints.CreateSponsorCard,
          payload: {
            name: values.cardName,
            ctaUrl: values.cardUrl,
            imageUri: uploadCTACardData.fileList[0].remoteUrl,
          },
          requiresToken: true,
        })
        .then(() => {
          uploadCTACardDispatch({ type: DNDActionTypes.RESET_STATE });
          dispatch(updateSponsorCardsTable());
          CTAFormik.resetForm();
        })
        .catch((e) => console.log('err', e));
    }
  };

  const createLogo = (values: IValuesSponsorLogos) => {
    if (values.contentTitle && uploadLogoData.fileList[0]?.remoteUrl) {
      httpClient
        .post<CreateSponsorApiModel, SponsorsApiModel>({
          url: SponsorsEndpoints.CreateSponsor,
          payload: {
            name: values.contentTitle,
            imageUri: uploadLogoData.fileList[0].remoteUrl,
          },
          requiresToken: true,
        })
        .then(() => {
          uploadLogoDispatch({ type: DNDActionTypes.RESET_STATE });
          dispatch(updateSponsorsTable());
          SponsorFormik.resetForm();
        })
        .catch((e) => console.log('err', e));
    }
  };

  const renderLeftCard = () => {
    switch (page) {
      case MonetizationPageRoutes.CallToAction: {
        return (
          <UploadCTACard
            dndData={uploadCTACardData}
            dndDispatch={uploadCTACardDispatch}
          />
        );
      }
      case MonetizationPageRoutes.SponsorLogos: {
        return (
          <UploadLogo
            dndData={uploadLogoData}
            dndDispatch={uploadLogoDispatch}
          />
        );
      }
    }
  };

  const CTAFormik = useFormik({
    initialValues: initialValuesCallToAction,
    validateOnChange: false,
    validateOnBlur: false,
    validationSchema: Yup.object().shape<IValuesCallToAction>(
      validationSchemaCallToAction
    ),
    onSubmit: (values) => {
      createCallToActionCard(values);
    },
  });

  const SponsorFormik = useFormik({
    initialValues: initialValuesSponsorLogos,
    validateOnChange: false,
    validateOnBlur: false,
    validationSchema: Yup.object().shape<IValuesSponsorLogos>(
      validationSchemaSponsorLogos
    ),
    onSubmit: (values) => {
      createLogo(values);
    },
  });

  const renderRightCard = () => {
    switch (page) {
      case MonetizationPageRoutes.CallToAction: {
        return <CTACardForm formik={CTAFormik} />;
      }
      case MonetizationPageRoutes.SponsorLogos: {
        return <SponsorLogoForm formik={SponsorFormik} />;
      }
    }
  };

  return (
    <ContentSectionContainer>
      <KindaBook>
        <KindaBookHeader>{MonetizationPageStrings.Title}</KindaBookHeader>
        <KindaBookHeaderWithTabs>
          <KindaBookHeaderTabContainer
            active={pathname === MonetizationPageRoutes.CallToAction}
            onClick={() => onTabClick(MonetizationPageRoutes.CallToAction)}
          >
            {MonetizationPageStrings.CallToActionCards}
          </KindaBookHeaderTabContainer>
          <KindaBookHeaderTabContainer
            active={pathname === MonetizationPageRoutes.SponsorLogos}
            onClick={() => onTabClick(MonetizationPageRoutes.SponsorLogos)}
          >
            {MonetizationPageStrings.SponsorLogos}
          </KindaBookHeaderTabContainer>
        </KindaBookHeaderWithTabs>
        <CardsContainer>
          {renderLeftCard()}
          {renderRightCard()}
        </CardsContainer>
      </KindaBook>
    </ContentSectionContainer>
  );
};
