import * as Yup from 'yup';

export interface IValuesCallToAction {
  cardName: string;
  cardUrl: string;
}
export const initialValuesCallToAction: IValuesCallToAction = {
  cardName: '',
  cardUrl: '',
};
export const validationSchemaCallToAction: Yup.ObjectSchemaDefinition<IValuesCallToAction> = {
  cardName: Yup.string()
    .max(200, 'max length 200 symbols')
    .required('Required'),
  cardUrl: Yup.string().required('Required'),
};

export interface IValuesSponsorLogos {
  contentTitle: string;
}
export const initialValuesSponsorLogos: IValuesSponsorLogos = {
  contentTitle: '',
};
export const validationSchemaSponsorLogos: Yup.ObjectSchemaDefinition<IValuesSponsorLogos> = {
  contentTitle: Yup.string()
    .max(200, 'max length 200 symbols')
    .required('Required'),
  // category: Yup.string().required('Required'),
};
