import styled from 'styled-components';
import { KindaBookHeader } from '../../../common/styles/commonStyles.style';

export const KindaBookHeaderWithTabs = styled(KindaBookHeader)`
  padding: 0;
  border-bottom: 1px solid var(--grey);
  justify-content: center;
`;

interface KindaBookHeaderTabContainerStyleProps {
  active?: boolean;
}
export const KindaBookHeaderTabContainer = styled.div<
  KindaBookHeaderTabContainerStyleProps
>`
  display: flex;
  position: relative;
  width: 250px;
  height: 36px;
  font-size: 16px;
  font-weight: ${(props) => (props.active ? 'bold' : 'normal')};
  justify-content: center;
  align-items: flex-start;
  cursor: pointer;

  ${(props) =>
    props.active &&
    '&:after { content: ""; position: absolute; bottom: 0; border-radius: 6px 6px 0 0; display: block; width: 42px; height: 6px; background-color: var(--activeColor)}'}
`;
