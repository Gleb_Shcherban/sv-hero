import React, { useEffect } from 'react';
import { Card, CardHeader } from '../../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../../common/localization/en';
import {
  DNDAction,
  IDragAndDropState,
} from '../../../../common/components/dragAndDrop/DragAndDropState';
import { DragAndDrop } from '../../../../common/components/dragAndDrop/DragAndDrop';
import { MediaFilePrefix } from '../../../../api/models/common';
import { uploadMediaFile } from '../../../../common/components/dragAndDrop/DragAndDropHelpers';

interface UploadLogoProps {
  dndData: IDragAndDropState;
  dndDispatch: React.Dispatch<DNDAction>;
}

export const UploadLogo: React.FC<UploadLogoProps> = ({
  dndData,
  dndDispatch,
}) => {
  useEffect(() => {
    dndData.fileList.forEach((_file) => {
      if (!_file.isUploading && !_file.isUploaded && !_file.isUploadError) {
        uploadMediaFile(_file, MediaFilePrefix.Client, dndDispatch);
      }
    });
  }, [dndData.fileList, dndDispatch]);

  return (
    <Card>
      <CardHeader>{MonetizationPageStrings.UploadLogo}</CardHeader>
      <DragAndDrop
        resendMediaFile={(file) =>
          uploadMediaFile(file, MediaFilePrefix.Client, dndDispatch)
        }
        data={dndData}
        dispatch={dndDispatch}
        multiple
      />
    </Card>
  );
};
