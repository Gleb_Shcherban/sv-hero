import React, { useEffect } from 'react';
import { Card, CardHeader } from '../../../../common/styles/commonStyles.style';
import { MonetizationPageStrings } from '../../../../common/localization/en';
import { DragAndDrop } from '../../../../common/components/dragAndDrop/DragAndDrop';
import {
  DNDAction,
  IDragAndDropState,
} from '../../../../common/components/dragAndDrop/DragAndDropState';
import { MediaFilePrefix } from '../../../../api/models/common';
import { uploadMediaFile } from '../../../../common/components/dragAndDrop/DragAndDropHelpers';

interface UploadCTACardProps {
  dndData: IDragAndDropState;
  dndDispatch: React.Dispatch<DNDAction>;
}

export const UploadCTACard: React.FC<UploadCTACardProps> = ({
  dndData,
  dndDispatch,
}) => {
  useEffect(() => {
    dndData.fileList.forEach((_file) => {
      if (!_file.isUploading && !_file.isUploaded && !_file.isUploadError) {
        uploadMediaFile(_file, MediaFilePrefix.Sponsors, dndDispatch);
      }
    });
  }, [dndData.fileList, dndDispatch]);

  return (
    <Card>
      <CardHeader>{MonetizationPageStrings.UploadCTACard}</CardHeader>
      <DragAndDrop
        resendMediaFile={(file) =>
          uploadMediaFile(file, MediaFilePrefix.Sponsors, dndDispatch)
        }
        data={dndData}
        dispatch={dndDispatch}
      />
    </Card>
  );
};
