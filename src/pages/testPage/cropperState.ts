import React from 'react';

export enum CropperSteps {
  BASE = 'BASE',
  JUMBO = 'JUMBO',
  CIRCULAR = 'CIRCULAR',
  SEQUENTIAL = 'SEQUENTIAL',
}

export enum CropperActionTypes {
  SET_AREA_SIZE = "SET_AREA_SIZE",
  SET_CROP_SIZE = "SET_CROP_SIZE",
  NEXT_STEP = "NEXT_STEP",
  PREV_STEP = "PREV_STEP",
  SET_STEP = "SET_STEP",
  SET_POSITION = 'SET_POSITION',
  RESET = "RESET",
}

const INITIAL_AREA = { width: 374, height: 552 };
const INITIAL_SIZES = { width: 374, height: 552, x: 0, y: 0 };
const INITIAL_POSITION = { x: 0, y: 0 };

export type CropArea = { width: number, height: number };
export type CropSizes = { width: number, height: number, x: number, y: number };
export type CropPosition = { x: number, y: number };

export interface CropperState {
  step: CropperSteps;
  areas: {
    BASE: CropArea,
    JUMBO: CropArea,
    CIRCULAR: CropArea,
    SEQUENTIAL: CropArea
  },
  sizes: {
    BASE: CropSizes,
    JUMBO: CropSizes,
    CIRCULAR: CropSizes,
    SEQUENTIAL: CropSizes,
  },
  positions: {
    BASE: CropPosition,
    JUMBO: CropPosition,
    CIRCULAR: CropPosition,
    SEQUENTIAL: CropPosition,
  }
}

export const initialCropperState: CropperState = {
  step: CropperSteps.BASE,
  areas: {
    BASE: INITIAL_AREA,
    JUMBO: INITIAL_AREA,
    CIRCULAR: INITIAL_AREA,
    SEQUENTIAL: INITIAL_AREA
  },
  sizes: {
    BASE: INITIAL_SIZES,
    JUMBO: INITIAL_SIZES,
    CIRCULAR: INITIAL_SIZES,
    SEQUENTIAL: INITIAL_SIZES,
  },
  positions: {
    BASE: INITIAL_POSITION,
    JUMBO: INITIAL_POSITION,
    CIRCULAR: INITIAL_POSITION,
    SEQUENTIAL: INITIAL_POSITION,
  },
}

const nextStepHandler = (step: CropperSteps): CropperSteps => {
  switch (step) {
    case CropperSteps.BASE:
      return CropperSteps.JUMBO;
    case CropperSteps.JUMBO:
      return CropperSteps.CIRCULAR;
    case CropperSteps.CIRCULAR:
      return CropperSteps.SEQUENTIAL
    default:
      return step;
  }
}

const prevStepHandler = (step: CropperSteps): CropperSteps => {
  switch (step) {
    case CropperSteps.SEQUENTIAL:
      return CropperSteps.CIRCULAR;
    case CropperSteps.CIRCULAR:
      return CropperSteps.JUMBO;
    case CropperSteps.JUMBO:
      return CropperSteps.BASE;
    default:
      return step;
  }
}

export type CropperAction =
  | { type: CropperActionTypes.SET_AREA_SIZE, payload: CropArea }
  | { type: CropperActionTypes.SET_CROP_SIZE, payload: CropSizes}
  | { type: CropperActionTypes.NEXT_STEP }
  | { type: CropperActionTypes.PREV_STEP}
  | { type: CropperActionTypes.SET_STEP, payload: CropperSteps }
  | { type: CropperActionTypes.SET_POSITION, payload: CropPosition }
  | { type: CropperActionTypes.RESET };

export const useCropperReducer = (): [
  CropperState,
  React.Dispatch<CropperAction>
] => {
  const cropperReducer = (state: CropperState, action: CropperAction): CropperState => {
    switch (action.type) {
      case CropperActionTypes.SET_AREA_SIZE:
        return {
          ...state,
          areas: {
            ...state.areas,
            [state.step]: action.payload
          }
        }
      case CropperActionTypes.SET_CROP_SIZE:
        return {
          ...state,
          sizes: {
            ...state.sizes,
            [state.step]: action.payload
          }
        }
      case CropperActionTypes.NEXT_STEP:
        return {
          ...state,
          step: nextStepHandler(state.step)
        }
      case CropperActionTypes.PREV_STEP:
        return {
          ...state,
          step: prevStepHandler(state.step)
        }
      case CropperActionTypes.SET_STEP:
        return {
          ...state,
          step: action.payload
        }
      case CropperActionTypes.SET_POSITION:
        return {
          ...state,
          positions: {
            ...state.positions,
            [state.step]: action.payload
          }
        }
      case CropperActionTypes.RESET:
        return initialCropperState
      default:
        return state
    }
  };

  const [data, dispatch] = React.useReducer(cropperReducer, initialCropperState);
  return [data, dispatch];
}
