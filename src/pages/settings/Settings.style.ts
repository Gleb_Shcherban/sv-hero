import styled from 'styled-components';

export const Column = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Row = styled.div`
  display: flex;
`;

export const StepsRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ProfileSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--white);
  border-radius: var(--commonBorderRadius);
  padding: 16px;
  height: 280px;
  box-sizing: border-box;
`;

export const SectionHeader = styled.div`
  font-size: 14px;
  color: var(--darkGreyColor);
  font-weight: 500;
  margin-bottom: 10px;
`;

export const InputsColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 260px;
`;

export const InputRow = styled.div`
  display: flex;
  height: 60px;
  align-items: center;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const InfoField = styled(InputRow)`
  height: 36px;
`;

export const InputName = styled.div`
  display: flex;
  color: var(--grey);
  flex-shrink: 0;
  width: 80px;
`;

export const TextValue = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const InfoFieldInputName = styled(InputName)`
  width: 210px;
`;

export const ResetPasswordButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: var(--white);
  width: 130px;
  height: 32px;
  border-radius: 19px;
  background-color: var(--spinnerColor);
  cursor: pointer;
`;

export const ProfilePictureContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 232px;
  font-size: 12px;
  color: var(--grey);
  margin-left: 58px;
`;

export const ProfilePicture = styled.img`
  height: 103px;
  width: 103px;
  border-radius: var(--commonBorderRadius);
  background-color: var(--grey);
  margin-left: 6px;
`;

export const ProfilePicturePlaceholder = styled.div`
  min-height: 103px;
  min-width: 103px;
  border-radius: var(--commonBorderRadius);
  background-color: var(--grey);
  margin-left: 6px;
`;

export const EditVideoScreenColumn = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  width: 210px;
  margin-bottom: 30px;

  > div {
    font-size: 12px;
    color: var(--grey);
    margin-bottom: 6px;
  }
`;

export const EditVideoScreenHorizontalColumn = styled(EditVideoScreenColumn)`
  width: 510px;
`;

export const StepImage = styled.img`
  background-color: var(--black);
  width: 180px;
  height: auto;
  border-radius: var(--commonBorderRadius);
  object-fit: contain;
`;

export const VideoContainer = styled.video`
  background-color: var(--black);
  width: 180px;
  height: auto;
  border-radius: var(--commonBorderRadius);
`;

export const StepImageHorizontal = styled(StepImage)`
  width: 480px;
`;

export const EditButton = styled.i`
  display: inline-block;
  position: absolute;
  top: 0;
  right: 15px;
  padding: 13px;
  margin-left: 24px;
  color: #222;
  font-size: 18px;
  border-radius: 50%;
  background-color: #fff;
  cursor: pointer;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.25);
  transition: all 0.2s ease-in-out;
  &:hover {
    color: #fff;
    background-color: #222;
  }
`;

export const ProfileEditButton = styled(EditButton)`
  top: 12px;
  right: -10px;
`;

export const InstagramIcon = styled.img`
  height: 64px;
  width: 64px;
`;
export const InstagramLoginImage = styled.img`
  height: 260px;
  width: auto;
  margin-top: -50px;
`;
