import React, { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import {
  CreateOrUpdateVenueAttributesRequest,
  CreateOrUpdateVenueAttributesResponse,
  ModalUploadImageType,
  PatchVenue,
  VenueAPIModel,
  VenueAttributeName,
} from '../../../api/models/venue';
import {
  ConfirmButtonContainer,
  UploaderContainer,
  UploadImageModal,
} from './ChangeWebappImage.style';
import { DragAndDrop } from '../../../common/components/dragAndDrop/DragAndDrop';
import { useDragAndDropReducer } from '../../../common/components/dragAndDrop/DragAndDropState';
import { uploadMediaFile } from '../../../common/components/dragAndDrop/DragAndDropHelpers';
import { MediaFilePrefix } from '../../../api/models/common';
import { VideosPageStrings } from '../../../common/localization/en';
import { httpClient } from '../../../services/httpClient/httpClient';
import { VenueEndpoints } from '../../../api/endpoints';

interface ChangeWebappImageProps {
  imageType?: ModalUploadImageType;
  imageAttribute: VenueAttributeName;
  closeModal: () => void;
  triggerRefresh: () => void;
}

export const ChangeWebappImage: React.FC<ChangeWebappImageProps> = ({
  imageType = ModalUploadImageType.VenueAttribute,
  imageAttribute,
  closeModal,
  triggerRefresh,
}) => {
  const [webappImageData, webAppImageDispatch] = useDragAndDropReducer();

  useEffect(() => {
    webappImageData.fileList.forEach((_file) => {
      if (!_file.isUploading && !_file.isUploaded && !_file.isUploadError) {
        uploadMediaFile(_file, MediaFilePrefix.Client, webAppImageDispatch);
      }
    });
  }, [webappImageData.fileList, webAppImageDispatch]);

  const getButtonVariant = (variant: string) => {
    return webappImageData.fileList[0]?.isUploaded ? variant : 'dark';
  };

  const deleteLogo = () => {
    httpClient
      .patch<PatchVenue, VenueAPIModel>({
        url: VenueEndpoints.Venue,
        requiresToken: true,
        payload: { logo: '' },
      })
      .then(() => {
        triggerRefresh();
        closeModal();
      })
      .catch((e) => {
        triggerRefresh();
        console.log('Error, while uploading image', e);
      });
  };

  const updateImage = () => {
    if (
      !webappImageData.fileList[0] ||
      !webappImageData.fileList[0].remoteUrl
    ) {
      return;
    }
    switch (imageType) {
      case ModalUploadImageType.VenueAttribute: {
        httpClient
          .post<
            CreateOrUpdateVenueAttributesRequest,
            CreateOrUpdateVenueAttributesResponse
          >({
            url: VenueEndpoints.CreateOrUpdateVenueAttributes,
            requiresToken: true,
            payload: [
              {
                name: imageAttribute,
                value: webappImageData.fileList[0].remoteUrl,
              },
            ],
          })
          .then(() => {
            triggerRefresh();
            closeModal();
          })
          .catch((e) => {
            triggerRefresh();
            console.log('Error, while uploading image', e);
          });
        break;
      }
      case ModalUploadImageType.Logo: {
        httpClient
          .patch<PatchVenue, VenueAPIModel>({
            url: VenueEndpoints.Venue,
            requiresToken: true,
            payload: { logo: webappImageData.fileList[0].remoteUrl },
          })
          .then(() => {
            triggerRefresh();
            closeModal();
          })
          .catch((e) => {
            triggerRefresh();
            console.log('Error, while uploading image', e);
          });
        break;
      }
    }
  };

  return (
    <UploadImageModal>
      <UploaderContainer>
        <DragAndDrop
          data={webappImageData}
          dispatch={webAppImageDispatch}
          resendMediaFile={(chosenFile) =>
            uploadMediaFile(
              chosenFile,
              MediaFilePrefix.Client,
              webAppImageDispatch
            )
          }
        />
      </UploaderContainer>
      <ConfirmButtonContainer>
        <Button variant={getButtonVariant('primary')} onClick={updateImage}>
          {VideosPageStrings.SaveChanges}
        </Button>
        {imageType === ModalUploadImageType.Logo && (
          <Button
            style={{ marginLeft: '40px' }}
            variant={'danger'}
            onClick={deleteLogo}
          >
            {VideosPageStrings.DeleteLogo}
          </Button>
        )}
      </ConfirmButtonContainer>
    </UploadImageModal>
  );
};
