import styled from 'styled-components';

export const UploadImageModal = styled.div`
  display: flex;
`;

export const UploaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 350px;
`;

export const ConfirmButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const EditProfileModal = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
