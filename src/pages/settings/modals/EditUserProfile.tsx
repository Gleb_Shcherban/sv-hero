import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import {
  ConfirmButtonContainer,
  UploadImageModal,
} from './ChangeWebappImage.style';
import { VideosPageStrings } from '../../../common/localization/en';
import { httpClient } from '../../../services/httpClient/httpClient';
import { AuthEndpoints } from '../../../api/endpoints';
import { PatchMe, MeApiModel } from '../../../api/models/auth';
import { InputName, InputRow, InputsColumn } from '../Settings.style';
import { InputField } from '../../../common/components/inputField/InputField';
import { useTypedSelector } from '../../../store';

interface EditUserProfileProps {
  closeModal: () => void;
  triggerRefresh: () => void;
}
export const EditUserProfile: React.FC<EditUserProfileProps> = ({
  closeModal,
  triggerRefresh,
}) => {
  const { firstName, lastName } = useTypedSelector((state) => state.me);
  const [firstNameLocal, setFirstNameLocal] = useState(firstName);
  const [lastNameLocal, setLastNameLocal] = useState(lastName);

  const saveChanges = () => {
    if (!firstNameLocal || !lastNameLocal) {
      return;
    }
    const payload: PatchMe = {
      firstName: firstNameLocal,
      lastName: lastNameLocal,
    };
    httpClient
      .patch<PatchMe, MeApiModel>({
        url: AuthEndpoints.PatchProfile,
        requiresToken: true,
        payload,
      })
      .then(() => {
        triggerRefresh();
        closeModal();
      })
      .catch((e) => {
        triggerRefresh();
        console.log('Error, while uploading image', e);
      });
  };

  return (
    <UploadImageModal>
      <InputsColumn style={{ width: '50%' }}>
        <InputRow>
          <InputName>First Name</InputName>
          <InputField
            name="firstName"
            onChange={(e) => {
              setFirstNameLocal(e.target.value);
            }}
            value={firstNameLocal}
          />
        </InputRow>
        <InputRow>
          <InputName>Last Name</InputName>
          <InputField
            name="lastName"
            onChange={(e) => {
              setLastNameLocal(e.target.value);
            }}
            value={lastNameLocal}
          />
        </InputRow>
      </InputsColumn>
      <ConfirmButtonContainer style={{ width: '50%' }}>
        <Button variant={'primary'} onClick={saveChanges}>
          {VideosPageStrings.SaveChanges}
        </Button>
      </ConfirmButtonContainer>
    </UploadImageModal>
  );
};
