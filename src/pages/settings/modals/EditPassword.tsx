import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import {
  ConfirmButtonContainer,
  UploadImageModal,
} from './ChangeWebappImage.style';
import { VideosPageStrings } from '../../../common/localization/en';
import { httpClient } from '../../../services/httpClient/httpClient';
import { AuthEndpoints } from '../../../api/endpoints';
import { MeApiModel, UpdatePassword } from '../../../api/models/auth';
import { InputName, InputRow, InputsColumn } from '../Settings.style';
import { InputField } from '../../../common/components/inputField/InputField';

interface EditPasswordProps {
  closeModal: () => void;
  triggerRefresh: () => void;
}
export const EditPassword: React.FC<EditPasswordProps> = ({
  closeModal,
  triggerRefresh,
}) => {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const saveChanges = () => {
    if (!oldPassword || !newPassword) {
      return;
    }
    httpClient
      .put<UpdatePassword, MeApiModel>({
        url: AuthEndpoints.UpdatePassword,
        requiresToken: true,
        payload: { newPassword, oldPassword },
      })
      .then(() => {
        triggerRefresh();
        closeModal();
      })
      .catch((e) => {
        triggerRefresh();
        console.log('Error, while uploading image', e);
      });
  };

  return (
    <UploadImageModal>
      <InputsColumn style={{ width: '50%' }}>
        <InputRow>
          <InputName style={{ width: '120px' }}>Old Password</InputName>
          <InputField
            name="oldPassword"
            onChange={(e) => {
              setOldPassword(e.target.value);
            }}
            value={oldPassword}
          />
        </InputRow>
        <InputRow>
          <InputName style={{ width: '120px' }}>New Password</InputName>
          <InputField
            name="newPassword"
            onChange={(e) => {
              setNewPassword(e.target.value);
            }}
            value={newPassword}
          />
        </InputRow>
      </InputsColumn>
      <ConfirmButtonContainer style={{ width: '50%' }}>
        <Button variant={'primary'} onClick={saveChanges}>
          {VideosPageStrings.SaveChanges}
        </Button>
      </ConfirmButtonContainer>
    </UploadImageModal>
  );
};
