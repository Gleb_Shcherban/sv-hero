import React from 'react';
import {
  InstagramIcon,
  InstagramLoginImage,
  ProfileSectionContainer,
  Row,
} from '../Settings.style';
import instagramIcon from '../tempAssets/instagramIcon.png';
import instagramRegistration from '../tempAssets/instagramRegistration.png';

export const SocialLoginSection: React.FC = () => {
  return (
    <ProfileSectionContainer style={{ width: '400px' }}>
      <Row>
        <InstagramIcon alt="" src={instagramIcon} />
        <InstagramLoginImage
          style={{ marginTop: '0' }}
          alt=""
          src={instagramRegistration}
        />
      </Row>
    </ProfileSectionContainer>
  );
};
