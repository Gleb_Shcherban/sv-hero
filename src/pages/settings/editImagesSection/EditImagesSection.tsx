import React, { useState } from 'react';
import homeHeroImage from '../tempAssets/firstImage.png';
import rewardsPopupImage from '../tempAssets/secondImage.png';
import videoProcessingImage from '../tempAssets/thirdImage.png';
import desktopHero from '../tempAssets/desktop-hero.png';
import desktopImage2 from '../tempAssets/desktopImage2.png';
import {
  Column,
  EditButton,
  EditVideoScreenColumn,
  EditVideoScreenHorizontalColumn,
  StepImage,
  StepImageHorizontal,
  StepsRow,
} from '../Settings.style';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { getAttributes } from '../../../store/slices/venueSlice';
import { Spinner } from '../../../common/assets/Spinner';
import { FSModal } from '../../../common/components/modal/Modal';
import { VenueAttributeName } from '../../../api/models/venue';
import { ChangeWebappImage } from '../modals/ChangeWebappImage';
import { Video } from '../../../common/components/video/Video';

interface EditImageModalData {
  isOpen: boolean;
  imageType: VenueAttributeName;
}

export const EditImagesSection: React.FC = () => {
  const dispatch = useAppDispatch();
  const { venueAttributes } = useTypedSelector((state) => state.venue);
  const [modalData, setModalData] = useState<EditImageModalData>({
    isOpen: false,
    imageType: VenueAttributeName.HomeHero,
  });

  const openEditImageModal = (imageType: VenueAttributeName) => {
    setModalData({ imageType, isOpen: true });
  };

  const closeModal = () => {
    setModalData({ isOpen: false, imageType: modalData.imageType });
  };

  if (venueAttributes.isLoading) {
    return (
      <StepsRow>
        <Column>
          <Spinner color="var(--spinnerColor)" />
        </Column>
      </StepsRow>
    );
  }

  return (
    <StepsRow>
      <Column>
        <EditVideoScreenColumn>
          <div>Home Hero</div>
          <StepImage
            alt=""
            src={venueAttributes.attributes[VenueAttributeName.HomeHero] || homeHeroImage}
          />
          <EditButton
            className="fa fa-pen"
            onClick={() => openEditImageModal(VenueAttributeName.HomeHero)}
          />
        </EditVideoScreenColumn>
        <EditVideoScreenColumn>
          <div>Rewards pop-up</div>
          <StepImage
            alt=""
            src={venueAttributes.attributes[VenueAttributeName.RewardsPopUp] || rewardsPopupImage}
          />
          <EditButton
            className="fa fa-pen"
            onClick={() => openEditImageModal(VenueAttributeName.RewardsPopUp)}
          />
        </EditVideoScreenColumn>
      </Column>
      <EditVideoScreenColumn>
        <div>Video processing screen</div>
        <StepImage
          alt=""
          src={
            venueAttributes.attributes[VenueAttributeName.VideoProcessingScreen] ||
            videoProcessingImage
          }
        />
        <EditButton
          className="fa fa-pen"
          onClick={() => openEditImageModal(VenueAttributeName.VideoProcessingScreen)}
        />
      </EditVideoScreenColumn>
      <Column>
        <EditVideoScreenHorizontalColumn>
          <div>Desktop Hero</div>
          <StepImageHorizontal
            alt=""
            src={venueAttributes.attributes[VenueAttributeName.DesktopHero] || desktopHero}
          />
          <EditButton
            className="fa fa-pen"
            onClick={() => openEditImageModal(VenueAttributeName.DesktopHero)}
          />
        </EditVideoScreenHorizontalColumn>
        <EditVideoScreenColumn>
          <div>Desktop image 2</div>
          <StepImage
            alt=""
            src={venueAttributes.attributes[VenueAttributeName.DesktopImage2] || desktopImage2}
          />
          <EditButton
            className="fa fa-pen"
            onClick={() => openEditImageModal(VenueAttributeName.DesktopImage2)}
          />
        </EditVideoScreenColumn>
      </Column>
      <EditVideoScreenColumn>
        <div>How to Create Video</div>
        {/*<VideoContainer src={venueAttributes.attributes[VenueAttributeName.HowToCreateVideo]} />*/}
        <Video
          videoUrl={venueAttributes.attributes[VenueAttributeName.HowToCreateVideo]}
          userId={'123'}
          isInfoIcon={false}
          isDownloadVideo={false}
        />
        <EditButton
          className="fa fa-pen"
          onClick={() => openEditImageModal(VenueAttributeName.HowToCreateVideo)}
        />
      </EditVideoScreenColumn>
      <FSModal modalIsOpen={modalData.isOpen} onClose={closeModal}>
        <ChangeWebappImage
          imageAttribute={modalData.imageType}
          closeModal={closeModal}
          triggerRefresh={() => dispatch(getAttributes())}
        />
      </FSModal>
    </StepsRow>
  );
};
