import React, { useState } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { PageContentContainer } from '../../common/styles/commonStyles.style';
import {
  Column,
  InfoField,
  InfoFieldInputName,
  InputName,
  InputRow,
  InputsColumn,
  ProfileEditButton,
  ProfilePicture,
  ProfilePicturePlaceholder,
  ProfileSectionContainer,
  ResetPasswordButton,
  Row,
  SectionHeader,
  TextValue,
} from './Settings.style';
import { InputField } from '../../common/components/inputField/InputField';
import { EditImagesSection } from './editImagesSection/EditImagesSection';
import { SocialLoginSection } from './socialLoginSection/SocialLoginSection';
import { useAppDispatch, useTypedSelector } from '../../store';
import { FSModal } from '../../common/components/modal/Modal';
import { EditUserProfile } from './modals/EditUserProfile';
import { getMe } from '../../store/slices/meSlice';
import { EditPassword } from './modals/EditPassword';
import { ModalUploadImageType, VenueAttributeName } from '../../api/models/venue';
import { ChangeWebappImage } from './modals/ChangeWebappImage';
import { getVenue } from '../../store/slices/venueSlice';

enum ModalTypes {
  Profile = 'profile',
  Password = 'password',
  VenueLogo = 'venueLogo',
  None = '',
}

// TODO: REFACTOR THIS PAGE ASAP, IT WAS CREATED IN A HUGE RUSH
export const Settings: React.FC = () => {
  const dispatch = useAppDispatch();
  const { firstName, lastName, email } = useTypedSelector((state) => state.me);
  const {
    venueAttributes,
    venue: { organizationName, logo },
  } = useTypedSelector((state) => state.venue);

  const refreshData = () => {
    dispatch(getMe());
    dispatch(getVenue());
  };

  const [modalData, setModalData] = useState<ModalTypes>(ModalTypes.None);

  const resetPasswordHandler = () => {
    setModalData(ModalTypes.Password);
  };

  const displayPercentage = (val: string) => {
    const numberVal = Number(val);
    return `${numberVal * 100}%`;
  };

  const displayDollars = (val: string) => {
    const numberVal = Number(val);
    return `$${numberVal}`;
  };

  const closeModal = () => {
    setModalData(ModalTypes.None);
  };

  return (
    <PageContentContainer style={{ flexDirection: 'row' }}>
      <Column>
        <Row style={{ justifyContent: 'space-between' }}>
          <Column style={{ marginRight: '40px', width: '100%', position: 'relative' }}>
            <ProfileEditButton
              className="fa fa-pen"
              onClick={() => setModalData(ModalTypes.Profile)}
            />
            <SectionHeader>Profile</SectionHeader>
            <ProfileSectionContainer style={{ minWidth: '320px' }}>
              <Row>
                <InputsColumn>
                  <InputRow>
                    <InputName>First Name</InputName>
                    <InputField
                      name="description"
                      onChange={(e) => {}}
                      value={firstName}
                      disabled
                    />
                  </InputRow>
                  <InputRow>
                    <InputName>Last Name</InputName>
                    <InputField name="description" onChange={(e) => {}} value={lastName} disabled />
                  </InputRow>
                  <InputRow>
                    <InputName>Email</InputName>
                    <OverlayTrigger
                      key="emailTooltip"
                      placement="top"
                      overlay={<Tooltip id="emailTooltip">{email}</Tooltip>}
                    >
                      <TextValue>{email}</TextValue>
                    </OverlayTrigger>
                  </InputRow>
                  <InputRow>
                    <InputName>Password</InputName>
                    <ResetPasswordButton onClick={resetPasswordHandler}>RESET</ResetPasswordButton>
                  </InputRow>
                </InputsColumn>
              </Row>
            </ProfileSectionContainer>
          </Column>
          <Column style={{ marginRight: '40px', width: '100%', position: 'relative' }}>
            <ProfileEditButton
              className="fa fa-pen"
              onClick={() => setModalData(ModalTypes.VenueLogo)}
            />
            <SectionHeader>Company Profile</SectionHeader>
            <ProfileSectionContainer>
              <Row style={{ fontSize: '12px', color: 'var(--grey)' }}>
                <InputName>Logo</InputName>
                {logo ? <ProfilePicture src={logo} alt="logo" /> : <ProfilePicturePlaceholder />}
              </Row>
              <Row style={{ fontSize: '12px' }}>
                <InputRow>
                  <InputName>Company</InputName>
                  <OverlayTrigger
                    key="organizationName"
                    placement="top"
                    overlay={<Tooltip id="organizationName">{organizationName}</Tooltip>}
                  >
                    <TextValue>{organizationName}</TextValue>
                  </OverlayTrigger>
                </InputRow>
              </Row>
            </ProfileSectionContainer>
          </Column>
          <Column style={{ marginRight: '40px', width: '100%' }}>
            <SectionHeader>Company Calculations</SectionHeader>
            <ProfileSectionContainer>
              <Row>
                <InputsColumn>
                  <InfoField>
                    <InfoFieldInputName>Average Influencer Followers</InfoFieldInputName>
                    {venueAttributes.attributes[VenueAttributeName.AverageInfluencerFollowers] ||
                      ''}
                  </InfoField>
                  <InfoField>
                    <InfoFieldInputName>Asset Value Multiplier</InfoFieldInputName>
                    {venueAttributes.attributes[VenueAttributeName.AssetValueMultiplier] || ''}
                  </InfoField>
                  <InfoField>
                    <InfoFieldInputName>Impression View Rate</InfoFieldInputName>
                    {displayPercentage(
                      venueAttributes.attributes[VenueAttributeName.ImpressionViewRate]
                    ) || ''}
                  </InfoField>
                  <InfoField>
                    <InfoFieldInputName>CPM Rate</InfoFieldInputName>
                    {displayDollars(venueAttributes.attributes[VenueAttributeName.CMPRate]) || ''}
                  </InfoField>
                  <InfoField>
                    <InfoFieldInputName>Sales Value</InfoFieldInputName>
                    {displayDollars(venueAttributes.attributes[VenueAttributeName.SalesValue]) ||
                      ''}
                  </InfoField>
                </InputsColumn>
              </Row>
            </ProfileSectionContainer>
          </Column>
          <Column>
            <SectionHeader>Social Integrations</SectionHeader>
            <SocialLoginSection />
          </Column>
        </Row>
        <Row style={{ marginTop: '40px' }}>
          <EditImagesSection />
        </Row>
      </Column>
      <FSModal modalIsOpen={modalData === ModalTypes.Profile} onClose={closeModal}>
        <EditUserProfile triggerRefresh={() => refreshData()} closeModal={() => closeModal()} />
      </FSModal>
      <FSModal modalIsOpen={modalData === ModalTypes.Password} onClose={closeModal}>
        <EditPassword triggerRefresh={() => refreshData()} closeModal={() => closeModal()} />
      </FSModal>
      <FSModal modalIsOpen={modalData === ModalTypes.VenueLogo} onClose={closeModal}>
        <ChangeWebappImage
          triggerRefresh={() => refreshData()}
          closeModal={() => closeModal()}
          imageAttribute={VenueAttributeName.SalesValue}
          imageType={ModalUploadImageType.Logo}
        />
      </FSModal>
    </PageContentContainer>
  );
};
