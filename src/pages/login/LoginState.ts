import React from 'react';

export interface LoginState {
  email: string;
  password: string;
  validEmail: boolean;
  validPassword: boolean;
  authError: boolean;
  activeSubmitButton: boolean;
}

const initialLoginState: LoginState = {
  activeSubmitButton: true,
  authError: false,
  email: '',
  password: '',
  validEmail: true,
  validPassword: true,
};

export enum LoginActionTypes {
  SET_EMAIL = 'SET_EMAIL',
  SET_PASSWORD = 'SET_PASSWORD',
  SET_EMAIL_VALID = 'SET_EMAIL_VALID',
  SET_PASSWORD_VALID = 'SET_PASSWORD_VALID',
  SET_AUTH_ERROR = 'SET_AUTH_ERROR',
  SET_ACTIVE_SUBMIT_BUTTON = 'SET_ACTIVE_SUBMIT_BUTTON',
}

export type LoginAction =
  | { type: LoginActionTypes.SET_ACTIVE_SUBMIT_BUTTON; payload: boolean }
  | { type: LoginActionTypes.SET_AUTH_ERROR; payload: boolean }
  | { type: LoginActionTypes.SET_EMAIL; payload: string }
  | { type: LoginActionTypes.SET_PASSWORD; payload: string }
  | { type: LoginActionTypes.SET_EMAIL_VALID; payload: boolean }
  | { type: LoginActionTypes.SET_PASSWORD_VALID; payload: boolean };

export const useLoginReducer = (): [
  LoginState,
  React.Dispatch<LoginAction>
] => {
  const loginReducer = (state: LoginState, action: LoginAction): LoginState => {
    switch (action.type) {
      case LoginActionTypes.SET_ACTIVE_SUBMIT_BUTTON:
        return { ...state, activeSubmitButton: action.payload };
      case LoginActionTypes.SET_AUTH_ERROR:
        return { ...state, authError: action.payload };
      case LoginActionTypes.SET_EMAIL:
        return { ...state, email: action.payload };
      case LoginActionTypes.SET_PASSWORD:
        return { ...state, password: action.payload };
      case LoginActionTypes.SET_EMAIL_VALID:
        return { ...state, validEmail: action.payload };
      case LoginActionTypes.SET_PASSWORD_VALID:
        return { ...state, validPassword: action.payload };
      default:
        return state;
    }
  };

  const [data, dispatch] = React.useReducer(loginReducer, initialLoginState);
  return [data, dispatch];
};
