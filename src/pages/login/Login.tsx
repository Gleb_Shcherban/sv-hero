import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { LoginActionTypes, useLoginReducer } from './LoginState';

import { httpClient } from '../../services/httpClient/httpClient';
import { AuthEndpoints } from '../../api/endpoints';
import { LoginPageStrings } from '../../common/localization/en';
import { LoginRequestBody, MeApiModel } from '../../api/models/auth';
import { emailMaskRegex } from '../../common/constants/constants';
import { WebsiteRoutes } from '../../common/constants/routes';

import { Button } from 'react-bootstrap';
import { InputField } from '../../common/components/inputField/InputField';
import { LoginContainer, LoginForm, LoginHeader } from './Login.style';
import { ErrorText } from '../../common/components/inputField/InputField.style';
import { useAppDispatch } from '../../store';
import { updateMe } from '../../store/slices/meSlice';
import { getAttributes, getVenue } from '../../store/slices/venueSlice';

export const Login: React.FC = () => {
  const history = useHistory();
  const appDispatch = useAppDispatch();
  const [loginData, dispatch] = useLoginReducer();

  const validatePassword = () => {
    if (loginData.password.length > 5) {
      dispatch({ type: LoginActionTypes.SET_PASSWORD_VALID, payload: true });
      return true;
    } else {
      dispatch({ type: LoginActionTypes.SET_PASSWORD_VALID, payload: false });
      return false;
    }
  };

  const validateEmail = () => {
    if (emailMaskRegex.test(loginData.email)) {
      dispatch({ type: LoginActionTypes.SET_EMAIL_VALID, payload: true });
      return true;
    } else {
      dispatch({ type: LoginActionTypes.SET_EMAIL_VALID, payload: false });
      return false;
    }
  };

  useEffect(() => {
    dispatch({ type: LoginActionTypes.SET_EMAIL_VALID, payload: true });
    dispatch({ type: LoginActionTypes.SET_PASSWORD_VALID, payload: true });
    dispatch({ type: LoginActionTypes.SET_AUTH_ERROR, payload: false });
    dispatch({
      type: LoginActionTypes.SET_ACTIVE_SUBMIT_BUTTON,
      payload: true,
    });
  }, [loginData.email, loginData.password, dispatch]);

  const submitForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (validateEmail() && validatePassword() && loginData.activeSubmitButton) {
      dispatch({
        type: LoginActionTypes.SET_ACTIVE_SUBMIT_BUTTON,
        payload: false,
      });
      httpClient
        .login<LoginRequestBody, MeApiModel>({
          url: AuthEndpoints.Login,
          payload: { username: loginData.email, password: loginData.password },
          requiresToken: false,
        })
        .then((value) => {
          appDispatch(updateMe(value));
          appDispatch(getVenue());
          appDispatch(getAttributes());
          history.push(WebsiteRoutes.Dashboard);
        })
        .catch(() => {
          dispatch({
            type: LoginActionTypes.SET_ACTIVE_SUBMIT_BUTTON,
            payload: false,
          });
          dispatch({ type: LoginActionTypes.SET_AUTH_ERROR, payload: true });
        });
    }
  };

  return (
    <LoginContainer>
      <LoginHeader>{LoginPageStrings.Title}</LoginHeader>
      <LoginForm onSubmit={submitForm}>
        <InputField
          name="email"
          value={loginData.email}
          onChange={(val) => {
            dispatch({
              type: LoginActionTypes.SET_EMAIL,
              payload: val.target.value,
            });
          }}
          placeholder={LoginPageStrings.Email}
          type="email"
          headerText={LoginPageStrings.UserNameLabel}
          errorText={loginData.validEmail ? '' : LoginPageStrings.EmailValidationError}
        />
        <InputField
          name="password"
          value={loginData.password}
          onChange={(val) => {
            dispatch({
              type: LoginActionTypes.SET_PASSWORD,
              payload: val.target.value,
            });
          }}
          placeholder={LoginPageStrings.Password}
          type="password"
          headerText={LoginPageStrings.PasswordLabel}
          errorText={loginData.validPassword ? '' : LoginPageStrings.PasswordValidationError}
        />
        {loginData.authError && <ErrorText>{LoginPageStrings.AuthError}</ErrorText>}
        <Button
          type="submit"
          variant={loginData.activeSubmitButton ? 'primary' : 'secondary'}
          style={{ marginTop: loginData.authError ? '8px' : '32px' }}
          active={loginData.activeSubmitButton}
        >
          Login
        </Button>
      </LoginForm>
    </LoginContainer>
  );
};
