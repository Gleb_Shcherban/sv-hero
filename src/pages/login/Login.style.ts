import styled from 'styled-components';

export const LoginContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  width: 100%;
  height: 100%;
  background-color: var(--grey);
`;

export const LoginHeader = styled.div`
  display: flex;
  position: absolute;
  top: 100px;
  left: 0;
  width: 100%;
  justify-content: center;
  font-size: 24px;
  color: var(--white);
`;

export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 350px;
  height: 350px;
  padding: 14px;
  border-radius: 14px;
  box-sizing: border-box;
  background-color: var(--white);
`;
