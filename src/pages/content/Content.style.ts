import styled from 'styled-components';

import { TABLET_MAX_WIDTH } from '../../common/constants/constants';

export const ContentSectionWrapper = styled.div`
  display: flex;
  max-width: 100%;

  > div {
    order: 1;
  }

  > section {
    order: 2;
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    flex-direction: column;

    > div {
      order: 2;
    }

    > section {
      order: 1;
    }
  }
`;
