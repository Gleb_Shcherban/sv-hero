import React, { useEffect } from 'react';
import { Container, ContainerItem, Header } from './ContentSection.style';
import { getCampaigns } from '../../../store/slices/campaignsSlice';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { Spinner } from '../../../common/assets/Spinner';
import { CampaignVideos } from '../../../common/components/campaignVideos/CampaignVideos';

interface IContentSection {
  onClickInfoButton?: (userId: string) => void;
}

export const ContentSection: React.FC<IContentSection> = ({
  onClickInfoButton,
}) => {
  const dispatch = useAppDispatch();
  const {
    isLoading,
    totalItems,
    items,
    page,
    size,
    sort,
    lastUpdated,
  } = useTypedSelector((state) => state.campaigns);

  useEffect(() => {
    if (!totalItems || page !== 0) {
      dispatch(getCampaigns({ page, size }));
    }
  }, [dispatch, page, sort, size, totalItems, lastUpdated]);

  const renderItems = () => {
    if (isLoading) {
      return <Spinner color="var(--spinnerColor)" />;
    }
    return items.map((item) => (
      <ContainerItem key={item.id}>
        <Header>
          <h1>{item.title}</h1>
          <h2>{item.description}</h2>
        </Header>
        <CampaignVideos
          isInfoIcon
          campaignId={item.id}
          onClickInfoButton={onClickInfoButton}
        />
      </ContainerItem>
    ));
  };

  return <Container>{renderItems()}</Container>;
};
