import React, { useState, useCallback } from 'react';
import { PageContentContainer } from '../../common/styles/commonStyles.style';
import { TopBar } from './topBar/TopBar';
import { ContentSection } from './contentSection/ContentSection';
import { UserDetailsSection } from '../../common/components/userDetailsSection/UserDetailsSection';

import { ContentSectionWrapper } from './Content.style';

export const Content: React.FC = () => {
  const [userId, setUserId] = useState('');

  const onCloseDetailsSectionHandler = () => {
    setUserId('');
  };

  const onClickInfoButtonHandler = useCallback((userId: string) => {
    const contentWrapper = document.body.querySelector(
      '.content-wrapper'
    ) as HTMLElement;

    contentWrapper.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
    setUserId(userId);
  }, []);

  return (
    <PageContentContainer>
      <TopBar />
      <ContentSectionWrapper>
        <ContentSection onClickInfoButton={onClickInfoButtonHandler} />
        {userId && (
          <UserDetailsSection
            userId={userId}
            closeSection={onCloseDetailsSectionHandler}
          />
        )}
      </ContentSectionWrapper>
    </PageContentContainer>
  );
};
