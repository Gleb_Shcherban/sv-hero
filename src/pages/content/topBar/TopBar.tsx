import React from 'react';
import { Container } from './TopBar.style';
// Cool, this filter is out of game now, but can be used elsewhere
// import { PageFilter } from '../../../common/components/pageFilter/SearchPanel';
import { NotificationButton } from '../../../common/components/notificationButton/NotificationButton';
import { SearchPanel } from '../../../common/components/searchPanel/SearchPanel';

export const TopBar: React.FC = () => {
  return (
    <Container>
      <SearchPanel onClickSearch={() => {}} value={''} />
      <NotificationButton />
    </Container>
  );
};
