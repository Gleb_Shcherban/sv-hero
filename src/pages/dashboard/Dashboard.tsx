import React, { useEffect, useState } from 'react';

import { AnalyticsSection } from './analyticsSection/AnalyticsSection';
import { ChartSection } from './chartSection/ChartSection';

import { PageContentContainer } from '../../common/styles/commonStyles.style';
import { RecentVideosSection } from './recentVideosSection/RecentVideosSection';
import { NotificationButton } from '../../common/components/notificationButton/NotificationButton';

import {
  GeneralKPIsApiModel,
  KPIValueApiModel,
} from '../../api/models/analytics';
import { httpClient } from '../../services/httpClient/httpClient';
import { AnalyticsEndpoints, getApiUrlForId } from '../../api/endpoints';

export enum ActiveStatisticsBlock {
  Influencers = 'influencers',
  Audience = 'audience',
  Shares = 'shares',
  Impressions = 'impressions',
  Engagements = 'engagements',
  Redemptions = 'redemptions',
}

export const Dashboard: React.FC = () => {
  const [activeBlock, setActiveBlock] = useState(
    ActiveStatisticsBlock.Influencers
  );
  const [generalKPIs, setGeneralKPIs] = useState<GeneralKPIsApiModel>({
    assetValue: 0,
    audience: 0,
    engagements: 0,
    impressions: 0,
    influencers: 0,
    marketingValue: 0,
    redemptions: 0,
    salesValue: 0,
    shares: 0,
  });
  const [chartData, setChartData] = useState<KPIValueApiModel[]>([]);

  useEffect(() => {
    httpClient
      .get<never, GeneralKPIsApiModel>({
        url: AnalyticsEndpoints.GetGeneralKPIs,
        requiresToken: true,
      })
      .then((res) => {
        if (res) setGeneralKPIs(res);
      })
      .catch((e) => console.log('Failed fetching KPIs', e));
  }, []);

  useEffect(() => {
    httpClient
      .get<never, KPIValueApiModel[]>({
        url: getApiUrlForId(AnalyticsEndpoints.GetKPI, activeBlock),
        requiresToken: true,
      })
      .then((res) => {
        if (res) setChartData(res);
      })
      .catch((e) => console.log('Failed fetching KPIs', e));
  }, [activeBlock]);

  return (
    <PageContentContainer>
      <NotificationButton />
      
      <AnalyticsSection
        activeBlock={activeBlock}
        setActiveBlock={setActiveBlock}
        generalKPIs={generalKPIs}
      />
      <ChartSection header={activeBlock} chartData={chartData} />
      <RecentVideosSection />
    </PageContentContainer>
  );
};
