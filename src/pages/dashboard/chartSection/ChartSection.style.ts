import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../../common/constants/constants';

export const ChartContainer = styled.div`
  position: relative;
  width: 100%;
  height: 360px;
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
  border-radius: 14px;
  padding: 23px 20px;
  box-sizing: border-box;
  background-color: var(--white);

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    height: 300px;
  }
`;
