import React from 'react';
import { Line } from 'react-chartjs-2';

import { ChartContainer } from './ChartSection.style';
import { ActiveStatisticsBlock } from '../Dashboard';
import { KPIValueApiModel } from '../../../api/models/analytics';

interface ChartSectionProps {
  header: ActiveStatisticsBlock;
  chartData: KPIValueApiModel[];
}

export const ChartSection: React.FC<ChartSectionProps> = ({
  header,
  chartData,
}) => {
  const data = (canvas: any) => {
    const ctx = canvas.getContext('2d');
    const gradient = ctx.createLinearGradient(0, 0, 0, 360);
    gradient.addColorStop(0, 'rgba(215, 204, 255, 0.5)');
    gradient.addColorStop(1, 'rgba(215, 204, 255, 0.5)');

    return {
      labels: chartData.map(
        (chartEntry) =>
          `${new Intl.DateTimeFormat('en', { day: '2-digit' }).format(
            new Date(chartEntry.date)
          )} ${new Intl.DateTimeFormat('en', { weekday: 'short' }).format(
            new Date(chartEntry.date)
          )}`
      ).reverse(),
      datasets: [
        {
          data: chartData.map((chartEntry) => chartEntry.value).reverse(),
          backgroundColor: gradient,
          borderColor: '#8868fe',
          pointRadius: 6,
          pointHoverRadius: 6,
          pointBackgroundColor: '#d43131',
          pointBorderColor: '#fff',
          pointHoverBorderColor: '#fff',
          pointBorderWidth: 2,
        },
      ],
    };
  };

  const options = {
    maintainAspectRatio: false,
    responsive: true,
    scales: {
      xAxes: [
        {
          gridLines: {
            drawOnChartArea: false,
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            borderDash: [2, 4],
            color: '#ececec;',
          },
        },
      ],
    },
    title: {
      display: true,
      text: header,
      position: 'top',
      fontSize: 14,
      fontColor: '#6236FF',
    },
    legend: {
      display: false,
    },
    tooltips: {
      xPadding: 10,
      yPadding: 10,
      borderWidth: 1,
      bodyFontSize: 13,
      bodyFontColor: '#000',
      backgroundColor: '#fff',
      borderColor: '#999B9E',
      displayColors: false,
      callbacks: {
        title: function () {
          return null;
        },
        label: function (tooltipItem: any) {
          return tooltipItem.yLabel.toString();
        },
      },
    },
  };

  return (
    <ChartContainer>
      <Line data={data} options={options} />
    </ChartContainer>
  );
};
