import React, { useCallback } from 'react';

import { DashboardPageStrings } from '../../../common/localization/en';

import { Title, Text } from '../../../common/styles/commonStyles.style';
import {
  AnalyticsContainer,
  Wrapper,
  PeopleBlock,
  RedemptionsBlock,
  ActionsBlock,
  TotalValue,
  TotalValueContent,
  Amount,
  AnalyticsBlock,
  AnalyticsIcon,
  Row,
} from './AnalyticsSection.style';
import { ActiveStatisticsBlock } from '../Dashboard';
import { GeneralKPIsApiModel } from '../../../api/models/analytics';
import userIcon from '../assets/user.png';
import twoGuysIcon from '../assets/two-guys.png';
import uploadIcon from '../assets/upload.png';
import illuminatiIcon from '../assets/illuminati.png';
import thumbsUpIcon from '../assets/thumbs-up.png';
import barcodeIcon from '../assets/barcode.png';

interface AnalyticsSectionProps {
  activeBlock: ActiveStatisticsBlock;
  setActiveBlock: (block: ActiveStatisticsBlock) => void;
  generalKPIs: GeneralKPIsApiModel;
}
export const AnalyticsSection: React.FC<AnalyticsSectionProps> = ({
  activeBlock,
  setActiveBlock,
  generalKPIs,
}) => {
  const renderTotalValueBlock = (
    title: string,
    description: string | null,
    amount: number
  ) => {
    return (
      <TotalValue>
        <TotalValueContent>
          <Title>{title}</Title>
          {description && <Text>{description}</Text>}
        </TotalValueContent>
        <Amount color="#06D22F">${amount.toLocaleString()}</Amount>
      </TotalValue>
    );
  };

  const renderAnalyticsBlock = useCallback(
    (
      name: ActiveStatisticsBlock,
      title: string,
      amount: number,
      description: string,
      color: string,
      icon
    ) => {
      return (
        <AnalyticsBlock
          border={activeBlock === name}
          onClick={() => setActiveBlock(name)}
        >
          <Title>{title}</Title>
          <Row>
            <AnalyticsIcon color={color}>
              <img
                src={icon}
                style={{ width: '22px', height: 'auto' }}
                alt=""
              />
            </AnalyticsIcon>
            <Amount color={color}>{amount.toLocaleString()}</Amount>
          </Row>
          <Text>{description}</Text>
        </AnalyticsBlock>
      );
    },
    [activeBlock, setActiveBlock]
  );

  return (
    <AnalyticsContainer>
      <Wrapper>
        <PeopleBlock>
          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Influencers,
            DashboardPageStrings.Influencers,
            generalKPIs.influencers,
            DashboardPageStrings.InyYourCommunity,
            '#7836FF',
            userIcon
          )}

          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Audience,
            DashboardPageStrings.Audience,
            generalKPIs.audience,
            DashboardPageStrings.TotalFollowers,
            '#B620E0',
            twoGuysIcon
          )}
        </PeopleBlock>

        {renderTotalValueBlock(
          DashboardPageStrings.AssetValue,
          DashboardPageStrings.CalculatedOver,
          generalKPIs.assetValue
        )}
      </Wrapper>
      <Wrapper>
        <ActionsBlock>
          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Shares,
            DashboardPageStrings.Shares,
            generalKPIs.shares,
            DashboardPageStrings.UGC,
            '#26C8F0',
            uploadIcon
          )}

          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Impressions,
            DashboardPageStrings.Impressions,
            generalKPIs.impressions,
            DashboardPageStrings.EstimatedUGCViews,
            '#20A3E0',
            illuminatiIcon
          )}

          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Engagements,
            DashboardPageStrings.Engagements,
            generalKPIs.engagements,
            DashboardPageStrings.AmountCommentsLikesUGC,
            '#205CE0',
            thumbsUpIcon
          )}
        </ActionsBlock>
        {renderTotalValueBlock(
          DashboardPageStrings.TotalValue,
          null,
          generalKPIs.marketingValue
        )}
      </Wrapper>
      <Wrapper>
        <RedemptionsBlock>
          {renderAnalyticsBlock(
            ActiveStatisticsBlock.Redemptions,
            DashboardPageStrings.Redemptions,
            generalKPIs.redemptions,
            DashboardPageStrings.AmountCodesForSales,
            '#E07B20',
            barcodeIcon
          )}
        </RedemptionsBlock>
        {renderTotalValueBlock(
          DashboardPageStrings.TotalValue,
          null,
          generalKPIs.salesValue
        )}
      </Wrapper>
    </AnalyticsContainer>
  );
};
