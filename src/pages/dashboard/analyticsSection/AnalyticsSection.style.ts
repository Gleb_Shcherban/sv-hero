import styled from 'styled-components';
import {
  LAPTOP_MAX_WIDTH,
  MOBILE_MAX_WIDTH,
} from '../../../common/constants/constants';

export const AnalyticsContainer = styled.div`
  margin-bottom: 30px;
  display: grid;
  grid-template-columns: 28% 43% 23%;
  justify-content: space-between;

  @media (max-width: ${LAPTOP_MAX_WIDTH}px) {
    grid-template-columns: 100%;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;

  @media (max-width: ${LAPTOP_MAX_WIDTH}px) {
    margin-bottom: 30px;
  }
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  margin-top: 22px;
`;

interface AnalyticsBlockProps {
  border: boolean;
}
export const AnalyticsBlock = styled.div<AnalyticsBlockProps>`
  padding: 40px 15px 15px;
  height: 194px;
  display: flex;
  flex-direction: column;
  border-radius: var(--commonBorderRadius);
  background-color: var(--white);
  cursor: pointer;
  border: ${(props) => (props.border ? '5px solid #6236FF' : 'none')};
`;

interface AmountProps {
  color: string;
}
//Total values
export const TotalValue = styled.div`
  margin-top: 10px;
  padding: 15px 20px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  border-radius: var(--commonBorderRadius);
  background-color: var(--white);
`;
export const TotalValueContent = styled.div``;
export const Amount = styled.div<AmountProps>`
  font-size: 30px;
  color: ${(props) => props.color};
`;

export const AnalyticsIcon = styled.div<AmountProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  width: 30px;
  margin-right: 10px;
  border-radius: 6px;
  background-color: ${(props) => props.color};
`;

export const PeopleBlock = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 10px;
`;
export const ActionsBlock = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 10px;

  @media (max-width: ${MOBILE_MAX_WIDTH}px) {
    grid-template-columns: 100%;

    > div {
      margin-top: 10px;
    }
  }
`;
export const RedemptionsBlock = styled.div``;
