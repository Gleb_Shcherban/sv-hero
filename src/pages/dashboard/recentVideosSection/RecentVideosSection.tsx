import React, { useEffect } from 'react';
import { Container, Header, HeaderBar } from './RecentVideosSection.style';
import { getStoryVideos } from '../../../store/slices/storyVideosSlice';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { defaultPagination } from '../../../common/constants/constants';
import { VideosSection } from '../../../common/components/videosSection/VideosSection';
import { MapButton } from '../../../common/components/mapButton/MapButton';

export const RecentVideosSection: React.FC = () => {
  const dispatch = useAppDispatch();
  const { isLoading, items, page, size, sort, lastUpdated, error } = useTypedSelector(
    (state) => state.storyVideos
  );

  useEffect(() => {
    dispatch(
      getStoryVideos({
        page: defaultPagination.page,
        sort: defaultPagination.sortByLastCreated,
        size: defaultPagination.size,
      })
    );
  }, [dispatch]);

  return (
    <Container>
      <HeaderBar>
        <Header>
          <h1>Latest videos</h1>
        </Header>
        <MapButton />
      </HeaderBar>
      <VideosSection
        isLoading={isLoading}
        error={error}
        items={items}
        page={page}
        size={size}
        sort={sort || ''}
        lastUpdated={lastUpdated}
      />
    </Container>
  );
};
