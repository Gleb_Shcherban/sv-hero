import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 85vh;
  border-radius: 14px;
  padding: 23px 20px;
  box-sizing: border-box;
  background-color: var(--white);
`;

export const HeaderBar = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;

  > h1 {
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    text-transform: uppercase;
    color: #616161;
  }

  > h2 {
    margin-top: 10px;
    font-size: 14px;
    font-weight: 500;
    font-style: normal;
    color: #6d7278;
  }
`;
