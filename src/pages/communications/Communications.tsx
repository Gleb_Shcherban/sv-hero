import React from 'react';
import {
  ContentSectionContainer,
  BodySection,
  TableWrapper,
  CheckMark,
  Search,
} from '../../common/styles/commonStyles.style';
import { CommunicationsPageStrings } from '../../common/localization/en';
import { CommonTable } from '../../common/components/table/CommonTable';
import { TableHeaderWrapper } from '../../common/components/table/tableHeaderWrapper/TableHeaderWrapper';
import {
  TableCellWithParams,
  TableContent,
  TableRow,
} from '../../common/commonTypes';

interface CommunicationsContent extends TableRow {
  sent: TableCellWithParams<JSX.Element>;
  dateSent: TableCellWithParams<string>;
  sends: TableCellWithParams<string>;
  opens: TableCellWithParams<string>;
  clicks: TableCellWithParams<string>;
  bounces: TableCellWithParams<string>;
  examine: TableCellWithParams<JSX.Element>;
}

export const Communications: React.FC = () => {
  const onEditClick = (id: string) => {
    console.log('ID =', id);
  };
  const manageCategoriesSectionTableContents: TableContent<CommunicationsContent> = {
    header: {
      sent: {
        name: CommunicationsPageStrings.Sent,
        sortable: false,
      },
      dateSent: {
        name: CommunicationsPageStrings.DateSent,
        sortable: true,
      },
      sends: {
        name: CommunicationsPageStrings.Sends,
        sortable: false,
      },
      opens: {
        name: CommunicationsPageStrings.Opens,
        sortable: false,
      },
      clicks: {
        name: CommunicationsPageStrings.Clicks,
        sortable: false,
      },
      bounces: {
        name: CommunicationsPageStrings.Bounces,
        sortable: false,
      },
      examine: {
        name: '',
        sortable: false,
      },
    },
    rows: [
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Aug 1, 2020' },
        sends: { render: '0' },
        opens: { render: '0' },
        clicks: { render: '0' },
        bounces: { render: '0' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 28, 2020' },
        sends: { render: '17,928' },
        opens: { render: '10,412' },
        clicks: { render: '6,562' },
        bounces: { render: '33' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 21, 2017' },
        sends: { render: '15,9452' },
        opens: { render: '8,994' },
        clicks: { render: '5,137' },
        bounces: { render: '25' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Aug 1, 2020' },
        sends: { render: '0' },
        opens: { render: '0' },
        clicks: { render: '0' },
        bounces: { render: '0' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 28, 2020' },
        sends: { render: '17,928' },
        opens: { render: '10,412' },
        clicks: { render: '6,562' },
        bounces: { render: '33' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 21, 2017' },
        sends: { render: '15,9452' },
        opens: { render: '8,994' },
        clicks: { render: '5,137' },
        bounces: { render: '25' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Aug 1, 2020' },
        sends: { render: '0' },
        opens: { render: '0' },
        clicks: { render: '0' },
        bounces: { render: '0' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 28, 2020' },
        sends: { render: '17,928' },
        opens: { render: '10,412' },
        clicks: { render: '6,562' },
        bounces: { render: '33' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 21, 2017' },
        sends: { render: '15,9452' },
        opens: { render: '8,994' },
        clicks: { render: '5,137' },
        bounces: { render: '25' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Aug 1, 2020' },
        sends: { render: '0' },
        opens: { render: '0' },
        clicks: { render: '0' },
        bounces: { render: '0' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 28, 2020' },
        sends: { render: '17,928' },
        opens: { render: '10,412' },
        clicks: { render: '6,562' },
        bounces: { render: '33' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
      {
        sent: {
          render: <CheckMark className="fas fa-check" />,
        },
        dateSent: { render: 'Sep 21, 2017' },
        sends: { render: '15,9452' },
        opens: { render: '8,994' },
        clicks: { render: '5,137' },
        bounces: { render: '25' },
        examine: {
          render: (
            <Search
              className="fas fa-search"
              onClick={() => onEditClick('1')}
            />
          ),
        },
      },
    ],
  };

  const onSortClick = (name: string) => {
    console.log('SORT BY:', name);
  };
  return (
    <ContentSectionContainer largeFill>
      <BodySection>
        <TableWrapper>
          <TableHeaderWrapper
            title={CommunicationsPageStrings.Title}
            modalTitle={CommunicationsPageStrings.AddModalTitle}
            orderDirection=""
            onClickSort={() => {}}
            addButtonComponent={<div>+</div>}
          >
            <CommonTable
              content={manageCategoriesSectionTableContents}
              onSortHeaderClick={onSortClick}
              goToPage={(page) => {
                console.log('GOTO:', page);
              }}
              page={1}
              totalPages={1}
              isLoading={false}
              noContent={false}
              totalItems={manageCategoriesSectionTableContents.rows.length}
              tablePadding="0"
              small
              highlightRow
            />
          </TableHeaderWrapper>
        </TableWrapper>
      </BodySection>
    </ContentSectionContainer>
  );
};
