import React, { useCallback, useRef, useState } from 'react';
import Modal from 'react-modal';
import { FiltersEndpoints, MediaEndpoints } from '../../api/endpoints';
import { FilterApiModel, VideoFilterVariationType } from '../../api/models/filters';
import { Spinner } from '../../common/assets/Spinner';
import { InlineInput } from '../../common/components/inlineInput';
import { httpClient } from '../../services/httpClient/httpClient';
import { getCloudinaryUrlForFilter } from '../../services/utilities';
import {
  ModalHeader,
  BackIcon,
  CampaignInfoWrapper,
  SubmitModalButton,
} from '../videoTool/videoTool.style';
import {
  VariantsContainer,
  VariantItem,
  VariantWrapper,
  VariantIcon,
  VariantDescription,
} from './filters.style';
import { useTypedSelector } from '../../store';

const customStyles = {
  content: {
    borderRadius: 8,
    backgroundColor: '#fff',
  },
  overlay: {
    zIndex: 2,
  },
};

interface VariantProps {
  description: string;
  setUri: (uri: string) => void;
  value: string | null;
}

const Variant: React.FC<VariantProps> = ({ description, setUri, value }) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const { venue } = useTypedSelector((state) => state.venue);

  const handleUpload = () => {
    if (inputRef && inputRef.current) {
      inputRef.current.click();
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files ? e.target.files[0] : null;
    if (!file) {
      return;
    }

    console.log(file);

    const formData = new FormData();
    formData.append('mediaFile', file);
    formData.append('prefix', 'tmp');
    formData.append('isStockContent', 'true');

    setIsLoading(true);
    httpClient
      .post<FormData, { url: string }>({
        url: MediaEndpoints.UploadFile,
        payload: formData,
        requiresToken: true,
      })
      .then(async (response) => {
        console.log(response);
        await fetch(getCloudinaryUrlForFilter(response.url, venue.subdomain));
        setUri(response.url);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  };

  return (
    <VariantWrapper>
      <VariantItem onClick={handleUpload} value={value}>
        {isLoading ? (
          <Spinner color="var(--spinnerColor)" />
        ) : (
          <VariantIcon className="fa fa-plus" />
        )}
      </VariantItem>
      <VariantDescription>{description}</VariantDescription>
      <input
        type="file"
        accept="image/png"
        ref={inputRef}
        onChange={handleChange}
        className="visually-hidden"
      />
    </VariantWrapper>
  );
};

interface CreateFilterModalProps {
  isOpen: boolean;
  handleClose: () => void;
  updateContent: () => void;
}

export const CreateFilterModal: React.FC<CreateFilterModalProps> = ({
  isOpen,
  handleClose,
  updateContent,
}) => {
  const [filterName, setFilterName] = useState('Filter Name');
  const [jumboUri, setJumboUri] = useState<string | null>(null);
  const [circleUri, setCircleUri] = useState<string | null>(null);
  const [sequentialUri, setSequentialUri] = useState<string | null>(null);

  const resetState = useCallback(() => {
    setFilterName('');
    setJumboUri(null);
    setCircleUri(null);
    setSequentialUri(null);
  }, [setFilterName, setJumboUri, setCircleUri, setSequentialUri]);

  const handleSubmitModal = useCallback(() => {
    httpClient
      .post<any, FilterApiModel>({
        url: FiltersEndpoints.CreateFilter,
        requiresToken: true,
        payload: {
          name: filterName,
          variations: [
            { type: VideoFilterVariationType.jumbo, imageUri: jumboUri },
            { type: VideoFilterVariationType.circular, imageUri: circleUri },
            {
              type: VideoFilterVariationType.sequential,
              imageUri: sequentialUri,
            },
          ],
        },
      })
      .then((response) => {
        console.log(response);
        resetState();
        updateContent();
        handleClose();
      })
      .catch((error) => console.log(error));
  }, [filterName, jumboUri, circleUri, sequentialUri, resetState, updateContent, handleClose]);

  return (
    <Modal style={customStyles} isOpen={isOpen} ariaHideApp={false}>
      <ModalHeader>
        <BackIcon onClick={handleClose} className="fa fa-arrow-left" />
        <SubmitModalButton
          disabled={!filterName || !jumboUri || !circleUri || !sequentialUri}
          onClick={handleSubmitModal}
        >
          Create
        </SubmitModalButton>
      </ModalHeader>
      <CampaignInfoWrapper style={{ padding: '20px 0' }}>
        <InlineInput
          tag="div"
          tagStyles={{
            border: '2px solid transparent',
            textTransform: 'uppercase',
          }}
          value={filterName}
          setValue={setFilterName}
        />
      </CampaignInfoWrapper>
      <VariantsContainer>
        <Variant description="Square" setUri={setJumboUri} value={jumboUri} />
        <Variant description="Circle" setUri={setCircleUri} value={circleUri} />
        <Variant description="Sequential" setUri={setSequentialUri} value={sequentialUri} />
      </VariantsContainer>
    </Modal>
  );
};
