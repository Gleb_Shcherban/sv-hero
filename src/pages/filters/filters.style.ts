import styled from 'styled-components';
import { CampaignWrapper, CampaignInfoWrapper } from '../videoTool/videoTool.style';

export const PageWrapper = styled.div`
  padding: 40px 20px;
  max-width: 1440px;
  margin: 0 auto;
  box-sizing: border-box;
  position: relative
`;

export const FilterWrapper = styled(CampaignWrapper)``;

export const FilterInfoWrapper = styled(CampaignInfoWrapper)``;

export const VariantWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-right: 40px;
`;

export const VariantItem = styled.div<{ value: string | null }>`
  width: 268px;
  height: 395px;
  background-color: #D8D8D8;
  display: flex;
  align-items: center;
  justify-content: center;
  background-position: top center;
  background-size: 100%;
  cursor: pointer;
  background-image: ${p => `url(${p.value})`};
  background-repeat: no-repeat;
`;

export const FilterVariantItem = styled(VariantItem)`
  width: 236px;
  height: 340px;
  border-radius: 14px;
`;

export const VariantDescription = styled.div`
  color: #6D7278;
  font-size: 20px;
  margin-top: 14px;
  font-weight: normal;
  text-transform: capitalize;
`;

export const VariantsContainer = styled.div`
  display: flex;
`;

export const VariantIcon = styled.i`
  font-size: 18px;
  font-weight: bold;
`;

export const VideoItem = styled.video`
  width: auto;
  border-radius: 5px;
`;

export const VideosRow = styled.div`
  display: grid;
  grid-template-columns: 374px 374px 374px;
  grid-gap: 40px;
`;

export const VideoWrapper = styled.div`
  position: relative;
  /* display: flex;
  flex-direction: column;
  align-items: center; */
`;
