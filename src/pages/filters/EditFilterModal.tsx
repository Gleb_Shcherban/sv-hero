import React, { useState } from 'react';
import { httpClient } from '../../services/httpClient/httpClient';
import {
  EditVideoTitleContainer,
  TitleRow,
} from '../videoTool/videoTool.style';
import {
  FiltersPageStrings,
  MonetizationPageStrings,
  VideosPageStrings,
} from '../../common/localization/en';
import { InputField } from '../../common/components/inputField/InputField';
import { Button } from 'react-bootstrap';
import { ButtonsContainer } from '../monetization/editItemForms/EditForm.style';
import { FiltersEndpoints, getApiUrlForId } from '../../api/endpoints';
import { FilterApiModel, PatchFilterApiModel } from '../../api/models/filters';

interface EditFilterModalProps {
  id: string;
  oldTitle: string;
  closeModal: () => void;
  triggerRefresh: () => void;
}

export const EditFilterModal: React.FC<EditFilterModalProps> = ({
  id,
  oldTitle,
  closeModal,
  triggerRefresh,
}) => {
  const [isUpdating, setIsUpdating] = useState(false);
  const [title, setTitle] = useState(oldTitle);

  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const editCategory = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    const payload: PatchFilterApiModel = {
      name: title,
    };
    httpClient
      .patch<PatchFilterApiModel, FilterApiModel>({
        url: getApiUrlForId(FiltersEndpoints.PatchFilterById, id),
        requiresToken: true,
        payload,
      })
      .then((response) => {
        triggerRefresh();
        closeModal();
      })
      .catch((err) => {
        setIsUpdating(false);
        console.log(err);
      });
  };

  return (
    <EditVideoTitleContainer>
      <TitleRow>
        <InputField
          name="categoryName"
          headerText={FiltersPageStrings.EditFilter}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          placeholder={MonetizationPageStrings.EnterName}
        />
      </TitleRow>
      <ButtonsContainer>
        <Button variant={getButtonVariant('primary')} onClick={editCategory}>
          {VideosPageStrings.SaveChanges}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
