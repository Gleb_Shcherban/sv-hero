import React, { memo, useCallback, useEffect, useState } from 'react';
import { FilterApiModel } from '../../api/models/filters';
import { Spinner } from '../../common/assets/Spinner';
import { GetFilters } from '../../store/slices/videosSlice';
import {
  CampaignLabel,
  CampaignValue,
  ControlsWrapper,
  CtaButton,
  CtaButtonWrapper,
  PageTitle,
  CtaIcon,
} from '../videoTool/videoTool.style';
import { CreateFilterModal } from './CreateFilterModal';
import {
  PageWrapper,
  FilterWrapper,
  FilterInfoWrapper,
  VariantWrapper,
  VariantDescription,
  VariantsContainer,
  FilterVariantItem,
} from './filters.style';
import { FSModal } from '../../common/components/modal/Modal';
import { RemoveFilterModal } from './RemoveFilterModal';
import { EditFilterModal } from './EditFilterModal';

interface FilterModalData {
  id: string;
  name: string;
}

interface EditOrDeleteFilterModal extends FilterModalData {
  isOpen: boolean;
}

export const FiltersPage: React.FC = memo(() => {
  // TODO: change logic of the component. There should not be any manual handling
  //  of isLoaded, errors etc. This logic MUST be moved to redux slices
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filtersList, setFiltersList] = useState<FilterApiModel[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const initialEditOrDeleteFilterModalData: EditOrDeleteFilterModal = {
    isOpen: false,
    id: '',
    name: '',
  };

  const [editFilterModalData, setEditFilterModalData] = useState(
    initialEditOrDeleteFilterModalData
  );
  const [deleteFilterModalData, setDeleteFilterModalData] = useState(
    initialEditOrDeleteFilterModalData
  );

  const getFiltersRequest = useCallback(() => {
    setIsLoading(true);
    // TODO: remove all this code, that is handling loading manually. We have redux slices just for that
    GetFilters()
      .then((filters) => {
        setFiltersList(filters.items);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, [setFiltersList, setIsLoading]);

  useEffect(() => {
    getFiltersRequest();
  }, [getFiltersRequest]);

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  if (isLoading) {
    return (
      <PageWrapper>
        <Spinner color="var(--spinnerColor)" />
      </PageWrapper>
    );
  }

  // TODO: review this code and decide, if it should be separated to smaller components
  return (
    <PageWrapper>
      <PageTitle>Filters</PageTitle>
      <CtaButtonWrapper>
        <CtaButton
          onClick={() => {
            setIsModalOpen(true);
          }}
        >
          Create Filter
        </CtaButton>
      </CtaButtonWrapper>
      {!!filtersList.length &&
        filtersList.map((filter) => (
          <FilterWrapper key={filter.id}>
            <ControlsWrapper>
              <CtaIcon
                className="fa fa-pen"
                onClick={() => {
                  setEditFilterModalData({
                    isOpen: true,
                    id: filter.id,
                    name: filter.name,
                  });
                }}
              />
              <CtaIcon
                className="fa fa-trash"
                onClick={() => {
                  setDeleteFilterModalData({
                    isOpen: true,
                    id: filter.id,
                    name: filter.name,
                  });
                }}
              />
            </ControlsWrapper>
            <FilterInfoWrapper>
              <CampaignLabel>Filter Name</CampaignLabel>
              <CampaignValue style={{ textTransform: 'uppercase', color: '#222' }}>
                {filter.name}
              </CampaignValue>
            </FilterInfoWrapper>
            <div>
              <CampaignLabel style={{ marginBottom: 10 }}>Filters</CampaignLabel>
              <VariantsContainer>
                {filter.variations.map((variant) => (
                  <VariantWrapper key={variant.imageUri}>
                    <FilterVariantItem value={variant.imageUri} />
                    <VariantDescription>{variant.type}</VariantDescription>
                  </VariantWrapper>
                ))}
              </VariantsContainer>
            </div>
          </FilterWrapper>
        ))}
      <CreateFilterModal
        isOpen={isModalOpen}
        handleClose={handleModalClose}
        updateContent={getFiltersRequest}
      />
      <FSModal
        modalIsOpen={editFilterModalData.isOpen}
        onClose={() => setEditFilterModalData(initialEditOrDeleteFilterModalData)}
      >
        <EditFilterModal
          id={editFilterModalData.id}
          oldTitle={editFilterModalData.name}
          closeModal={() => setEditFilterModalData(initialEditOrDeleteFilterModalData)}
          triggerRefresh={getFiltersRequest}
        />
      </FSModal>
      <FSModal
        modalIsOpen={deleteFilterModalData.isOpen}
        onClose={() => setDeleteFilterModalData(initialEditOrDeleteFilterModalData)}
      >
        <RemoveFilterModal
          id={deleteFilterModalData.id}
          name={deleteFilterModalData.name}
          closeModal={() => setDeleteFilterModalData(initialEditOrDeleteFilterModalData)}
          triggerRefresh={getFiltersRequest}
        />
      </FSModal>
    </PageWrapper>
  );
});
