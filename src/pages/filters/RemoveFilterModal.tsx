import React, { useState } from 'react';
import { httpClient } from '../../services/httpClient/httpClient';
import {
  EditVideoTitleContainer,
  TitleRow,
} from '../videoTool/videoTool.style';
import { FiltersPageStrings } from '../../common/localization/en';
import { Button } from 'react-bootstrap';
import { ButtonsContainer } from '../monetization/editItemForms/EditForm.style';
import { FiltersEndpoints, getApiUrlForId } from '../../api/endpoints';
import { DeleteItemResponse } from '../../api/models/common';

interface RemoveFilterModalProps {
  id: string;
  name: string;
  closeModal: () => void;
  triggerRefresh: () => void;
}

export const RemoveFilterModal: React.FC<RemoveFilterModalProps> = ({
  id,
  name,
  closeModal,
  triggerRefresh,
}) => {
  const [isUpdating, setIsUpdating] = useState(false);
  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const deleteCategory = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    httpClient
      .delete<never, DeleteItemResponse>({
        url: getApiUrlForId(FiltersEndpoints.DeleteFilterById, id),
        requiresToken: true,
      })
      .then((response) => {
        triggerRefresh();
        closeModal();
      })
      .catch((err) => {
        setIsUpdating(false);
        console.log(err);
      });
  };

  return (
    <EditVideoTitleContainer>
      <TitleRow>Are you sure, that you want to remove {name} filter?</TitleRow>
      <ButtonsContainer>
        <Button variant={getButtonVariant('danger')} onClick={deleteCategory}>
          {FiltersPageStrings.DeleteFilter}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
