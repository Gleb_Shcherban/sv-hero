import React, { useRef, useState } from 'react';
import PlayIcon from '../../common/assets/playIcon.png';
import { Video } from '../videoTool/VideoUploadModal';

import { PlayVideoImg } from '../../common/styles/commonStyles.style';
import { VideoItem, VideoWrapper } from './filters.style';

interface VideoComponentProps {
  video: Video;
}

export const VideoComponent: React.FC<VideoComponentProps> = ({ video }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  const onClickHandler = (e: React.MouseEvent<HTMLVideoElement>) => {
    const videoElement = e.target as HTMLVideoElement;
    const refVideoCurrent = videoRef.current;

    if (refVideoCurrent && refVideoCurrent.paused) {
      videoElement.play();
    } else {
      videoElement.pause();
      setIsPlaying(false);
    }
  };

  const onPlay = () => {
    setIsPlaying(true);
  };

  const onEnd = () => {
    setIsPlaying(false);
  };

  return (
    <VideoWrapper>
      <VideoItem
        onClick={onClickHandler}
        src={video.url}
        ref={videoRef}
        onPlay={onPlay}
        onEnded={onEnd}
      />
      {!isPlaying && <PlayVideoImg src={PlayIcon} />}
    </VideoWrapper>
  );
};
