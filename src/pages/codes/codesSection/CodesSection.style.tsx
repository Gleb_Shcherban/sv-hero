import styled from 'styled-components';
import { MOBILE_MAX_WIDTH } from '../../../common/constants/constants';

export const CodesRemaining = styled.div`
  margin-top: 43px;
  font-size: 14px;
  font-weight: 500;
  color: #6d7278;
`;

export const CommonTableNewWrapper = styled.div`
  max-width: 550px;
  margin-top: 18px;

  @media (max-width: ${MOBILE_MAX_WIDTH}px) {
    max-width: 100%;
  }
`;
