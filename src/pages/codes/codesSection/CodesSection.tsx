import React from 'react';

import { useAppDispatch, useTypedSelector } from '../../../store';
import { goToSelectedPage } from '../../../store/slices/codes';

import { createCodesTableContent } from '../../../services/helpers/tableMappers';

import { CommonTableNew } from '../../../common/components/table/CommonTableNew';
import {
  convertApiPageToFrontEndPage,
  convertFrontEndPageToApiPage,
} from '../../../services/utilities';

import { CodesRemaining, CommonTableNewWrapper } from './CodesSection.style';

interface ICodesSection {}

export const CodesSection: React.FC<ICodesSection> = () => {
  const dispatch = useAppDispatch();
  const { isLoading, error, items, page, totalPages, totalItems } = useTypedSelector(
    (state) => state.codes
  );

  const onGoToPage = (targetPage: number) => {
    dispatch(goToSelectedPage(convertFrontEndPageToApiPage(targetPage)));
  };

  const userDataTableContents = createCodesTableContent({
    items,
  });

  return (
    <>
      <CodesRemaining>Codes Remaining ({totalItems}) </CodesRemaining>

      <CommonTableNewWrapper>
        <CommonTableNew
          content={userDataTableContents}
          page={convertApiPageToFrontEndPage(page)}
          totalItems={totalItems}
          totalPages={totalPages}
          isLoading={isLoading}
          noContent={error}
          tablePadding="0"
          goToPage={onGoToPage}
        />
      </CommonTableNewWrapper>
    </>
  );
};
