import React, { useEffect } from 'react';

import { useAppDispatch, useTypedSelector } from '../../store';
import { updateCodes, getCodes, updateTable } from '../../store/slices/codes';

import { SearchPanel } from '../../common/components/searchPanel/SearchPanel';
import { UploadCodesButton } from './uploadCodesButton/UploadCodesButton';
import { CodesSection } from './codesSection/CodesSection';

import { PageContentContainer } from '../../common/styles/commonStyles.style';

export const CodesPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const { page, size, lastUpdated, errorUploadCodes } = useTypedSelector((state) => state.codes);

  useEffect(() => {
    dispatch(getCodes({ page, size }));
  }, [dispatch, lastUpdated, page, size]);

  const onChangeUploadedCodesHandler = (codes: string[]) => {
    dispatch(updateCodes(codes));
    dispatch(updateTable());
  };
  
  return (
    <PageContentContainer>
      <SearchPanel onClickSearch={() => {}} value={''} />
      <UploadCodesButton onChange={onChangeUploadedCodesHandler} error={errorUploadCodes} />
      <CodesSection />
    </PageContentContainer>
  );
};
