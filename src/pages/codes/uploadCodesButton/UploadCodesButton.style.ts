import styled from 'styled-components';

export const StyledLabel = styled.label`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 70px;
  width: 335px;
  height: 50px;
  background-color: #274be8;
  border-radius: 19px;
  color: var(--white);
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  cursor: pointer;

  input {
    display: none;
  }
`;

export const ErrorText = styled.div`
  color: var(--error);
`;
