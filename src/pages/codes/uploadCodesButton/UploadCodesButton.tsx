import React from 'react';

import { parseCVSFileField } from './UploadCodesButton.helper';

import { StyledLabel, ErrorText } from './UploadCodesButton.style';

interface IUploadCodesButton {
  error: string;
  onChange: (codes: string[]) => void;
}

export const UploadCodesButton: React.FC<IUploadCodesButton> = ({ onChange, error }) => {
  const onChangeHandler = async (event: React.ChangeEvent<HTMLInputElement>) => {
    parseCVSFileField(event).then((result) => {
      onChange(result);
    });
  };
  
  const ErrorMessage = () => {
    return error && <ErrorText>{error}</ErrorText>;
  };

  return (
    <>
      <StyledLabel id="file">
        UPLOAD CODES CSV
        <input name="file" type="file" accept=".csv" onChange={onChangeHandler} />
      </StyledLabel>
      {ErrorMessage()}
    </>
  );
};
