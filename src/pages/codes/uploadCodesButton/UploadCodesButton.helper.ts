const checkCodesOnLength = (codes: string[]) => {
  return codes.filter((code: string) => {
    const minLength = 4;
    const maxLength = 6;
    const codeLength = code.length as number;
    return codeLength >= minLength && codeLength <= maxLength;
  });
};

export const parseCVSFileField = (
  event: React.ChangeEvent<HTMLInputElement>
): Promise<string[]> => {
  return new Promise((resolve, reject) => {
    const target = event.target as HTMLInputElement;
    const file: File = (target.files as FileList)[0];
    const reader = new FileReader();
    reader.onload = () => {
      const readerResult = reader.result as string;
      const codes = readerResult.split(';');
      const serializedValues = checkCodesOnLength(codes);
      resolve(serializedValues);
    };

    reader.onerror = () => {
      reject([]);
    };

    reader.readAsBinaryString(file);
  });
};
