import React, { useState } from 'react';
import { httpClient } from '../../services/httpClient/httpClient';
import { EditVideoTitleContainer, TitleRow } from './videoTool.style';
import {
  MonetizationPageStrings,
  VideosPageStrings,
} from '../../common/localization/en';
import { InputField } from '../../common/components/inputField/InputField';
import { Button } from 'react-bootstrap';
import { ButtonsContainer } from '../monetization/editItemForms/EditForm.style';
import { PatchStockVideo, VideosApiModel } from '../../api/models/videos';
import { getApiUrlForId, StockVideosEndpoints } from '../../api/endpoints';
import { DeleteItemResponse } from '../../api/models/common';
import { Video } from './Video';

interface EditVideoTitleModalProps {
  id: string;
  uri: string;
  src: string;
  name: string;
  closeModal: () => void;
  triggerRefresh: () => void;
}

export const EditVideoTitleModal: React.FC<EditVideoTitleModalProps> = ({
  id,
  src,
  name,
  closeModal,
  triggerRefresh,
}) => {
  const [title, setTitle] = useState(name);
  const [isUpdating, setIsUpdating] = useState(false);
  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const updateVideoTitle = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    httpClient
      .patch<PatchStockVideo, VideosApiModel>({
        url: getApiUrlForId(StockVideosEndpoints.PatchVideo, id),
        requiresToken: true,
        payload: {
          name: title,
        },
      })
      .then(() => {
        triggerRefresh();
        closeModal();
      })
      .catch((e) => {
        setIsUpdating(false);
        console.log('Error, while updating video: ', e);
      });
  };

  const deleteVideo = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    httpClient
      .delete<never, DeleteItemResponse>({
        url: getApiUrlForId(StockVideosEndpoints.DeleteVideo, id),
        requiresToken: true,
      })
      .then(() => {
        triggerRefresh();
        closeModal();
      })
      .catch((e) => {
        setIsUpdating(false);
        console.log('Error, while deleting video: ', e);
      });
  };

  return (
    <EditVideoTitleContainer>
      <TitleRow>
        <InputField
          name="videoTitle"
          headerText={VideosPageStrings.ModalEditVideoTitle}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          placeholder={MonetizationPageStrings.EnterName}
        />
        <Video url={src} />
      </TitleRow>
      <ButtonsContainer>
        <Button
          variant={getButtonVariant('primary')}
          onClick={updateVideoTitle}
        >
          {VideosPageStrings.SaveChanges}
        </Button>
        <Button variant={getButtonVariant('danger')} onClick={deleteVideo}>
          {VideosPageStrings.DeleteVideo}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
