import React, { useState } from 'react';
import { httpClient } from '../../services/httpClient/httpClient';
import { EditVideoTitleContainer, TitleRow } from './videoTool.style';
import { VideosPageStrings } from '../../common/localization/en';
import { Button } from 'react-bootstrap';
import { ButtonsContainer } from '../monetization/editItemForms/EditForm.style';
import { CampaignsEndpoints, getApiUrlForId } from '../../api/endpoints';
import { DeleteItemResponse } from '../../api/models/common';

interface RemoveCategoryModalProps {
  id: string;
  title: string;
  closeModal: () => void;
  triggerRefresh: () => void;
}

//TODO: Refactor. As an example take a look at RemoveRewardModal.tsx
export const RemoveCategoryModal: React.FC<RemoveCategoryModalProps> = ({
  id,
  title,
  closeModal,
  triggerRefresh,
}) => {
  const [isUpdating, setIsUpdating] = useState(false);
  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const deleteCategory = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    httpClient
      .delete<never, DeleteItemResponse>({
        url: getApiUrlForId(CampaignsEndpoints.DeleteCampaignById, id),
        requiresToken: true,
      })
      .then((response) => {
        triggerRefresh();
        closeModal();
      })
      .catch((err) => {
        setIsUpdating(false);
        console.log(err);
      });
  };

  return (
    <EditVideoTitleContainer>
      <TitleRow>Are you sure, that you want to remove {title} category?</TitleRow>
      <ButtonsContainer>
        <Button variant={getButtonVariant('danger')} onClick={deleteCategory}>
          {VideosPageStrings.DeleteCategory}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
