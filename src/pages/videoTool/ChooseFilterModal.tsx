import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { ChosenCategoryType } from '.';
import { getApiUrlForId, CampaignsEndpoints } from '../../api/endpoints';
import { FilterApiModel } from '../../api/models/filters';
import { Spinner } from '../../common/assets/Spinner';
import { httpClient } from '../../services/httpClient/httpClient';
import { GetFilters } from '../../store/slices/videosSlice';
import {
  FilterInfoWrapper,
  FilterVariantItem,
  FilterWrapper,
  VariantDescription,
  VariantsContainer,
  VariantWrapper,
} from '../filters/filters.style';
import { BackIcon, CampaignLabel, CampaignValue, ModalHeader, SubmitModalButton } from './videoTool.style';

const customStyles = {
  content: {
    borderRadius: 8,
    backgroundColor: '#fff',
  },
  overlay: {
    zIndex: 2
  }
}

interface ChooseFilterModalProps {
  isOpen: boolean;
  handleClose: () => void;
  chosenCategory: ChosenCategoryType;
  updateContent: () => void;
}

export const ChooseFilterModal: React.FC<ChooseFilterModalProps> = ({
  isOpen, handleClose, chosenCategory, updateContent
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [filtersList, setFiltersList] = useState<FilterApiModel[]>([])

  useEffect(() => {
    setIsLoading(true);
    GetFilters()
      .then((filters) => {
        setFiltersList(filters.items);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setIsLoading(false);
      });
  }, [setFiltersList, setIsLoading]);


  const handleChoose = (id: string, campaignId: string) => {
    setIsLoading(true);
    httpClient.put<{videoFilterId: string}, {id: string}>({
      url: `${getApiUrlForId(CampaignsEndpoints.UpdateCampaignFilter, campaignId)}?videoFilterId=${id}`,
      requiresToken: true
    }).then((response) => {
      setIsLoading(false);
      updateContent();
      handleClose();
    }).catch(err => {
      console.log(err);
      setIsLoading(false);
      handleClose();
    })
  }

  return (
    <Modal isOpen={isOpen} style={customStyles} ariaHideApp={false}>
      <div>
        <BackIcon
          className="fa fa-arrow-left"
          style={{ marginBottom: 20 }}
          onClick={handleClose}
        />
      </div>
      {isLoading && (
        <Spinner  color="var(--spinnerColor)" />
      )}
      {!isLoading && !!filtersList.length && filtersList.map((filter) => (
        <FilterWrapper key={filter.id} style={{
          border: '1px solid #eee',
          borderColor: (chosenCategory && chosenCategory.filterId === filter.id) ? '#0ce' : '#eee'
        }}>
          <ModalHeader>
            <FilterInfoWrapper>
              <CampaignLabel>Filter Name</CampaignLabel>
              <CampaignValue style={{ textTransform: 'uppercase', color: '#222' }}>{filter.name}</CampaignValue>
            </FilterInfoWrapper>
            {chosenCategory?.filterId !== filter.id && (
              <SubmitModalButton onClick={() => {
              chosenCategory && handleChoose(filter.id, chosenCategory.campaignId)
            }}>Apply filter</SubmitModalButton>
            )}
          </ModalHeader>
          {/* <div>
            <button onClick={}>apply</button>
          </div> */}
          <div>
            <CampaignLabel style={{ marginBottom: 10 }}>Filters</CampaignLabel>
            <VariantsContainer>
              {filter.variations.map(variant => (
                <VariantWrapper key={variant.imageUri}>
                  <FilterVariantItem  value={variant.imageUri} />
                  <VariantDescription>
                    {variant.type}
                  </VariantDescription>
                </VariantWrapper>
              ))}
            </VariantsContainer>
          </div>
        </FilterWrapper>
      ))}
    </Modal>
  )
}
