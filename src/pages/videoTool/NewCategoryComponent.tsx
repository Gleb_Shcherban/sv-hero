import React, { memo, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Spinner } from '../../common/assets/Spinner';
import { InlineInput } from '../../common/components/inlineInput';
import { createCampaignRequest } from '../../store/slices/campaignsSlice';
import { NewCategoryWrapper } from './videoTool.style';

interface NewCategoryComponentProps {
  handleClose: () => void;
  updateContent: () => void;
}

export const NewCategoryComponent: React.FC<NewCategoryComponentProps> = memo(
  ({ handleClose, updateContent }) => {
    const [categoryName, setCategoryName] = useState('');
    const [newDescription, setNewDescription] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const handleSumitCategory = async () => {
      setIsLoading(true);
      createCampaignRequest({
        description: newDescription,
        title: categoryName,
      })
        .then((response) => {
          setIsLoading(false);
          handleClose();
          updateContent();
        })
        .catch((err) => {
          console.log(err);
          setIsLoading(false);
        });
    };

    if (isLoading) {
      return (
        <div>
          <Spinner color="var(--spinnerColor)" />
        </div>
      );
    }

    return (
      <NewCategoryWrapper>
        <div>
          <InlineInput
            tag="h1"
            value={categoryName}
            placeholder="New Category"
            setValue={setCategoryName}
            tagStyles={{
              fontSize: 32,
              fontWeight: 'bold',
              margin: '12px 0',
              padding: '8px 0',
              border: '2px solid transparent',
            }}
            inputStyles={{
              fontSize: 32,
              fontWeight: 'bold',
              margin: '12px 0',
            }}
          />
        </div>
        <div>
          <InlineInput
            tag="div"
            value={newDescription}
            placeholder="New Description"
            setValue={setNewDescription}
            inputStyles={{
              marginBottom: '24px',
            }}
            tagStyles={{
              marginBottom: '24px',
              padding: '8px 0',
              border: '2px solid transparent',
            }}
          />
        </div>
        <div style={{ display: 'flex' }}>
          <Button
            style={{ marginRight: 8 }}
            variant="success"
            disabled={!categoryName || !newDescription}
            onClick={handleSumitCategory}
          >
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Discard
          </Button>
        </div>
      </NewCategoryWrapper>
    );
  }
);
