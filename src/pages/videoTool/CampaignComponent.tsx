import React, { useCallback, useState } from 'react';
import { Campaign } from '.';
import { EditVideoTitleModal } from './EditVideoTitleModal';
import {
  CampaignInfoWrapper,
  CampaignLabel,
  CampaignValue,
  CampaignWrapper,
  // EditCampaignIcon,
  EditIcon,
  FiltersWrapper,
  VideoCard,
  VideoCardWrapper,
  VideosRow,
  VideosRowWrapper,
  ControlsWrapper,
  CtaIcon,
} from './videoTool.style';
import { FSModal } from '../../common/components/modal/Modal';
import { VideosPageStrings } from '../../common/localization/en';
import { RemoveCategoryModal } from './RemoveCategoryModal';
import { EditCategoryModal } from './EditCategoryModal';

interface CampaignComponentProps {
  campaign: Campaign;
  handleAddVideo: () => void;
  removeCampaign: (id: string) => void;
  handleFilterModal: (
    item: { filterId: string | null; campaignId: string } | null
  ) => void;
  setCampaign: (campaignCampaign: Campaign) => void;
  refreshData: () => void;
}

const CampaignComponent: React.FC<CampaignComponentProps> = ({
  campaign,
  handleAddVideo,
  removeCampaign,
  handleFilterModal,
  setCampaign,
  refreshData,
}) => {
  const { id, name, description, videos, videoFilter } = campaign;

  interface EditVideoTitleModalData {
    modalIsOpen: boolean;
    id: string;
    uri: string;
    name: string;
    src: string;
  }
  const [
    editVideoTitleModalData,
    setEditVideoTitleModalData,
  ] = useState<EditVideoTitleModalData | null>(null);

  const [deleteCampaignModalOpen, setDeleteCampaignModalOpen] = useState(false);
  const [editCampaignModalOpen, setEditCampaignModalOpen] = useState(false);

  const handleSetCampaign = () => {
    setCampaign(campaign);
    handleAddVideo();
  };

  const handleVideoClick = (
    id: string,
    uri: string,
    name: string,
    src: string
  ) => {
    setEditVideoTitleModalData({ id, uri, name, modalIsOpen: true, src });
  };

  const handleFilter = useCallback(() => {
    handleFilterModal({
      campaignId: id,
      filterId: videoFilter ? videoFilter.id : null,
    });
  }, [handleFilterModal, id, videoFilter]);

  return (
    <CampaignWrapper>
      <ControlsWrapper>
        <CtaIcon
          className="fa fa-pen"
          onClick={() => {
            setEditCampaignModalOpen(true);
          }}
        />
        <CtaIcon
          className="fa fa-trash"
          onClick={() => {
            setDeleteCampaignModalOpen(true);
          }}
        />
      </ControlsWrapper>
      <CampaignInfoWrapper>
        <CampaignLabel>Title</CampaignLabel>
        <CampaignValue style={{ textTransform: 'uppercase', color: '#222' }}>
          {name}
        </CampaignValue>
        <CampaignLabel>Description</CampaignLabel>
        <CampaignValue>{description}</CampaignValue>
        <CampaignLabel>{VideosPageStrings.ShareCopyTitle}</CampaignLabel>
        <CampaignValue>
          Get your free box from @username using my code above @social_venu
        </CampaignValue>
        {/* <EditCampaignIcon className="fa fa-pen" /> */}
      </CampaignInfoWrapper>
      <div>
        <VideosRowWrapper>
          <FiltersWrapper>
            <VideoCardWrapper>
              {videoFilter ? (
                <>
                  <VideoCard imageLink={videoFilter.variations[0].imageUri} />
                  <EditIcon className="fa fa-pen" onClick={handleFilter} />
                </>
              ) : (
                <VideoCard className="c-pointer" onClick={handleFilter}>
                  <i className="fa fa-plus" />
                </VideoCard>
              )}
            </VideoCardWrapper>
          </FiltersWrapper>
          <VideosRow>
            {videos.map(({ id, uri, name, src }) => (
              <VideoCardWrapper key={id}>
                <VideoCard imageLink={uri} />
                <EditIcon
                  className="fa fa-pen"
                  onClick={() => handleVideoClick(id, uri, name, src)}
                />
              </VideoCardWrapper>
            ))}
          </VideosRow>
          <div style={{ padding: '20px 0', marginLeft: 20 }}>
            <VideoCard
              className="c-pointer"
              onClick={handleSetCampaign}
              disabled={!videoFilter}
            >
              <i className="fa fa-plus" />
            </VideoCard>
          </div>
        </VideosRowWrapper>
      </div>
      <FSModal
        modalIsOpen={editVideoTitleModalData?.modalIsOpen || false}
        onClose={() => setEditVideoTitleModalData(null)}
      >
        <EditVideoTitleModal
          id={editVideoTitleModalData?.id || ''}
          uri={editVideoTitleModalData?.uri || ''}
          name={editVideoTitleModalData?.name || ''}
          src={editVideoTitleModalData?.src || ''}
          closeModal={() => setEditVideoTitleModalData(null)}
          triggerRefresh={refreshData}
        />
      </FSModal>
      <FSModal
        modalIsOpen={editCampaignModalOpen}
        onClose={() => setEditCampaignModalOpen(false)}
      >
        <EditCategoryModal
          id={id}
          oldTitle={name}
          oldDescription={description}
          closeModal={() => setEditCampaignModalOpen(false)}
          triggerRefresh={refreshData}
        />
      </FSModal>
      <FSModal
        modalIsOpen={deleteCampaignModalOpen}
        onClose={() => setDeleteCampaignModalOpen(false)}
      >
        <RemoveCategoryModal
          id={id}
          title={name}
          closeModal={() => setDeleteCampaignModalOpen(false)}
          triggerRefresh={refreshData}
        />
      </FSModal>
    </CampaignWrapper>
  );
};

export default React.memo(CampaignComponent);
