import { MediaEndpoints } from "../../api/endpoints";
import { httpClient } from "../../services/httpClient/httpClient";

export async function fileFromUrl(url: string) {
  return httpClient.post<
    { url: string; isStockContent: boolean; prefix: string },
    { url: string }
  >({
    url: MediaEndpoints.UploadFileFromUrl,
    requiresToken: true,
    payload: {
      isStockContent: true,
      prefix: 'videos/',
      url: url,
    },
  });
}
