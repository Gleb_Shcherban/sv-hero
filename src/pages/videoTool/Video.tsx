import React, { useRef, useState } from 'react';
import { PlayIcon, StyledVideo, VideoContainer } from './Video.style';
import Play from '../../common/assets/playIcon.png';

interface VideoProps {
  url: string;
}

export const Video: React.FC<VideoProps> = ({ url }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  const onClickHandler = (e: React.MouseEvent<HTMLVideoElement>) => {
    const videoElement = e.target as HTMLVideoElement;
    const refVideoCurrent = videoRef.current;

    if (refVideoCurrent && refVideoCurrent.paused) {
      videoElement.play();
    } else {
      videoElement.pause();
      setIsPlaying(false);
    }
  };

  const onPlay = () => {
    setIsPlaying(true);
  }

  const onEnd = () => {
    setIsPlaying(false);
  }

  return (
    <VideoContainer>
      <StyledVideo
        src={url}
        onClick={onClickHandler}
        ref={videoRef}
        onPlay={onPlay}
        onEnded={onEnd}
      />
      {!isPlaying && (
        <PlayIcon src={Play} />
      )}
    </VideoContainer>
  )
}
