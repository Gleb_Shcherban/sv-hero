import React, { useCallback, useEffect, useState } from 'react';
import { CampaignsEndpoints, getApiUrlForId } from '../../api/endpoints';
import { Spinner } from '../../common/assets/Spinner';
import { httpClient } from '../../services/httpClient/httpClient';
import { getCampaignsRequest } from '../../store/slices/campaignsSlice';
import { getVideoThumbnail } from '../../store/slices/utils';
import { getVideosRequest } from '../../store/slices/videosSlice';
import CampaignComponent from './CampaignComponent';
import { ChooseFilterModal } from './ChooseFilterModal';
import { NewCategoryComponent } from './NewCategoryComponent';
import VideoUploadModal from './VideoUploadModal';

import {
  PageTitle,
  VideoToolContainer,
  CtaButtonWrapper,
  CtaButton,
} from './videoTool.style';
import { FilterApiModel } from '../../api/models/filters';

interface Ivideo {
  id: string;
  campaignId: string | null;
  uri: string;
  src: string;
  name: string;
}
export interface Campaign {
  id: string;
  description: string;
  name: string;
  videos: Ivideo[];
  videoFilter: FilterApiModel | null;
  published: boolean;
}

export type ChosenCategoryType = {
  filterId: string | null;
  campaignId: string;
} | null;

// TODO: Refactor all this code ASAP
export const VideoTool: React.FC = () => {
  const [campaignsList, setCampaignsList] = useState<Campaign[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isCreateCategory, setIsCreateCategory] = useState(false);
  const [isFilterModalOpen, setIsFilterModalOpen] = useState(false);
  const [chosenCategory, setChosenCategory] = useState<ChosenCategoryType>(
    null
  );
  const [currentCampaign, setCurrentCampaign] = useState<Campaign | null>(null);

  const fetchCampaignsData = useCallback(() => {
    setIsLoading(true);
    Promise.all([
      getCampaignsRequest(),
      getVideosRequest({ size: 10000, page: 0 }),
    ])
      .then((response) => {
        const [campaingsResponse, videosResponse] = response;
        const normalizedVideos = videosResponse.items.map(
          ({ id, campaignId, uri, name }) => {
            return {
              id,
              campaignId,
              uri: getVideoThumbnail(uri),
              src: uri,
              name,
            };
          }
        );
        const normalizedCampaings = campaingsResponse.items.map(
          ({ id, title, description, published, videoFilter, createdAt }) => {
            const videos = normalizedVideos.filter((i) => i.campaignId === id);
            return {
              id,
              description,
              name: title,
              videos,
              published,
              videoFilter,
              createdAt,
            };
          }
        );
        setIsLoading(false);
        setCampaignsList(normalizedCampaings);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [setCampaignsList, setIsLoading]);

  const removeCampaign = useCallback(
    (id: string) => {
      setIsLoading(true);

      // @ts-ignore
      httpClient
        .delete<null, any>({
          url: getApiUrlForId(CampaignsEndpoints.DeleteCampaignById, id),
          requiresToken: true,
        })
        .then((response) => {
          console.log('delete', response);
          setIsLoading(false);
          fetchCampaignsData();
        })
        .catch((err) => {
          setIsLoading(false);
          console.log(err);
        });
    },
    [setIsLoading, fetchCampaignsData]
  );

  const startAddVideo = useCallback(() => {
    setIsModalOpen(true);
  }, [setIsModalOpen]);

  const handleCloseModal = useCallback(() => {
    setIsModalOpen(false);
  }, [setIsModalOpen]);

  useEffect(() => {
    fetchCampaignsData();
  }, [fetchCampaignsData]);

  const handleFilterModal = useCallback(
    (item: ChosenCategoryType) => {
      setIsFilterModalOpen(true);
      setChosenCategory(item);
    },
    [setIsFilterModalOpen, setChosenCategory]
  );

  const handleCloseFilterModal = useCallback(() => {
    setChosenCategory(null);
    setIsFilterModalOpen(false);
  }, [setChosenCategory, setIsFilterModalOpen]);

  return (
    <VideoToolContainer>
      {!isLoading && (
        <>
          <PageTitle>Videos</PageTitle>
          <CtaButtonWrapper>
            <CtaButton
              onClick={() => {
                setIsCreateCategory(true);
              }}
            >
              New Category
            </CtaButton>
          </CtaButtonWrapper>
        </>
      )}
      {isCreateCategory && (
        <NewCategoryComponent
          handleClose={() => {
            setIsCreateCategory(false);
          }}
          updateContent={fetchCampaignsData}
        />
      )}
      {isLoading && <Spinner color="var(--spinnerColor)" />}
      {!isLoading &&
        campaignsList &&
        campaignsList.map((campaign) => (
          <CampaignComponent
            key={campaign.id}
            campaign={campaign}
            handleAddVideo={startAddVideo}
            removeCampaign={removeCampaign}
            handleFilterModal={handleFilterModal}
            setCampaign={setCurrentCampaign}
            refreshData={fetchCampaignsData}
          />
        ))}
      <VideoUploadModal
        isOpen={isModalOpen}
        handleClose={handleCloseModal}
        currentCampaign={currentCampaign}
        updateContent={fetchCampaignsData}
      />
      <ChooseFilterModal
        isOpen={isFilterModalOpen}
        handleClose={handleCloseFilterModal}
        chosenCategory={chosenCategory}
        updateContent={fetchCampaignsData}
      />
    </VideoToolContainer>
  );
};
