import styled from 'styled-components';
import { ReactComponent as Sequential } from './icons/sequential-icon.svg';

export const SequentialIcon = styled(Sequential)`
  border-radius: 6px;
  box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);
`;

export const CropVideoContainer = styled.div`
  margin: 0 10px;
  position: relative;
`;

export const CropVideosWrapper = styled.div`
  margin: 0px auto;
  margin-bottom: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const ZoomButton = styled.button`
  display: inline-block;
  font-size: 16px;
  font-weight: bold;
  padding: 8px 16px;
  border: 2px solid #222;
  border-radius: 6px;
  margin: 4px 4px;
`;

export const StepIcons = styled.div`
  display: inline-flex;
  padding-bottom: 30px;
`;

export const StepIconWrapper = styled.div`
  position: relative;
  margin-right: 40px;
  opacity: 0.33;
  transition: opacity .2s ease-in-out;
  cursor: pointer;

  &:last-child {
    margin-right: 0;
  }
  &.active-step {
    opacity: 1;
  }
`;

export const StepIconText = styled.div`
  display: inline-block;
  position: absolute;
  bottom: 0px;
  left: 50%;
  transform: translateX(-50%) translateY(25px);
`;


