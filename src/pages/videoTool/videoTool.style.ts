import styled from 'styled-components';

export const VideoToolContainer = styled.div`
  padding: 40px 20px;
  max-width: 1440px;
  margin: 0 auto;
  box-sizing: border-box;
  position: relative;
`;

export const PageTitle = styled.h1`
  font-size: 14px;
  color: #6d7278;
  margin-bottom: 15px;
`;

export const CampaignWrapper = styled.div`
  background-color: #fff;
  margin-bottom: 30px;
  border-radius: 19px;
  padding: 54px;
  box-sizing: border-box;
  position: relative;
`;

export const CampaignLabel = styled.div`
  font-size: 14px;
  color: #6d7278;
`;

export const CampaignValue = styled.div`
  font-size: 20px;
  color: #6d7278;
`;

export const VideoCardWrapper = styled.div`
  position: relative;
  margin-left: 20px;
  &:first-child {
    margin-left: 0;
  }
`;

interface IVideoCardProps {
  imageLink?: string;
  disabled?: boolean;
}

export const VideoCard = styled.div<IVideoCardProps>`
  min-width: 167px;
  min-height: 239px;
  background-color: #eaeaea;
  background-size: cover;
  background-position: center;
  box-shadow: 0 3px 7px 0px rgba(0, 0, 0, 0.2);
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  ${(props) => props.imageLink && `background-image: url(${props.imageLink});`}
  ${(props) =>
    props.disabled &&
    `
    opacity: 0.4;
    pointer-events: none;
  `}
`;

export const CampaignContent = styled.div`
  display: flex;
  align-items: flex-start;
`;

export const VideosRowWrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;

export const VideosRow = styled.div`
  display: flex;
  max-width: 100%;
  padding: 20px;
  overflow-x: hidden;
  /* overflow-x: scroll; */
  &:empty {
    display: none;
  }
  &:hover {
    overflow-x: scroll;
    margin-bottom: -30px;
  }
`;

export const CtaIcon = styled.i`
  display: inline-block;
  padding: 13px;
  margin-left: 24px;
  color: #222;
  font-size: 18px;
  border-radius: 50%;
  background-color: #fff;
  cursor: pointer;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.25);
  transition: all 0.2s ease-in-out;
  &:hover {
    color: #fff;
    background-color: #222;
  }
`;

export const EditIcon = styled(CtaIcon)`
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(45%, -45%);
`;

export const EditCampaignIcon = styled(CtaIcon)`
  position: absolute;
  top: 0;
  right: 0;
  transform: translateX(150%);
`;

export const BackIcon = styled(CtaIcon)``;

export const ControlsWrapper = styled.div`
  position: absolute;
  right: 17px;
  top: 19px;
`;

export const DragNDropWrapper = styled.div`
  max-width: 340px;
  margin: 0 auto;
`;

export const FiltersWrapper = styled.div`
  padding: 20px 20px 0 0;
`;

export const SubmitModalButton = styled.button`
  display: inline-block;
  width: 380px;
  height: 70px;
  border-radius: 19px;
  background-color: #274be8;
  color: #fff;
  text-align: center;
  font-weight: 600;
  font-size: 20px;
  text-transform: uppercase;
  &:disabled {
    opacity: 0.6;
  }
`;

export const ModalHeader = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

export const CtaButtonWrapper = styled.div`
  width: 100%;
  padding: 66px 54px;
  background-color: #fff;
  border-radius: 14px;
  margin-bottom: 30px;
`;

export const CtaButton = styled.button`
  background-color: #274be8;
  border-radius: 19px;
  color: #fff;
  display: inline-block;
  width: 100%;
  font-size: 20px;
  text-transform: uppercase;
  text-align: center;
  padding: 22px;
  font-weight: 600;
`;

export const NewCategoryWrapper = styled.div`
  background-color: #fff;
  border-radius: 19px;
  margin-bottom: 30px;
  padding: 54px;
`;

export const CampaignInfoWrapper = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: minmax(50px, 150px) 1fr;
  grid-row-gap: 10px;
  align-items: center;
  margin-bottom: 30px;
  //max-width: 500px;
`;

export const VideoItemWrapper = styled.div`
  position: relative;
  margin-left: 40px;
`;

export const VideoItem = styled.video`
  width: auto;
`;

export const EditVideoTitleContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const TitleRow = styled.div`
  display: flex;
`;
