import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import axios, { AxiosResponse } from 'axios';
import Modal from 'react-modal';
import { ProgressBar } from 'react-bootstrap';
import { DragAndDrop } from '../../common/components/dragAndDrop/DragAndDrop';
import {
  DNDActionTypes,
  useDragAndDropReducer,
} from '../../common/components/dragAndDrop/DragAndDropState';
import {
  DragNDropWrapper,
  ModalHeader,
  SubmitModalButton,
  BackIcon,
  CampaignInfoWrapper,
  CampaignLabel,
} from './videoTool.style';
import CropVideosComponent from './CropVideosComponent';
import { httpClient } from '../../services/httpClient/httpClient';
import { StockVideosEndpoints } from '../../api/endpoints';
import { Campaign } from '.';
import { GetFilterById } from '../../store/slices/videosSlice';
import { Spinner } from '../../common/assets/Spinner';
import {
  getVideoCropUrl,
  getVideoOverlayLink,
  getVideoDimensions,
  getPaddedVideoUrl,
  getVideoPublicId,
  getVideoInMp4,
  getPaddedUrlForEdge,
} from '../../services/utilities';
import {
  VideoFilterVariation,
  VideoFilterVariationType,
} from '../../api/models/filters';
import { VideosRow } from '../filters/filters.style';
import { VideoComponent } from '../filters/VideoComponent';
import { InlineInput } from '../../common/components/inlineInput';
import {
  CropperSteps,
  useCropperReducer,
  CropSizes,
  CropperActionTypes,
} from '../testPage/cropperState';
import { useTypedSelector } from '../../store';
import { cloudinaryUploadLink, VIDEO_PADDING } from '../../common/constants/constants';
import { fileFromUrl } from './utils';
import { useToasts } from 'react-toast-notifications';

interface VideoUploadModalProps {
  isOpen: boolean;
  handleClose: () => void;
  currentCampaign: Campaign | null;
  updateContent: () => void;
}

const customStyles = {
  content: {
    borderRadius: 8,
    backgroundColor: '#fff',
    width: '100vw',
    inset: 0,
  },
  overlay: {
    zIndex: 2,
  },
};

export interface Video {
  type: VideoFilterVariationType;
  url: string;
}

export type Dimensions = {
  width: number;
  height: number;
}

export type Croppings = {
  [value in VideoFilterVariationType]: string;
}

/* TODO: get it back when find solution */
// const getNormalizedDimensions = (dimensions: Dimensions): Dimensions => {
//   if (dimensions.height >= 2000) {
//     return {
//       width: dimensions.width * 0.35,
//       height: (dimensions.height + 200) * 0.35,
//     }
//   }
//   if (dimensions.height >= 1500) {
//     return {
//       width: dimensions.width * 0.45,
//       height: (dimensions.height + 200) * 0.45,
//     }
//   }
//   if (dimensions.height >= 1000) {
//     return {
//       width: dimensions.width * 0.5,
//       height: (dimensions.height + 200) * 0.5,
//     }
//   }
//   return dimensions;
// }

const VideoUploadModal: React.FC<VideoUploadModalProps> = ({
  isOpen,
  handleClose,
  currentCampaign,
  updateContent,
}) => {
  const [dndData, dndDispatch] = useDragAndDropReducer();
  const [cropData, cropDispatch] = useCropperReducer();
  const { venue } = useTypedSelector((state) => state.venue);

  const [videoUrl, setVideoUrl] = useState<string | null>(null);
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  const [progress, setProgress] = useState(0);
  const [isUploading, setIsUploading] = useState(false);
  const [publicId, setPublicId] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [variations, setVariations] = useState<VideoFilterVariation[] | null>(
    null
  );
  const [croppings, setCroppings] = useState<Croppings | null | {}>(null);
  const [videos, setVideos] = useState<Video[] | null>(null);
  const [videoName, setVideoName] = useState('');
  const [originalUrl, setOriginalUrl] = useState('');

  const { addToast } = useToasts();

  const showWarning = useCallback(() => {
    addToast('Something gone wrong. Please, try again', {
      appearance: 'warning',
      autoDismiss: true,
    });
  }, [addToast]);

  const resetDnd = useCallback(() => {
    dndDispatch({ type: DNDActionTypes.RESET_STATE });
  }, [dndDispatch]);

  useEffect(() => {
    if (!isOpen) {
      cropDispatch({ type: CropperActionTypes.RESET });
    }
  }, [isOpen, cropDispatch]);

  const handleDimensions = useCallback(async () => {
    const [data] = dndData.fileList;
    const { file } = data;

    const config = {
      onUploadProgress: function (progressEvent: ProgressEvent) {
        let percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setProgress(percentCompleted);
      },
    };

    const formData = new FormData();
    formData.append('upload_preset', 'lr8wafct');
    formData.append('file', file);

    setIsUploading(true);
    try {
      const response = await axios.post<
        FormData,
        AxiosResponse<{ url: string; public_id: string }>
      >(cloudinaryUploadLink, formData, config);

      let firstVideoUrl = response.data.url;
      let paddedUrl = '';

      setIsUploading(false);
      setIsLoading(true);

      const dimensions = await getVideoDimensions(getVideoInMp4(firstVideoUrl));

      if (!firstVideoUrl.includes('.mp4')) {
        const res = await fileFromUrl(getVideoInMp4(firstVideoUrl));
        paddedUrl = getPaddedUrlForEdge(res.url, dimensions, venue.subdomain);
      } else {
        paddedUrl = getPaddedVideoUrl(getVideoInMp4(firstVideoUrl), dimensions);
      }

      const data = await fileFromUrl(paddedUrl);
      const videoId = getVideoPublicId(data.url);

      setPublicId(videoId);
      setVideoUrl(getVideoInMp4(data.url));

      // setDimensions(getNormalizedDimensions(dimensions));
      setDimensions({
        width: dimensions.width,
        height: dimensions.height + VIDEO_PADDING,
      });
      setIsUploading(false);
      setIsLoading(false);
    } catch (err) {
      showWarning();
      setIsUploading(false);
      setIsLoading(false);
      resetDnd()
    }
  }, [
    setPublicId,
    setVideoUrl,
    setIsUploading,
    setIsLoading,
    setDimensions,
    setProgress,
    dndData,
    venue.subdomain,
    showWarning,
    resetDnd
  ]);

  /* retrive uploaded video dimensions and url string */
  useEffect(() => {
    if (!!dndData.fileList.length) {
      handleDimensions();
    }
  }, [dndData, handleDimensions]);

  const reset = () => {
    setVideoUrl(null);
    handleClose();
    resetDnd();
    setProgress(0);
    setVideos(null);
    setIsLoading(false);
    setVideoName('');
    setCroppings(null);
  };

  const cropVideo = async () => {
    let arr: { url: string; type: string }[] = [];
    const videosData: Video[] = [];

    for (let step in CropperSteps) {
      // @ts-ignore Key is present
      const data: CropSizes = cropData.sizes[step];
      const { height, width, x, y } = data;
      const url = getVideoCropUrl({
        height,
        width,
        x,
        y,
        id: publicId,
        subdomain: venue.subdomain,
      });
      const obj = { url: url, type: step };
      arr.push(obj);
    }

    setIsLoading(true);
    try {
      const croppingsObj: Croppings | {} = {};
      for (const i of arr) {
        await axios.get<{ url: string }>(i.url);
        const fileResponse = await fileFromUrl(i.url);

        if (i.type === 'BASE') {
          setOriginalUrl(fileResponse.url);
          continue;
        }
        // @ts-ignore
        // need to properly type when refactoring
        croppingsObj[i.type] = fileResponse.url;
        if (variations) {
          const variation = variations.find(
            (variation) => variation.type === i.type
          );
          if (variation) {
            const url = getVideoOverlayLink(
              fileResponse.url,
              variation.imageUri,
              venue.subdomain
            );
            videosData.push({ type: variation.type, url });
          }
        }
      }
      setVideos(videosData);
      setIsLoading(false);
      setCroppings(croppingsObj);
    } catch (err) {
      setIsLoading(false);
      showWarning();
    }
  };

  const handleCreateVideo = async () => {
    if (videos) {
      try {
        const obj = {};
        setIsLoading(true);

        for await (let video of videos) {
          const l = await fileFromUrl(video.url);
          // @ts-ignore
          obj[video.type] = l.url;
        }

        await httpClient.post({
          url: StockVideosEndpoints.CreateVideo2,
          requiresToken: true,
          payload: {
            name: videoName,
            uri: originalUrl,
            variations: obj,
            croppings,
            campaignId: currentCampaign?.id,
            description: null,
            actionLabel: '',
          },
        })
        setIsLoading(false);
        updateContent();
        reset();
      } catch (err) {
        setIsLoading(false);
        showWarning();
      }
    }
  };

  // fetch filter variations of the category
  useEffect(() => {
    if (currentCampaign && currentCampaign.videoFilter) {
      GetFilterById(currentCampaign.videoFilter.id)
        .then((response) => {
          setVariations(response.variations);
        })
        .catch((error) => console.log(error));
    }
  }, [currentCampaign, setVariations]);

  // validate that every cropping variation is correct
  const isStepsCropValid = useMemo(() => {
    const values: boolean[] = [];

    Object.values(CropperSteps).forEach((value => {
      const { x, y } =  cropData.sizes[value]
      values.push(Boolean(x || y));
    }));

    return values.every(i => !!i);
  }, [cropData.sizes]);

  if (videos) {
    return (
      <Modal style={customStyles} isOpen={isOpen} ariaHideApp={false}>
        <ModalHeader>
          <BackIcon
            className="fa fa-arrow-left"
            onClick={() => {
              resetDnd();
              setVideos(null);
              setVideoName('');
            }}
          />
          <SubmitModalButton
            disabled={!videoName || isLoading}
            onClick={handleCreateVideo}
          >
            Apply Filter
          </SubmitModalButton>
        </ModalHeader>
        <CampaignInfoWrapper style={{ padding: '20px 0', margin: 0 }}>
          <CampaignLabel>Video Name</CampaignLabel>
          <InlineInput
            tag="div"
            tagStyles={{
              border: '2px solid transparent',
              textTransform: 'uppercase',
            }}
            placeholder="Enter video name"
            value={videoName}
            setValue={setVideoName}
          />
        </CampaignInfoWrapper>
        {isLoading && <Spinner color="var(--spinnerColor)" />}
        <VideosRow>
          {!isLoading &&
            videos.map((v) => (
              <div key={v.url}>
                <VideoComponent video={v} />
                <div
                  style={{
                    paddingTop: 14,
                    fontSize: 20,
                    color: '#6D7278',
                    textAlign: 'center',
                  }}
                >
                  {v.type}
                </div>
              </div>
            ))}
        </VideosRow>
      </Modal>
    );
  }

  return (
    <Modal style={customStyles} isOpen={isOpen} ariaHideApp={false}>
      <ModalHeader>
        <BackIcon className="fa fa-arrow-left" onClick={reset} />
        <SubmitModalButton
          disabled={!videoUrl || isLoading || !isStepsCropValid}
          onClick={cropVideo}
        >
          Crop video
        </SubmitModalButton>
      </ModalHeader>
      {!isLoading && videoUrl && !isUploading && (
        <CropVideosComponent
          onClose={reset}
          videoUrl={videoUrl}
          dimensions={dimensions}
          variations={variations}
          data={cropData}
          dispatch={cropDispatch}
        />
      )}
      {!videoUrl && !isUploading && !isLoading && (
        <DragNDropWrapper>
          <DragAndDrop
            data={dndData}
            dispatch={dndDispatch}
            resendMediaFile={(file) => {
              console.log(file);
            }}
          />
        </DragNDropWrapper>
      )}
      {isUploading && (
        <DragNDropWrapper>
          <div style={{ padding: '8px 0' }}>
            <ProgressBar now={progress} label={`${progress}%`} />
          </div>
        </DragNDropWrapper>
      )}
      {isLoading && (
        <DragNDropWrapper>
          <div style={{ padding: '8px 0' }}>
            <Spinner color="var(--spinnerColor)" />
          </div>
        </DragNDropWrapper>
      )}
    </Modal>
  );
};

export default memo(VideoUploadModal);
