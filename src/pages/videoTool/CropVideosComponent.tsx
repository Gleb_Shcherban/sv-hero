import React, { useMemo, useState, Dispatch, useEffect } from 'react';
import Cropper from 'react-easy-crop';
import { VideoFilterVariation } from '../../api/models/filters';
import {
  CropperActionTypes,
  CropperState,
  CropperAction,
  CropperSteps
} from '../testPage/cropperState';
import {
  CropVideoContainer,
  CropVideosWrapper,
  ZoomButton,
  StepIcons,
  StepIconWrapper,
  StepIconText,
  SequentialIcon
} from './CropVideosComponent.style';

import { ReactComponent as CircleIcon } from './icons/circle-icon.svg';
import { ReactComponent as BaseIcon } from './icons/base-icon.svg';
import { ReactComponent as SquareIcon } from './icons/square-icon.svg';

interface CropVideosComponentProps {
  dimensions: { width: number; height: number };
  videoUrl: string;
  onClose: () => void;
  variations: null | VideoFilterVariation[];
  data: CropperState,
  dispatch: Dispatch<CropperAction>
}

const CropVideosComponent: React.FC<CropVideosComponentProps> = ({
  dimensions,
  videoUrl,
  variations,
  data,
  dispatch,
}) => {
  const [zoomSize] = useState(1);
  const [crop, setCrop] = useState({ x: 0, y: 0 });

  const handleIncrease = () => {
    const { height, width } = data.areas[data.step];
    const newCropSize = {
      width: width * 1.1,
      height: height * 1.1,
    };
    if (newCropSize.height > dimensions.height || newCropSize.width > dimensions.width) {
      return;
    }
    dispatch({
      type: CropperActionTypes.SET_AREA_SIZE,
      payload: newCropSize,
    });
  };

  const handleDecrease = () => {
    const { height, width } = data.areas[data.step];
    const newCropSize = {
      width: width * 0.9,
      height: height * 0.9,
    };
    if (newCropSize.height < 300 || newCropSize.width < 300) {
      return;
    }
    dispatch({
      type: CropperActionTypes.SET_AREA_SIZE,
      payload: newCropSize,
    });
  };

  /* get crop area posotion on step change */
  useEffect(() => {
    setCrop(data.positions[data.step]);
  }, [data.step, setCrop, data.positions]);

  const filterLink = useMemo(() => {
    if (!variations) {
      return '';
    }
    // @ts-ignore They do overlap
    const val = variations.find((item) => item.type === data.step);
    return val ? val.imageUri : '';
  }, [variations, data.step]);

  const handleStep = (step: CropperSteps) => () => {
    dispatch({ type: CropperActionTypes.SET_STEP, payload: step});
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <StepIcons>
        <StepIconWrapper
          className={[data.step === CropperSteps.BASE ? 'active-step' : ''].join(' ')}
          onClick={handleStep(CropperSteps.BASE)}
        >
          <BaseIcon />
          <StepIconText>Base</StepIconText>
        </StepIconWrapper>
        <StepIconWrapper
          className={[data.step === CropperSteps.JUMBO ? 'active-step' : ''].join(' ')}
          onClick={handleStep(CropperSteps.JUMBO)}
        >
          <SquareIcon />
          <StepIconText>Square</StepIconText>
        </StepIconWrapper>
        <StepIconWrapper
          className={[data.step === CropperSteps.CIRCULAR ? 'active-step' : ''].join(' ')}
          onClick={handleStep(CropperSteps.CIRCULAR)}
        >
          <CircleIcon />
          <StepIconText>Circle</StepIconText>
        </StepIconWrapper>
        <StepIconWrapper
          className={[data.step === CropperSteps.SEQUENTIAL ? 'active-step' : ''].join(' ')}
          onClick={handleStep(CropperSteps.SEQUENTIAL)}
        >
          <SequentialIcon />
          <StepIconText>Sequential</StepIconText>
        </StepIconWrapper>
      </StepIcons>
      <div style={{ padding: '10px 0' }}>
        <div style={{ display: 'inline-flex', alignItems: 'center' }}>
          <ZoomButton onClick={handleIncrease}> + </ZoomButton>
          <ZoomButton onClick={handleDecrease}> - </ZoomButton>
        </div>
      </div>
      <CropVideosWrapper>
        <CropVideoContainer
          style={{
            width: dimensions.width,
            height: dimensions.height,
          }}
        >
          <Cropper
            style={{
              cropAreaStyle: {
                backgroundRepeat: 'no-repeat',
                backgroundImage: `url(${filterLink})`,
                backgroundSize: '100%',
              },
            }}
            restrictPosition={true}
            cropSize={data.areas[data.step]}
            // aspect={187 / 276}
            showGrid={false}
            video={videoUrl}
            zoom={zoomSize}
            crop={crop}
            onCropChange={setCrop}
            onCropComplete={(area, areaPixels) => {
              dispatch({
                type: CropperActionTypes.SET_CROP_SIZE,
                payload: areaPixels
              })
              dispatch({
                type: CropperActionTypes.SET_POSITION,
                payload: crop
              })
            }}
          />
        </CropVideoContainer>
      </CropVideosWrapper>
    </div>
  );
};

export default CropVideosComponent;
