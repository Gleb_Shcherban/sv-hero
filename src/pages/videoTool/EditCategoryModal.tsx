import React, { useState } from 'react';
import { httpClient } from '../../services/httpClient/httpClient';
import { EditVideoTitleContainer, TitleRow } from './videoTool.style';
import {
  MonetizationPageStrings,
  VideosPageStrings,
} from '../../common/localization/en';
import { InputField } from '../../common/components/inputField/InputField';
import { Button } from 'react-bootstrap';
import { ButtonsContainer } from '../monetization/editItemForms/EditForm.style';
import { CampaignsEndpoints, getApiUrlForId } from '../../api/endpoints';
import {
  CampaignsApiModel,
  PatchCampaignsApiModel,
} from '../../api/models/campaigns';

interface RemoveCategoryModalProps {
  id: string;
  oldTitle: string;
  oldDescription: string;
  closeModal: () => void;
  triggerRefresh: () => void;
}

export const EditCategoryModal: React.FC<RemoveCategoryModalProps> = ({
  id,
  oldTitle,
  oldDescription,
  closeModal,
  triggerRefresh,
}) => {
  const [isUpdating, setIsUpdating] = useState(false);
  const [title, setTitle] = useState(oldTitle);
  const [description, setDescription] = useState(oldDescription);

  const getButtonVariant = (variant: string) => {
    return isUpdating ? 'dark' : variant;
  };

  const editCategory = () => {
    setIsUpdating(true);
    if (isUpdating) return;
    const payload: PatchCampaignsApiModel = {
      title: title,
      description: description,
    };
    httpClient
      .patch<PatchCampaignsApiModel, CampaignsApiModel>({
        url: getApiUrlForId(CampaignsEndpoints.PatchCampaigns, id),
        requiresToken: true,
        payload,
      })
      .then((response) => {
        triggerRefresh();
        closeModal();
      })
      .catch((err) => {
        setIsUpdating(false);
        console.log(err);
      });
  };

  return (
    <EditVideoTitleContainer>
      <TitleRow>
        <InputField
          name="categoryName"
          headerText={VideosPageStrings.ModalEditCategoryTitle}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          placeholder={MonetizationPageStrings.EnterName}
        />
      </TitleRow>
      <br />
      <TitleRow>
        <InputField
          name="categoryDescription"
          headerText={VideosPageStrings.ModalEditCategoryDescription}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          placeholder={MonetizationPageStrings.EnterName}
        />
      </TitleRow>
      <ButtonsContainer>
        <Button variant={getButtonVariant('primary')} onClick={editCategory}>
          {VideosPageStrings.SaveChanges}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
