import React, { useEffect } from 'react';
import { CurrentActivity } from './UserDetailsModal';
import {
  ActivityDetailsWrapper,
  ActivityImagePreview,
  SectionContainer,
  SectionTitle,
} from './UserDetailsModal.style';
import ActivityDetailsVideo from './ActivityDetailsVideo';

interface ActivityDetailsComponentProps {
  data: CurrentActivity
}

const ActivityDetailsComponent: React.FC<ActivityDetailsComponentProps> = ({
  data
}) => {
  const { date, imageUrl, type, video } = data || {};

  useEffect(() => {
    if (data) {
      console.log(data);
    }
  }, [data]);

  return (
    <ActivityDetailsWrapper className={[data ? 'show-details' : ''].join(' ')}>
      <SectionTitle>Activity Details</SectionTitle>
      <SectionContainer>
        <SectionTitle>{type}</SectionTitle>
        {type === 'share video' && (
          <ActivityDetailsVideo id={video} />
        )}
        {type === 'reward' && (
          <ActivityImagePreview bgImage={imageUrl ? imageUrl : ''} >
            {imageUrl ? '' : 'Image is not available'}
          </ActivityImagePreview>
        )}
        <SectionTitle style={{ color: '#cacaca', marginBottom: 15 }}>Date requested</SectionTitle>
        <div>{date}</div>
      </SectionContainer>
    </ActivityDetailsWrapper>
  )
}

export default ActivityDetailsComponent;
