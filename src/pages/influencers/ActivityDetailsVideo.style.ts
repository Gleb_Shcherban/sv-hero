import styled from 'styled-components';

export const VideoContainer = styled.div`
  position: relative;
  min-width: 168px;
  min-height: 240px;
  box-shadow: 0 3px 7px 0px rgba(0,0,0,0.2);
  margin-bottom: 20px;
`;

export const StyledVideo = styled.video`
  width: 100%;
  border-radius: 5px;
`;

export const PlayIcon = styled.img`
  position: absolute;
  width: 40px;
  height: 40px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  pointer-events: none
`;
