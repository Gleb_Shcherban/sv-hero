import React, { useCallback, useEffect, useState, useRef } from 'react';

import { getApiUrlForId, UsersEndpoints } from '../../api/endpoints';
import { UserApiModel } from '../../api/models/users';
import { Spinner } from '../../common/assets/Spinner';
import { LocationSection } from '../../common/components/userDetailsSection/locationSection/LocationSection';
import { httpClient } from '../../services/httpClient/httpClient';
import ActivityDetailsComponent from './ActivityDetailsComponent';
import ActivityFeed from './ActivityFeed';
import { ModalOverlay, ModalContainer, ModalLayout } from './UserDetailsModal.style';
import { UserInfoComponent } from './UserInfoComponent';
import UserPoints from './UserPoints';

interface UserDetailsModalProps {
  userId: string;
  firstName: string | null;
  handleClose: () => void;
  fetchUserWithStats: () => void;
}

export type CurrentActivity = null | {
  id: string;
  type: string;
  imageUrl: string | null;
  date: string;
  video?: string | null;
};

export const UserDetailsModal: React.FC<UserDetailsModalProps> = ({
  userId,
  fetchUserWithStats,
  handleClose,
}) => {
  const scrollParentRef = useRef<HTMLInputElement | null>(null);

  const [user, setUser] = useState<UserApiModel | null>(null);
  const [updateActivityFeedRecords, setUpdateActivityFeedRecords] = useState<number>(0);
  const [currentActivity, setCurrentActivity] = useState<CurrentActivity>(null);

  const fetchUser = useCallback(() => {
    if (userId) {
      httpClient
        .get<null, UserApiModel>({
          url: getApiUrlForId(UsersEndpoints.GetUser, userId),
          requiresToken: true,
        })
        .then((response) => {
          setUser(response);
        });
    }
  }, [userId]);

  const updateActivityFeedRecordsHandler = useCallback(() => {
    const randomNumber = new Date().getTime();
    setUpdateActivityFeedRecords(randomNumber);
  }, []);

  const reloadInfluencerTable = useCallback(() => {
    fetchUser();
    updateActivityFeedRecordsHandler();
    fetchUserWithStats();
  }, [fetchUser, fetchUserWithStats, updateActivityFeedRecordsHandler]);

  useEffect(() => {
    fetchUser();
  }, [fetchUser]);

  if (!user) {
    return (
      <ModalOverlay>
        <Spinner color="#fff" />
      </ModalOverlay>
    );
  }

  return (
    <ModalOverlay onClick={handleClose}>
      <ModalContainer onClick={(e) => e.stopPropagation()}>
        <ModalLayout ref={scrollParentRef}>
          <div>
            <UserInfoComponent data={user} />
            <LocationSection properties={user.registrationIp} />
          </div>
          <div>
            <UserPoints data={user} triggerUpdate={reloadInfluencerTable} />

            <ActivityFeed
              updateActivityFeedRecords={updateActivityFeedRecords}
              scrollParentRef={scrollParentRef}
              id={user.id}
              currentActivity={currentActivity}
              setCurrentActivity={setCurrentActivity}
            />
          </div>
          <div>
            <ActivityDetailsComponent data={currentActivity} />
          </div>
        </ModalLayout>
      </ModalContainer>
    </ModalOverlay>
  );
};
