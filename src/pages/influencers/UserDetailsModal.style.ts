import styled from 'styled-components';
import { EditControls } from '../rewards/Rewards.style';

export const ModalOverlay = styled.div`
  position: fixed;
  background-color: rgba(0, 0, 0, 0.8);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
  overflow: hidden;
`;

export const ModalOverlayWithSpinner = styled(ModalOverlay)`
  z-index: 11;
`;

export const Controls = styled(EditControls)`
  top: 10px;
`;

export const ModalContainer = styled.div`
  max-width: 1440px;
  min-width: 90vw;
  height: 90vh;
  padding: 40px 20px 20px 20px;
  background-color: #f0f3f8;
  border-radius: 14px;
  display: flex;
  flex-direction: column;
  /* overflow-y: scroll; */
`;

export const ModalLayout = styled.div`
  display: grid;
  grid-template-columns: 360px 570px 360px;
  grid-column-gap: 50px;
  overflow-y: scroll;
`;

export const SectionTitle = styled.div`
  font-size: 12px;
  text-transform: uppercase;
  color: #616161;
  font-weight: 600;
  margin-bottom: 20px;
  letter-spacing: 1px;
`;

export const InfoItem = styled.div`
  margin-bottom: 40px;
  display: flex;
  align-items: baseline;
`;

export const ItemLabel = styled(SectionTitle)`
  width: 100px;
  color: #b0b0b0;
  margin-bottom: 8px;
`;

export const ItemText = styled.div`
  color: #6d7278;
  font-size: 14px;
`;

export const SectionContainer = styled.div`
  background-color: #fff;
  border-radius: 14px;
  box-sizing: border-box;
  padding: 20px 24px;
  margin-bottom: 30px;
`;

export const PointsBlock = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  font-weight: bold;
  height: 40px;
  width: 100%;
  background-color: #fff;
  border-radius: 12px;
`;

export const PointsCtaBlock = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 46px;
`;

export const PointsCtaButton = styled.button`
  display: inline-block;
  font-size: 20px;
  text-transform: uppercase;
  color: #fff;
  background-color: #274be8;
  border-radius: 19px;
  width: 195px;
  text-align: center;
  padding: 8px;
  &:disabled {
    opacity: 0.5;
  }
`;

export const ActivitiesHeader = styled.div`
  display: flex;
`;

export const ActivityHeaderItem = styled(SectionTitle)`
  margin-right: 50px;
  margin-bottom: 17px;
  &:last-child {
    margin-left: auto;
    margin-right: auto;
  }
`;

export const ActivityItem = styled.div`
  display: flex;
  align-items: center;
  background-color: #fff;
  width: 100%;
  border: 1px solid #dbdbdb;
  border-radius: 6px;
  height: 86px;
  padding: 5px 8px;
  box-shadow: 0 0 0 2px transparent;
  transition: all 0.2s ease-in-out;
  cursor: pointer;
  margin-bottom: 10px;
  &.chosen-a {
    box-shadow: 0 0 0 2px #274be8;
    border-color: #274be8;
  }
`;

interface ActivityItemImageProps {
  bgImage?: string | null;
}
export const ActivityItemImage = styled.div<ActivityItemImageProps>`
  display: flex;
  width: 76px;
  height: 76px;
  border-radius: 6px;
  background-color: #7e7e7e;
  font-size: 12px;
  box-sizing: border-box;
  padding: 4px;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: #fff;
  margin-right: 14px;
  ${(props) =>
    props.bgImage &&
    `
    background-image: url(${props.bgImage});
    background-position: center;
    background-size: cover;
  `}
`;

export const ActivityDate = styled.div`
  font-size: 14px;
  font-weight: 600;
  margin-right: 45px;
`;

export const ActivityPoints = styled(ActivityDate)``;

export const ActivityType = styled(ActivityDate)`
  margin-left: auto;
  margin-right: 25px;
  text-transform: capitalize;
`;

export const ActivityDescription = styled(ActivityType)`
  font-weight: normal;
  text-align: right;
`;

export const PointsModalContainer = styled.div`
  position: absolute;
  width: 100%;
  padding: 10px;
  border-radius: 6px;
  background-color: #fff;
  transform: translateY(5px);
`;

export const ActivityDetailsWrapper = styled.div`
  margin-top: 170px;
  transition: all 0.3s ease-in-out;
  transform: translateX(100px);
  opacity: 0;
  &.show-details {
    opacity: 1;
    transform: translateX(0);
  }
`;

interface ActivityImagePreviewProps {
  bgImage?: string | null;
}
export const ActivityImagePreview = styled.div<ActivityImagePreviewProps>`
  display: flex;
  width: 100%;
  height: 140px;
  justify-content: center;
  align-items: center;
  color: #fff;
  background-color: #222;
  margin-bottom: 20px;
  ${(props) =>
    props.bgImage &&
    `
    background-image: url(${props.bgImage});
    background-position: center;
    background-size: cover;
  `}
`;
