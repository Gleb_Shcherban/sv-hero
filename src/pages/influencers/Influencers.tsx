import React, { useCallback, useMemo, useEffect, useState } from 'react';
import { useParams, useHistory, useLocation } from 'react-router';

import { getTableItemsCount, getUsersWithStats } from '../../store/slices/influencersSlice';
import { useAppDispatch, useTypedSelector } from '../../store';

import { getQueryParam } from '../../services/utilities';

import { TableSection } from './tableSection/TableSection';
import { SearchPanel } from '../../common/components/searchPanel/SearchPanel';
import { NotificationButton } from '../../common/components/notificationButton/NotificationButton';

import { WebsiteRoutes, getRouteById } from '../../common/constants/routes';

import { PageContentContainer } from '../../common/styles/commonStyles.style';
import { Container } from './Influencers.style';
import { UserDetailsModal } from './UserDetailsModal';
import { InfluencersTabs } from './InfluencersTabls';

interface ParamTypes {
  id: string;
}

interface ILocation {
  search: string;
}

export enum TableFilterStates {
  all = 'all',
  shares = 'shares',
  rewards = 'rewards',
}

export const Influencers: React.FC = () => {
  const history = useHistory();
  const params = useParams<ParamTypes>();
  const location = useLocation<ILocation>();
  const dispatch = useAppDispatch();

  const [tableFilterState, setTableFilterState] = useState(TableFilterStates.all);
  const [records, setRecords] = useState<number | undefined>(0);
  const [rewards, setRewards] = useState<number | undefined>(0);

  const { page, size, sort } = useTypedSelector((state) => state.influencers);
  
  const firstNameParam = useMemo(() => {
    return getQueryParam(location, 'firstName');
  }, [location]);

  const fetchUserWithStats = useCallback(() => {
    const search = firstNameParam ? firstNameParam : '';

    if (tableFilterState === TableFilterStates.all) {
      dispatch(getUsersWithStats({ page, sort, size, search }));
      return;
    }
    if (tableFilterState === TableFilterStates.rewards) {
      dispatch(
        getUsersWithStats({
          page,
          sort,
          size,
          search,
          filterUnverifiedRewards: true,
        })
      );
      return;
    }
    if (tableFilterState === TableFilterStates.shares) {
      dispatch(
        getUsersWithStats({
          page,
          sort,
          size,
          search,
          filterUnverifiedRecords: true,
        })
      );
      return;
    }
  }, [dispatch, firstNameParam, page, size, sort, tableFilterState]);

  const getTabsData = useCallback(() => {
    const search = firstNameParam ? firstNameParam : '';
    getTableItemsCount({ page, size, sort, search, filterUnverifiedRewards: true })
      .then((rew) => {
        setRewards(rew);
      })
      .catch((err) => {
        console.log(err);
      });
    getTableItemsCount({ page, size, sort, search, filterUnverifiedRecords: true })
      .then((rec) => {
        setRecords(rec);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [page, size, sort, firstNameParam, setRecords, setRewards]);

  /* Retrieve users with unverified records and rewards */
  useEffect(() => {
    getTabsData();
  }, [tableFilterState, getTabsData]);

  useEffect(() => {
    fetchUserWithStats();
  }, [dispatch, page, sort, size, firstNameParam, tableFilterState, fetchUserWithStats]);

  const onClickRowHandler = useCallback(
    (id: string) => {
      const url = getRouteById(WebsiteRoutes.Influencers, id);
      history.push(url + location.search);
    },
    [history, location.search]
  );

  const onChangeSearchHandler = useCallback(
    (value: string) => {
      if (value) {
        history.push(`${location.pathname}?firstName=${value}`);
      } else {
        history.push(location.pathname);
      }
    },
    [history, location.pathname]
  );

  return (
    <Container>
      <PageContentContainer>
        <NotificationButton />
        <SearchPanel onClickSearch={onChangeSearchHandler} value={firstNameParam} />
        <InfluencersTabs
          recordsCount={records}
          rewardsCount={rewards}
          state={tableFilterState}
          setState={setTableFilterState}
        />
        <TableSection
          tableFilterState={tableFilterState}
          setTableFilterState={setTableFilterState}
          onClickRow={onClickRowHandler}
          userId={params.id}
          firstName={firstNameParam}
        />
      </PageContentContainer>
      {params.id && (
        <UserDetailsModal
          firstName={firstNameParam}
          userId={params.id}
          fetchUserWithStats={fetchUserWithStats}
          handleClose={() => {
            onClickRowHandler('');
            getTabsData();
          }}
        />
      )}
    </Container>
  );
};
