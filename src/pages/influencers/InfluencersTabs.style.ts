import styled from 'styled-components';

export const TabsContainer = styled.div`
  display: flex;
  margin-top: 25px;
  margin-top: 30px;
  margin-bottom: 20px;
`;

interface TabItemProps {
  active?: boolean;
}
export const TabItem = styled.div<TabItemProps>`
  display: inline-block;
  height: 54px;
  width: 346px;
  line-height: 54px;
  box-sizing: border-box;
  border-radius: 6px;
  color: #050505;
  font-size: 18px;
  background-color: #fff;
  margin-right: 20px;
  text-align: center;
  cursor: pointer;
  position: relative;
  box-shadow: 0 0 0 3px transparent;
  transition: all 0.15s ease-in-out;
  ${(props) => props.active && `
    box-shadow: 0 0 0 3px #274BE9;
  `}
`;

export const CountBadge = styled.div`
  display: inline-block;
  height: 30px;
  width: 30px;
  color: #fff;
  text-align: center;
  line-height: 30px;
  background-color: #274BE8;
  border-radius: 50%;
  font-size: 16px;
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(40%,-30%);
`;

