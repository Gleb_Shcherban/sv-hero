import React from 'react';

import { useAppDispatch, useTypedSelector } from '../../../store';

import { goToSelectedPage, setUserDataSorting } from '../../../store/slices/influencersSlice';

import { createUserDataTableContent } from '../../../services/helpers/tableMappers';

import { CommonTableNew } from '../../../common/components/table/CommonTableNew';
import {
  convertApiPageToFrontEndPage,
  convertFrontEndPageToApiPage,
} from '../../../services/utilities';
import { TableFilterStates } from '../Influencers';

interface ITableSection {
  userId: string;
  firstName: string;
  tableFilterState: TableFilterStates;
  setTableFilterState: (state: TableFilterStates) => void;
  onClickRow: (id: string) => void;
}

export const TableSection: React.FC<ITableSection> = ({ userId, onClickRow }) => {
  const dispatch = useAppDispatch();
  const { isLoading, error, items, totalPages, totalItems, page, sort } = useTypedSelector(
    (state) => state.influencers
  );

  const userDataTableContents = createUserDataTableContent({
    items,
  });

  const onGoToPage = (targetPage: number) => {
    dispatch(goToSelectedPage(convertFrontEndPageToApiPage(targetPage)));
  };

  const onSort = (name: string) => {
    dispatch(setUserDataSorting(name));
  };

  return (
    <>
      <CommonTableNew
        selectedRowId={userId}
        content={userDataTableContents}
        page={convertApiPageToFrontEndPage(page)}
        sort={sort}
        totalItems={totalItems}
        totalPages={totalPages}
        isLoading={isLoading}
        noContent={error}
        tablePadding="0"
        goToPage={onGoToPage}
        onClickRow={onClickRow}
        onSortHeaderClick={onSort}
      />
    </>
  );
};
