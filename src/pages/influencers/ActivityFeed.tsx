import React, { useEffect, useCallback, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';

import { RewardStatuses, UserReward, UserPerformanceRecord } from '../../api/models/users';

import { useTypedSelector } from '../../store';
import { getVideoImgUrlFromS3 } from '../../services/utilities';

import {
  getUserRewardsRequest,
  getUsersPerformanceRecords,
  unverifyPerofmanceRecord,
  updateUserReward,
  verifyPerofmanceRecord,
} from '../../store/slices/influencersSlice';

import { defaultPagination } from '../../common/constants/constants';

import ToggleComponent from './ToggleComponent';
import { CurrentActivity } from './UserDetailsModal';
import {
  SectionTitle,
  ActivitiesHeader,
  ActivityHeaderItem,
  ActivityItem,
  ActivityItemImage,
  ActivityDate,
  ActivityPoints,
  ActivityType,
  ActivityDescription,
} from './UserDetailsModal.style';

interface FeedItem {
  id: string;
  userId: string;
  type: string;
  date: string;
  imageUrl: string | null;
  points: number;
  venueId: string;
  status: boolean;
  description?: string;
  rewardId?: string;
  relatedStoryVideoId?: string | null;
}

export enum ActivityTypes {
  REFER = 'REFER',
  SIGN_UP = 'SIGN_UP',
  SHARE_VIDEO = 'SHARE_VIDEO',
  NONE = 'NONE',
}

function getRecordType(type: string | ActivityTypes) {
  if (type === ActivityTypes.REFER) {
    return 'sale';
  }
  if (type === ActivityTypes.SIGN_UP) {
    return 'none';
  }
  if (type === ActivityTypes.SHARE_VIDEO) {
    return 'verify';
  }
  if (type === ActivityTypes.NONE) {
    return 'none';
  }
  return '';
}

interface IScrollParentRef {
  current: HTMLDivElement | null;
}

interface ActivityFeedProps {
  updateActivityFeedRecords: number;
  id: string;
  currentActivity: CurrentActivity;
  setCurrentActivity: React.Dispatch<React.SetStateAction<CurrentActivity>>;
  scrollParentRef: IScrollParentRef;
}

const ActivityFeed: React.FC<ActivityFeedProps> = ({
  updateActivityFeedRecords,
  id,
  currentActivity,
  setCurrentActivity,
  scrollParentRef,
}) => {
  const {
    venueAttributes: { attributes },
  } = useTypedSelector((state) => state.venue);

  const [feedItems, setFeedItems] = useState<FeedItem[]>([]);
  const [recordsTotalPages, setRecordsTotalPages] = useState(0);
  const [rewardsTotalPages, setRewardsTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(0);
  const [blocked, setBlocked] = useState(false);

  const isChosen = useCallback(
    (id) => {
      return currentActivity && currentActivity.id === id;
    },
    [currentActivity]
  );

  const getData = useCallback(
    async (page: number, recordsTotalPages: number, rewardsTotalPages: number, feedItems = []) => {
      let rewards = {
        items: [] as UserReward[],
        totalPages: 0,
      };

      let performance = {
        items: [] as UserPerformanceRecord[],
        totalPages: 0,
      };

      setBlocked(true);
      if (rewardsTotalPages > page || page === 0) {
        rewards = await getUserRewardsRequest(id, {
          page,
          sort: defaultPagination.sortByLastCreated,
        });
      }
      if (recordsTotalPages > page || page === 0) {
        performance = await getUsersPerformanceRecords(id, {
          page,
          sort: defaultPagination.sortByLastCreated,
        });
      }

      const rewardsArr = rewards.items.map(({ id, createdAt, reward, userId, status }) => {
        return {
          id,
          userId,
          type: 'reward',
          date: new Date(createdAt).toLocaleDateString('en-US').replace(/\//g, '-'),
          imageUrl: reward.imageUri,
          points: reward.redemptionPoints,
          venueId: reward.venueId,
          status: RewardStatuses.verified === status,
          rewardId: reward.id,
        };
      });
      const filteredRecords = performance.items.filter((i) => i.activityType);

      const recordsArr = filteredRecords.map(
        ({
          activityType,
          id,
          createdAt,
          description,
          imageUri,
          status,
          userId,
          rewardPoints,
          venueId,
          relatedStoryVideoId,
        }) => {
          return {
            id,
            userId,
            venueId,
            description,
            type: getRecordType(activityType),
            date: new Date(createdAt).toLocaleDateString('en-US').replace(/\//g, '-'),
            imageUrl: getVideoImgUrlFromS3(imageUri, attributes['webapp.client-cdn']),
            points: rewardPoints,
            status: RewardStatuses.verified === status,
            relatedStoryVideoId,
          };
        }
      );
      setRecordsTotalPages(performance.totalPages);
      setRewardsTotalPages(rewards.totalPages);
      setBlocked(false);
      setFeedItems([...feedItems, ...recordsArr, ...rewardsArr]);
    },
    [attributes, id]
  );

  useEffect(() => {
    setCurrentPage(0);
    getData(0, 0, 0);
  }, [getData, updateActivityFeedRecords]);

  const verify = useCallback(
    (id) => {
      verifyPerofmanceRecord(id)
        .then(() => {
          setCurrentPage(0);
          getData(0, recordsTotalPages, rewardsTotalPages);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    [getData, recordsTotalPages, rewardsTotalPages]
  );

  const unverify = useCallback(
    (id) => {
      unverifyPerofmanceRecord(id)
        .then(() => {
          setCurrentPage(0);
          getData(0, recordsTotalPages, rewardsTotalPages);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    [getData, recordsTotalPages, rewardsTotalPages]
  );

  const verifyReward = useCallback(
    (rewardId) => {
      updateUserReward(id, rewardId, 'verify')
        .then(() => {
          setCurrentPage(0);
          getData(0, recordsTotalPages, rewardsTotalPages);
        })
        .catch((err) => {
          console.log(err);
        });
    },
    [id, getData, recordsTotalPages, rewardsTotalPages]
  );

  const loadMoreHandler = useCallback(() => {
    const page = currentPage + 1;
    setBlocked(true);
    setCurrentPage(page);
    getData(page, recordsTotalPages, rewardsTotalPages, feedItems);
  }, [currentPage, feedItems, getData, recordsTotalPages, rewardsTotalPages]);

  const hasMoreHandler = useCallback(() => {
    const isNextPage = recordsTotalPages - currentPage > 1 || rewardsTotalPages - currentPage > 1;
    return Boolean(isNextPage && !blocked);
  }, [blocked, currentPage, recordsTotalPages, rewardsTotalPages]);

  return (
    <>
      <SectionTitle>Activity feed</SectionTitle>
      <ActivitiesHeader>
        <ActivityHeaderItem>content</ActivityHeaderItem>
        <ActivityHeaderItem>date</ActivityHeaderItem>
        <ActivityHeaderItem>points impact</ActivityHeaderItem>
        <ActivityHeaderItem>action</ActivityHeaderItem>
      </ActivitiesHeader>
      <InfiniteScroll
        useWindow={false}
        pageStart={currentPage}
        initialLoad={false}
        loadMore={loadMoreHandler}
        hasMore={hasMoreHandler()}
        getScrollParent={() => scrollParentRef.current}
      >
        <div>
          {feedItems.map(
            ({ id, type, date, points, description, imageUrl, status, relatedStoryVideoId }) => {
              if (type === 'sale') {
                return (
                  <ActivityItem
                    key={id}
                    className={[isChosen(id) ? 'chosen-a' : ''].join(' ')}
                    onClick={() => setCurrentActivity({ id, date, imageUrl, type: 'sale' })}
                  >
                    <ActivityItemImage>Sale</ActivityItemImage>
                    <ActivityDate>{date}</ActivityDate>
                    <ActivityPoints>{points}</ActivityPoints>
                    <ActivityType>{type}</ActivityType>
                    <ToggleComponent
                      isActive={status}
                      trigger={
                        status
                          ? () => {
                              unverify(id);
                            }
                          : () => {
                              verify(id);
                            }
                      }
                    />
                  </ActivityItem>
                );
              }
              if (type === 'none') {
                return (
                  <ActivityItem key={id} className={[isChosen(id) ? 'chosen-a' : ''].join(' ')}>
                    <ActivityItemImage>Points Added Manually</ActivityItemImage>
                    <ActivityDate>{date}</ActivityDate>
                    <ActivityPoints>{points}</ActivityPoints>
                    <ActivityDescription>{description}</ActivityDescription>
                  </ActivityItem>
                );
              }
              if (type === 'reward') {
                return (
                  <ActivityItem
                    key={id}
                    className={[isChosen(id) ? 'chosen-a' : ''].join(' ')}
                    onClick={() => setCurrentActivity({ id, date, imageUrl, type: 'reward' })}
                  >
                    <ActivityItemImage bgImage={imageUrl}>
                      {imageUrl ? '' : `Reward ${points}`}
                    </ActivityItemImage>
                    <ActivityDate>{date}</ActivityDate>
                    <ActivityPoints>{points}</ActivityPoints>
                    <ActivityType>{type}</ActivityType>
                    <ToggleComponent
                      disabled={status}
                      isActive={status}
                      trigger={
                        status
                          ? () => {}
                          : () => {
                              verifyReward(id);
                            }
                      }
                    />
                  </ActivityItem>
                );
              }

              if (type === 'verify') {
                return (
                  <ActivityItem
                    key={id}
                    className={[isChosen(id) ? 'chosen-a' : ''].join(' ')}
                    onClick={() => {
                      setCurrentActivity({
                        id,
                        date,
                        imageUrl,
                        type: 'share video',
                        video: relatedStoryVideoId,
                      });
                    }}
                  >
                    <ActivityItemImage bgImage={imageUrl}></ActivityItemImage>
                    <ActivityDate>{date}</ActivityDate>
                    <ActivityPoints>{points}</ActivityPoints>
                    <ActivityType>{type}</ActivityType>
                    <ToggleComponent
                      isActive={status}
                      trigger={
                        status
                          ? () => {
                              unverify(id);
                            }
                          : () => {
                              verify(id);
                            }
                      }
                    />
                  </ActivityItem>
                );
              }
              return null;
            }
          )}
        </div>
      </InfiniteScroll>
    </>
  );
};

export default React.memo(ActivityFeed);
