import React, { useEffect, useRef, useState } from 'react';
import { PlayIcon, StyledVideo, VideoContainer } from './ActivityDetailsVideo.style';
import Play from '../../common/assets/playIcon.png';
import { Spinner } from '../../common/assets/Spinner';
import { getStoryVideoById } from '../../store/slices/influencersSlice';
import { getVideoFromS3 } from '../../services/utilities';

interface ActivityDetailsVideoProps {
  id: string | null | undefined;
}

const ActivityDetailsVideo: React.FC<ActivityDetailsVideoProps> = ({ id }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [url, setUrl] = useState('');

  useEffect(() => {
    if (id) {
      setIsLoading(true);
      getStoryVideoById(id)
        .then((response) => {
          setUrl(
            getVideoFromS3(response.uri)
          )
          setIsLoading(false);
        })
        .catch(error => {
          console.error(error);
          setIsLoading(false);
        })
    }
  }, [id, setIsLoading, setUrl]);

  if (isLoading) {
    return (
      <Spinner  color="var(--spinnerColor)" />
    )
  }

  const onClickHandler = (e: React.MouseEvent<HTMLVideoElement>) => {
    const videoElement = e.target as HTMLVideoElement;
    const refVideoCurrent = videoRef.current;

    if (refVideoCurrent && refVideoCurrent.paused) {
      videoElement.play();
    } else {
      videoElement.pause();
      setIsPlaying(false);
    }
  };

  const onPlay = () => {
    setIsPlaying(true);
  }

  const onEnd = () => {
    setIsPlaying(false);
  }

  if (!id) {
    return (
      <div style={{ padding: '20px 0' }}>Video is not available</div>
    )
  }

  return (
    <VideoContainer>
      <StyledVideo
        src={url}
        onClick={onClickHandler}
        ref={videoRef}
        onPlay={onPlay}
        onEnded={onEnd}
      />
      {!isPlaying && (
        <PlayIcon src={Play} />
      )}
    </VideoContainer>
  )
}

export default ActivityDetailsVideo;
