import React, { useMemo } from 'react';
import { UserApiModel } from '../../api/models/users';
import { getStringValue } from '../../services/utilities';
import {
  InfoItem,
  ItemLabel,
  ItemText,
  SectionContainer,
  SectionTitle,
} from './UserDetailsModal.style';

interface UserInfoComponentProps {
  data: UserApiModel;
}

export const UserInfoComponent: React.FC<UserInfoComponentProps> = ({ data }) => {
  const {
    email,
    phoneNumber,
    firstName,
    rewardPoints,
    referralCode,
    socialProfiles,
  } = data;

  const igLink = useMemo(() => {
    if (!socialProfiles.length) {
      return 'N/A';
    }
    const link = socialProfiles[0].handle;
    return (
      <a
        href={`https://www.instagram.com/${link}/`}
        target="_blank"
        rel="noreferrer noopener"
      >
        @{link}
      </a>
    );
  }, [socialProfiles]);

  const igFields = useMemo(() => {
    if (!socialProfiles.length || !socialProfiles[0].data) {
      return {
        followers: getStringValue(null),
        following: getStringValue(null),
        biography: getStringValue(null),
        posts: getStringValue(null),
      };
    }

    const { data } = socialProfiles[0];

    return {
      followers: getStringValue(data?.followers),
      following: getStringValue(data?.following),
      biography: getStringValue(data?.biography),
      posts: getStringValue(data?.posts),
    };
  }, [socialProfiles]);

  return (
    <>
      <SectionTitle>user info</SectionTitle>
      <SectionContainer>
        <InfoItem>
          <ItemLabel>User</ItemLabel>
          <ItemText>{getStringValue(firstName)}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Email</ItemLabel>
          <ItemText>{email}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Phone</ItemLabel>
          <ItemText>{getStringValue(phoneNumber)}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>social</ItemLabel>
          <ItemText>{igLink}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Code</ItemLabel>
          <ItemText>{referralCode}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Points</ItemLabel>
          <ItemText>{rewardPoints}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Followers</ItemLabel>
          <ItemText>{igFields.followers}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Following</ItemLabel>
          <ItemText>{igFields.following}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>Posts</ItemLabel>
          <ItemText>{igFields.posts}</ItemText>
        </InfoItem>
        <InfoItem>
          <ItemLabel>BIO</ItemLabel>
          <ItemText>{igFields.biography}</ItemText>
        </InfoItem>
      </SectionContainer>
    </>
  );
};
