import React from 'react';
import styled from 'styled-components';

const ToggleTrack = styled.div`
  width: 57px;
  height: 34px;
  border: 1px solid #274be8;
  border-radius: 20px;
  background-color: rgba(39, 75, 232, 0.1);
  position: relative;
  transition: all 0.2s ease-in-out;
  opacity: 0.1;
  cursor: pointer;
  &.active {
    opacity: 1;
  }
`;

const ToggleCircle = styled.div`
  display: inline-block;
  position: absolute;
  top: -1px;
  left: 0;
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-color: #274be8;
  transform: translateX(0);
  transition: all 0.2s ease-in-out;
  &.active {
    transform: translateX(23px);
  }
`;

interface ToggleComponentProps {
  isActive: boolean;
  trigger: () => void;
  disabled?: boolean;
}
const ToggleComponent: React.FC<ToggleComponentProps> = ({
  isActive,
  trigger,
  disabled = false
}) => {
  const [isOn, setIsOn] = React.useState(isActive);

  const handleToggle = React.useCallback(() => {
    if (disabled) {
      return;
    }
    setIsOn((i) => !i);
    setTimeout(() => {
      trigger();
    }, 300);
  }, [setIsOn, trigger, disabled]);

  return (
    <ToggleTrack
      onClick={handleToggle}
      className={[isOn ? 'active' : ''].join(' ')}
    >
      <ToggleCircle className={[isOn ? 'active' : ''].join(' ')} />
    </ToggleTrack>
  );
};

export default ToggleComponent;
