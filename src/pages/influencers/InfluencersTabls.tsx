import React from 'react';
import { CountBadge, TabItem, TabsContainer } from './InfluencersTabs.style';
import { TableFilterStates } from './Influencers';

interface InfluencersTabsProps {
  state: TableFilterStates;
  setState: (state: TableFilterStates) => void;
  rewardsCount: number | undefined;
  recordsCount: number | undefined;
}

export const InfluencersTabs: React.FC<InfluencersTabsProps> = ({
  state, setState, rewardsCount, recordsCount
}) => {

  return (
    <TabsContainer>
      <TabItem
        onClick={() => { setState(TableFilterStates.all) }}
        active={state === TableFilterStates.all}
      >
        All Influencers
      </TabItem>
      <TabItem
        onClick={() => { setState(TableFilterStates.shares) }}
        active={state === TableFilterStates.shares}
      >
        Verify Content
        {!!recordsCount && <CountBadge>{recordsCount}</CountBadge>}
      </TabItem>
      <TabItem
        onClick={() => { setState(TableFilterStates.rewards) }}
        active={state === TableFilterStates.rewards}
      >
        Rewards
        {!!rewardsCount && <CountBadge>{rewardsCount}</CountBadge>}
      </TabItem>
    </TabsContainer>
  )
}
