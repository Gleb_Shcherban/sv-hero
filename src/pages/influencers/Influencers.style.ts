import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../common/constants/constants';

export const Container = styled.div`
  display: flex;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    flex-direction: column;
  }
`;
