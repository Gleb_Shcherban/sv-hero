import React, { useEffect, useRef, useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { UserApiModel } from '../../api/models/users';
import { InfluencersPageStrings } from '../../common/localization/en';
import { InputField } from '../../common/components/inputField/InputField';
import { addPointsManually, getUserRefereeRedeemed } from '../../store/slices/influencersSlice';
import { EditButton, ModalCta } from '../rewards/Rewards.style';
import {
  Controls,
  PointsBlock,
  PointsCtaBlock,
  PointsCtaButton,
  SectionTitle,
  PointsModalContainer,
} from './UserDetailsModal.style';

export interface IValuesRewardsPoints {
  description: string;
  rewardPoints: number | null;
}

export const initialValues: IValuesRewardsPoints = {
  description: '',
  rewardPoints: null,
};
export const validationSchema: Yup.ObjectSchemaDefinition<IValuesRewardsPoints> = {
  description: Yup.string().max(1000, 'max length 1000 symbols').required('Required'),
  rewardPoints: Yup.number()
    .min(-2000000000, 'min -2 000 000 000 value')
    .max(2000000000, 'max 2 000 000 000 value')
    .required('Required'),
};

interface PointsModalProps {
  userId: string;
  handleClose: () => void;
  triggerUpdate: () => void;
}
const PointsModal: React.FC<PointsModalProps> = ({ userId, handleClose, triggerUpdate }) => {
  const [isLoading, setIsLoading] = useState(false);

  const onSubmitHandler = (values: IValuesRewardsPoints) => {
    setIsLoading(true);
    addPointsManually({
      description: values.description,
      userId,
      rewardPoints: values.rewardPoints,
    })
      .then(() => {
        setIsLoading(false);
        handleClose();
        triggerUpdate();
      })
      .catch((error) => {
        setIsLoading(false);
      });
  };

  const PointsModalFormik = useFormik({
    initialValues,
    enableReinitialize: true,
    validateOnChange: false,
    validateOnBlur: false,
    validationSchema: Yup.object().shape<IValuesRewardsPoints>(validationSchema),
    onSubmit: (values) => {
      onSubmitHandler(values);
    },
  });
  const { handleSubmit, values, handleChange, errors } = PointsModalFormik;

  return (
    <PointsModalContainer>
      <form style={{ width: '100%' }} onSubmit={handleSubmit}>
        <InputField
          name="rewardPoints"
          type="number"
          headerText={InfluencersPageStrings.PointsRewardsPointsForm}
          value={values.rewardPoints}
          onChange={handleChange}
          placeholder={InfluencersPageStrings.RewardsPointsPlaceholder}
          errorText={errors.rewardPoints}
        />
        <InputField
          name="description"
          type="text"
          headerText={InfluencersPageStrings.DescriptionRewardsPointsForm}
          value={values.description}
          onChange={handleChange}
          placeholder={InfluencersPageStrings.RewardsDescriptionPlaceholder}
          errorText={errors.description}
        />
        <ModalCta type="submit" disabled={!values.description || !values.rewardPoints || isLoading}>
          {isLoading ? 'Loading...' : 'Update'}
        </ModalCta>
      </form>
    </PointsModalContainer>
  );
};

interface UserPointsProps {
  data: UserApiModel;
  triggerUpdate: () => void;
}

const UserPointsComponent: React.FC<UserPointsProps> = ({ data, triggerUpdate }) => {
  const { rewardPoints } = data;
  const [isEdit, setIsEdit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const blockRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    const handleClick = (e: Event) => {
      // @ts-ignore
      if (blockRef.current && !blockRef.current.contains(e.target)) {
        setIsEdit(false);
        console.log(blockRef.current);
      }
    };
    document.addEventListener('click', handleClick);
    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [setIsEdit]);

  const handleRedeem = () => {
    setIsLoading(true);
    getUserRefereeRedeemed(data.id)
      .then(() => {
        setIsLoading(false);
        triggerUpdate();
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  };

  return (
    <PointsCtaBlock>
      <div style={{ position: 'relative', width: 300, marginRight: 70 }}>
        <SectionTitle>current points</SectionTitle>
        <div ref={blockRef}>
          <PointsBlock>{rewardPoints}</PointsBlock>
          {isEdit && (
            <PointsModal
              userId={data.id}
              handleClose={() => {
                setIsEdit(false);
              }}
              triggerUpdate={triggerUpdate}
            />
          )}
        </div>
        <Controls>
          <EditButton className="fa fa-pen" onClick={() => setIsEdit(true)} />
        </Controls>
      </div>
      <div>
        <SectionTitle>Client confirmed sales</SectionTitle>
        <PointsCtaButton onClick={handleRedeem} disabled={isLoading}>
          add sale
        </PointsCtaButton>
      </div>
    </PointsCtaBlock>
  );
};

export default UserPointsComponent;
