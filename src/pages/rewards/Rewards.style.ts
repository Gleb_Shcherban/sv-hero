import styled from 'styled-components';

export const CtaButton = styled.button`
  display: inline-block;
  border-radius: 20px;
  color: #fff;
  background-color: #274BE8;
  font-size: 20px;
  padding: 22px 0;
  text-align: center;
  width: 100%;
  font-weight: 600;
  text-transform: uppercase;
  &:disabled {
    opacity: 0.6;
  }
`;

export const LayoutGrid = styled.div`
  display: grid;
  width: 100%;
  box-sizing: border-box;
  grid-template-columns: minmax(300px, 350px) minmax(300px, 350px) minmax(300px, 350px);
  grid-column-gap: 100px;
  margin-bottom: 20px;
`;

export const PageContainer = styled.div`
  max-width: 1440px;
  box-sizing: border-box;
  margin: 0 auto;
  padding: 70px 20px 0 20px;
`;

export const RewardContainer = styled.div`
  margin-bottom: 82px;
  position: relative;
`;

export const SignUpContainer = styled(RewardContainer)`
  position: relative;
`;

export const ReferContainer = styled(RewardContainer)`
  position: relative;
`;

export const ShareContainer = styled(RewardContainer)`
  position: relative;
`;

export const BlockLabel = styled.div`
  color: #6D7278;
  font-size: 14px;
  margin-bottom: 5px;
`;

interface RewardImageBlockProps {
  url: string | null
}
export const RewardImageBlock = styled.div<RewardImageBlockProps>`
  width: 100%;
  height: 135px;
  background-color: #eaeaea;
  background-size: cover;
  background-position: center;
  margin-bottom: 14px;
  ${props => props.url && `
    background-image: url(${props.url});
  `}
`;

export const RewardPointsBlock = styled.div`
  background-color: #fff;
  border-radius: 12px;
  overflow: hidden;
  box-sizing: border-box;
  padding: 10px;
  box-shadow: 0 0 12px 0px rgba(0,0,0,0.2);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SignUpBlock = styled(RewardPointsBlock)`
  align-items: center;
`;

export const ReferBlock = styled(RewardPointsBlock)`
  flex-direction: column;
  text-align: left;
  padding-bottom: 0;
  margin-bottom: 20px;
`;

export const ReferHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  box-sizing: border-box;
  padding: 8px 0 0 8px;
`;

export const ReferTitle = styled.h2`
  font-size: 16px;
  font-weight: bold;
`;

export const TextValueBlock = styled.div`
  display: inline-block;
  border: 1px solid #CFCFCF;
  border-radius: 8px;
  box-sizing: border-box;
  padding: 14px 8px;
  font-size: 12px;
  width: 100%;
`;

export const PlainText = styled(TextValueBlock)`
  border: none;
  padding-left: 0;
`;

export const ImageBlock = styled(RewardImageBlock)`
  margin-bottom: 0;
  margin-left: -10px;
  margin-right: -10px;
  width: calc(100% + 20px);
`;

export const RewardThresholdBlock = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  font-weight: bold;
  height: 40px;
  width: 230px;
  border: 1px solid #CFCFCF;
  border-radius: 8px;
`;

export const PointsBadge = styled.div`
  display: flex;
  min-width: 47px;
  min-height: 47px;
  border: 3px solid #FFE575;
  border-radius: 50%;
  background-color: #FFCC6D;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  font-size: 14px;
  margin-left: 16px;
`;

export const EditControls = styled.div`
  position: absolute;
  right: 0;
  transform: translateX(22px);
`;

export const EditButton = styled.div`
  display: inline-block;
  height: 44px;
  width: 44px;
  background-color: #fff;
  box-shadow: 0 0 6px rgba(0,0,0,0.3);
  cursor: pointer;
  font-size: 18px;
  color: #222;
  line-height: 44px;
  text-align: center;
  border-radius: 50%;
  margin-left: 5px;
`;

export const ModalOverlay = styled.div`
  position: fixed;
  background-color: rgba(0,0,0,0.8);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
`;

export const CreateRewardModalContainer = styled.div`
  max-width: 500px;
  min-width: 500px;
  padding: 20px;
  background-color: #fff;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
`;

export const UpdateRewardModalContainer = styled(CreateRewardModalContainer)``;

export const UpdateSignUpModalContainer = styled(CreateRewardModalContainer)``;

export const FormInput = styled.input`
  display: block;
  width: 100%;
  margin: 4px 0;
  padding: 8px;
  border-radius: 8px;
  border: 1px solid #CFCFCF;
  &:placeholder {
    font-size: 16px;
    color: #eee;
  }
  &:focus {
    border-color: #007aff;
  }
`;

export const ModalCta = styled.button`
  display: inline-block;
  padding: 20px;
  font-size: 18px;
  text-align: center;
  border-radius: 8px;
  background-color: #007aff;
  color: #fff;
  width: 100%;
  margin-top: 20px;
  &:disabled {
    opacity: 0.6;
  }
`;
