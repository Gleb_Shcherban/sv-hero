import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../../common/constants/constants';

export const LeaderBoardSectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: var(--white);
  width: 100%;
`;

export const LeaderBoardList = styled.ul`
  margin: 0 var(--sectionPadding);
  padding: 12px;
  box-sizing: border-box;
  background-color: var(--sectionStripeGrey);

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    margin: 0;
  }
`;

export const LeaderBoardItem = styled.li`
  margin-bottom: 10px;
  height: 65px;
  display: flex;
  align-items: center;
  background-color: var(--white);
  border-radius: var(--commonBorderRadius);
  overflow: hidden;

  :first-child > div:nth-child(2) {
    height: 64px;
    border-radius: 0 var(--commonBorderRadius) var(--commonBorderRadius) 0;
    border: 1px solid #45ff54;
    border-left: none;
  }
`;

export const LeaderBoardImage = styled.div`
  width: 65px;
  min-width: 65px;
`;

export const LeaderBoardPlaceValues = styled.div`
  padding: 7px 15px;
  width: 100%;
`;

export const LeaderBoardPlace = styled.div`
  font-size: 14px;
  color: #575757;
`;

export const LeaderBoardUserData = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 18px;
  font-weight: 800;
  color: var(--black);
`;

export const LeaderBoardUserName = styled.div``;

export const LeaderBoardPts = styled.div``;
