import React, { memo } from 'react';

import Thumbnail from '../assets/Thumbnail.jpg';

import {
  LeaderBoardSectionContainer,
  LeaderBoardList,
  LeaderBoardItem,
  LeaderBoardImage,
  LeaderBoardPlaceValues,
  LeaderBoardPlace,
  LeaderBoardUserData,
  LeaderBoardUserName,
  LeaderBoardPts,
} from './LeaderBoardSection.style';
import { SectionHeaderStripe } from '../../../common/components/sectionHeaderStripe/SectionHeaderStripe';
import { RewardsPageStrings } from '../../../common/localization/en';

const leaderBoardValues = [
  {
    id: 1,
    img: Thumbnail,
    place: '1st',
    userName: 'Justin',
    pts: '1500 pts',
  },
  {
    id: 2,
    img: Thumbnail,
    place: '2nd',
    userName: 'Bob',
    pts: '1300 pts',
  },
];

export const LeaderBoardSection = memo(() => {
  return (
    <LeaderBoardSectionContainer>
      <SectionHeaderStripe title={RewardsPageStrings.Leaderboards} />
      <LeaderBoardList>
        {leaderBoardValues.map((value) => (
          <LeaderBoardItem key={value.id}>
            <LeaderBoardImage>
              <img src={value.img} alt="person" />
            </LeaderBoardImage>
            <LeaderBoardPlaceValues>
              <LeaderBoardPlace>{value.place}</LeaderBoardPlace>
              <LeaderBoardUserData>
                <LeaderBoardUserName>{value.userName}</LeaderBoardUserName>
                <LeaderBoardPts>{value.pts}</LeaderBoardPts>
              </LeaderBoardUserData>
            </LeaderBoardPlaceValues>
          </LeaderBoardItem>
        ))}
      </LeaderBoardList>
    </LeaderBoardSectionContainer>
  );
});
