import React from 'react';

import { TableHeaderWrapper } from '../../../common/components/table/tableHeaderWrapper/TableHeaderWrapper';
import { CommonTable } from '../../../common/components/table/CommonTable';
import {
  TableCellWithParams,
  TableContent,
  TableRow,
} from '../../../common/commonTypes';
import { EditButton } from '../../../common/components/table/EditButton';
import { RewardsPageStrings } from '../../../common/localization/en';

import { ContentSectionContainer } from '../../../common/styles/commonStyles.style';
import {
  BodySection,
  TableWrapper,
  Title,
  Text,
  TitleWrapper,
} from './ManageRewards.style';

interface ManageRewardsContent extends TableRow {
  name: TableCellWithParams<string>;
  duration: TableCellWithParams<string>;
  startDate: TableCellWithParams<string>;
  endDate: TableCellWithParams<string>;
  edit: TableCellWithParams<JSX.Element>;
}

export const ManageRewards: React.FC = () => {
  //TODO: is an example dummy data, change it to data from API
  const manageCategoriesSectionTableContents: TableContent<ManageRewardsContent> = {
    rows: [
      {
        name: { render: 'VIP Game Tickets' },
        duration: { render: '21 Days' },
        startDate: { render: 'Oct 5, 2020' },
        endDate: { render: 'Oct 25, 2020' },
        edit: {
          render: (
            <EditButton>
              <div />
            </EditButton>
          ),
        },
      },
      {
        name: { render: 'VIP Game Tickets' },
        duration: { render: '21 Days' },
        startDate: { render: 'Oct 5, 2020' },
        endDate: { render: 'Oct 25, 2020' },
        edit: {
          render: (
            <EditButton>
              <div />
            </EditButton>
          ),
        },
      },
    ],
  };
  return (
    <ContentSectionContainer>
      <TitleWrapper>
        <Title>{RewardsPageStrings.Title}</Title>
        <Text>{RewardsPageStrings.SubTitle}</Text>
      </TitleWrapper>
      <BodySection>
        <TableWrapper>
          <TableHeaderWrapper
            title={RewardsPageStrings.ManageRewards}
            modalTitle={RewardsPageStrings.AddModalTitle}
            orderDirection=""
            onClickSort={() => {}}
            addButtonComponent={<div>+</div>}
          >
            <CommonTable
              content={manageCategoriesSectionTableContents}
              totalItems={manageCategoriesSectionTableContents.rows.length}
              page={1}
              totalPages={1}
              isLoading={false}
              noContent={false}
              goToPage={(page) => {
                console.log('GOTO:', page);
              }}
              tablePadding="0"
            />
          </TableHeaderWrapper>
        </TableWrapper>
      </BodySection>
    </ContentSectionContainer>
  );
};
