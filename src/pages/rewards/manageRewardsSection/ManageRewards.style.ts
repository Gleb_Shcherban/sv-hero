import styled from 'styled-components';
import { TABLET_MAX_WIDTH } from '../../../common/constants/constants';

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-top: 36px;
  color: var(--white);
  text-align: center;
`;

export const Title = styled.h1`
  display: flex;
  width: 486px;
  font-size: 38px;
  font-weight: 800;

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    width: 100%;
    font-size: 28px;
  }
`;

export const Text = styled.p`
  display: flex;
  padding-top: 23px;
  font-size: 20px;
  font-weight: 600;
`;

export const BodySection = styled.div`
  position: relative;
  width: 100%;
  box-sizing: border-box;
  margin-top: 32px;
`;

export const TableWrapper = styled.div`
  margin: 0 28px 10px;
  padding: 0 11px;
  min-height: 350px;
  border-radius: var(--commonBorderRadius) var(--commonBorderRadius) 0 0;
  background-color: var(--white);
  box-shadow: 0px -18px 13px -2px var(--pageHeaderBgr);

  > div > div:first-child {
    padding-left: 25px;
  }

  @media (max-width: ${TABLET_MAX_WIDTH}px) {
    margin: 0;
    padding: 0;
    min-height: fit-content;
  }
`;
