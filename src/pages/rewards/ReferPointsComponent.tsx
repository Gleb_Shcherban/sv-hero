import React, { useState } from 'react';
import { RewardActivityApiModel } from '../../api/models/rewardables';
import {
  BlockLabel,
  EditButton,
  EditControls,
  ImageBlock,
  PlainText,
  PointsBadge,
  ReferBlock,
  ReferContainer,
  ReferHeader,
  ReferTitle,
  RewardPointsBlock,
  RewardThresholdBlock,
  TextValueBlock,
} from './Rewards.style';
import UpdateReferModal from './modals/UpdateReferModal';
import { useTypedSelector } from '../../store';
import { VenueAttributeName } from '../../api/models/venue';

interface ReferPointsComponentProps {
  data: RewardActivityApiModel;
}

const ReferPointsComponent: React.FC<ReferPointsComponentProps> = ({
  data,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { points, description, title, imageUrl } = data;

  const { venueAttributes } = useTypedSelector(state => state.venue);

  const inviteText = venueAttributes.attributes[VenueAttributeName.InviteText] || '';
  const code = venueAttributes.attributes[VenueAttributeName.SalesCodeStructure] || '';

  return (
    <ReferContainer>
      <EditControls>
        <EditButton
          className="fa fa-pen"
          onClick={() => {
            setIsModalOpen(true);
          }}
        />
      </EditControls>
      <BlockLabel>Refer points</BlockLabel>
      <ReferBlock>
        <ReferHeader>
          <div>
            <ReferTitle>{title}</ReferTitle>
            <PlainText>{description}</PlainText>
          </div>
          <PointsBadge>{points}</PointsBadge>
        </ReferHeader>
        <ImageBlock url={imageUrl} />
      </ReferBlock>
      <BlockLabel>Refer text</BlockLabel>
      <RewardPointsBlock style={{ marginBottom: 40 }}>
        <TextValueBlock>{inviteText}</TextValueBlock>
      </RewardPointsBlock>
      <BlockLabel>Sale Code Structure</BlockLabel>
      <RewardPointsBlock>
        <RewardThresholdBlock>{code}</RewardThresholdBlock>
      </RewardPointsBlock>
      {isModalOpen && (
        <UpdateReferModal
          data={data}
          salesCode={code}
          text={inviteText}
          handleClose={() => {
            setIsModalOpen(false);
          }}
        />
      )}
    </ReferContainer>
  );
};

export default ReferPointsComponent;
