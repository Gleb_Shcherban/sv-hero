import React, { useEffect, useMemo } from 'react';
import { RewardActivityType } from '../../../api/models/rewardables';

import { useAppDispatch, useTypedSelector } from '../../../store';
import { getRewardables } from '../../../store/slices/rewardablesSlice';

import ReferPointsComponent from '../ReferPointsComponent';
import SharePointsComponent from '../SharePointsComponent';
import SignUpPointsComponent from '../SignUpPointsComponent';

interface RewardablesSectionProps {}

const RewardablesSection: React.FC<RewardablesSectionProps> = () => {
  const dispatch = useAppDispatch();
  const { items: rewardables } = useTypedSelector((state) => state.rewardables);

  useEffect(() => {
    dispatch(getRewardables({}));
  }, [dispatch]);

  const renderRewardables = useMemo(() => {
    if (!rewardables.length) {
      return null;
    }
    const shareData = rewardables.find(
      (r) => r.activityType === RewardActivityType.SHARE_VIDEO
    );
    const referData = rewardables.find(
      (r) => r.activityType === RewardActivityType.REFER
    );
    const signUpData = rewardables.find(
      (r) => r.activityType === RewardActivityType.SIGN_UP
    );

    if (!shareData || !referData || !signUpData) {
      return null;
    }

    return (
      <>
        <div>
          <SignUpPointsComponent data={signUpData} />
          <ReferPointsComponent data={referData} />
        </div>
        <div>
          <SharePointsComponent data={shareData} />
        </div>
      </>
    );
  }, [rewardables]);

  return <>{renderRewardables}</>;
};

export default RewardablesSection;
