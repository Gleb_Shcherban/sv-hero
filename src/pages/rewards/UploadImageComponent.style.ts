import styled from 'styled-components';

export const ImageArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  max-width: 100%;
  height: 135px;
  background-color: #eaeaea;
  color: #007aff;
  margin-top: 20px;
  cursor: pointer;
`;

export const ImageBlock = styled.img`
  max-height: 50vh;
  width: 100%;
  height: auto;
  margin-top: 10px;
`;

export const EditImageButton = styled.div`
  position: absolute;
  display: inline-block;
  height: 44px;
  width: 44px;
  background-color: #fff;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.3);
  cursor: pointer;
  font-size: 18px;
  color: #222;
  line-height: 44px;
  text-align: center;
  border-radius: 50%;
  margin-left: 5px;
  right: 0;
  transform: translateX(22px);
`;

export const MaximumRewardsReachedContainer = styled.div`
  display: flex;
  font-size: 18px;
  font-weight: bold;
`;
