import React, { useState } from 'react';
import { RewardActivityApiModel } from '../../api/models/rewardables';
import {
  BlockLabel,
  EditButton,
  EditControls,
  PointsBadge,
  RewardPointsBlock,
  ShareContainer,
  SignUpBlock,
  TextValueBlock,
} from './Rewards.style';
import UpdateShareModal from './modals/UpdateShareModal';
import { useTypedSelector } from '../../store';
import { VenueAttributeName } from '../../api/models/venue';

interface SharePointsComponentProps {
  data: RewardActivityApiModel;
}

const SharePointsComponent: React.FC<SharePointsComponentProps> = ({ data }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { attributes } = useTypedSelector((state) => state.venue.venueAttributes);
  const { title, points, description } = data;

  return (
    <ShareContainer>
      <EditControls>
        <EditButton
          className="fa fa-pen"
          onClick={() => {
            setIsModalOpen(true);
          }}
        />
      </EditControls>
      <BlockLabel>Create & Share Points</BlockLabel>
      <SignUpBlock style={{ marginBottom: '20px' }}>
        <div>
          <TextValueBlock
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              marginBottom: 8,
            }}
          >
            {title}
          </TextValueBlock>
          <TextValueBlock>{description}</TextValueBlock>
        </div>
        <PointsBadge>{points}</PointsBadge>
      </SignUpBlock>
      <BlockLabel>Share text</BlockLabel>
      <RewardPointsBlock>
        <TextValueBlock>{attributes[VenueAttributeName.ShareText]}</TextValueBlock>
      </RewardPointsBlock>
      {isModalOpen && (
        <UpdateShareModal
          data={data}
          handleClose={() => {
            setIsModalOpen(false);
          }}
        />
      )}
    </ShareContainer>
  );
};

export default SharePointsComponent;
