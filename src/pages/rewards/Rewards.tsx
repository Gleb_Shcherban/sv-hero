import React, { useCallback, useEffect, useState } from 'react';
import { useAppDispatch, useTypedSelector } from '../../store';
import { getRewards } from '../../store/slices/rewardsSlice';
import { RewardApiModel } from '../../api/models/rewards';

import RewardComponent from './RewardComponent';
import { CreateOrUpdateRewardModal } from './modals/CreateOrUpdateRewardsModal/CreateOrUpdateRewardModal';

import { PageContainer, CtaButton, LayoutGrid } from './Rewards.style';
import RewardablesSection from './RewardablesSection/RewardablesSection';
import { FSModal } from '../../common/components/modal/Modal';
import { MaximumRewardsReached } from './modals/MaximumRewardsReached';
import { RemoveRewardModal } from './modals/RemoveRewardModal';
import { ALLOWED_REWARDS_COUNT } from '../../common/constants/constants';

//TODO: The whole rewards page should be refactored. All the files in rewards folder MUST be ordered and placed in respectable folders, or rewritten
export const Rewards: React.FC = () => {
  const dispatch = useAppDispatch();
  const [isCreateRewardModal, setIsCreateRewardModal] = useState(false);
  const [isUpdateRewardModal, setIsUpdateRewardModal] = useState(false);
  const [showMaximumRewardsReachedModal, setShowMaximumRewardsReachedModal] = useState(false);
  const [deleteRewardModalData, setDeleteRewardModalData] = useState<{
    id: string | null;
    title: string;
  }>({ id: null, title: '' });
  const [rewardToUpdate, setRewardToUpdate] = useState<RewardApiModel | null>(null);

  const { isLoading: rewardsLoading, items: rewards, lastUpdated } = useTypedSelector(
    (state) => state.rewards
  );

  useEffect(() => {
    dispatch(getRewards({}));
  }, [dispatch, lastUpdated]);

  const triggerUpdate = useCallback(
    (data: RewardApiModel) => {
      setRewardToUpdate(data);
      setIsUpdateRewardModal(true);
    },
    [setRewardToUpdate, setIsUpdateRewardModal]
  );

  const clickCreateNewReward = () => {
    if (rewards.length < ALLOWED_REWARDS_COUNT) {
      setIsCreateRewardModal(true);
    } else {
      setShowMaximumRewardsReachedModal(true);
    }
  };

  return (
    <PageContainer>
      <LayoutGrid>
        <CtaButton
          disabled={rewardsLoading}
          onClick={() => {
            clickCreateNewReward();
          }}
        >
          Create Reward
        </CtaButton>
      </LayoutGrid>
      <LayoutGrid>
        <div>
          {rewardsLoading ? (
            <div>Loading...</div>
          ) : (
            rewards.map((reward) => (
              <RewardComponent
                key={reward.id}
                data={reward}
                handleDelete={() => setDeleteRewardModalData({ id: reward.id, title: reward.name })}
                setUpdate={triggerUpdate}
              />
            ))
          )}
        </div>
        <RewardablesSection />
      </LayoutGrid>
      {isCreateRewardModal && (
        <CreateOrUpdateRewardModal handleClose={() => setIsCreateRewardModal(false)} />
      )}
      {isUpdateRewardModal && rewardToUpdate && (
        <CreateOrUpdateRewardModal
          handleClose={() => setIsUpdateRewardModal(false)}
          data={rewardToUpdate}
        />
      )}
      <FSModal
        modalIsOpen={showMaximumRewardsReachedModal}
        onClose={() => setShowMaximumRewardsReachedModal(false)}
      >
        <MaximumRewardsReached />
      </FSModal>
      <FSModal
        modalIsOpen={!!deleteRewardModalData.id}
        onClose={() => setDeleteRewardModalData({ id: null, title: '' })}
      >
        <RemoveRewardModal
          title={deleteRewardModalData.title}
          closeModal={() => setDeleteRewardModalData({ id: null, title: '' })}
          id={deleteRewardModalData.id || ''}
        />
      </FSModal>
    </PageContainer>
  );
};
