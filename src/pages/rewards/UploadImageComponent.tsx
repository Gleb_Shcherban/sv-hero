import React, { useRef, useState } from 'react';
import { MediaEndpoints } from '../../api/endpoints';
import { MediaFilePrefix } from '../../api/models/common';
import { Spinner } from '../../common/assets/Spinner';
import { httpClient } from '../../services/httpClient/httpClient';
import { ImageArea, ImageBlock, EditImageButton } from './UploadImageComponent.style';

interface UploadImageComponentProps {
  uri: string | null;
  setUri: (value: string) => void;
}

const UploadImageComponent: React.FC<UploadImageComponentProps> = ({
  uri,
  setUri
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const handleUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files ? e.target.files[0] : null;
    if (!file) {
      return;
    }

    setIsLoading(true);
    const formData = new FormData();
    formData.append('mediaFile', file);
    formData.append('prefix', MediaFilePrefix.Client);
    formData.append('isStockContent', 'true');

    httpClient.post<FormData, { url: string }>({
      url: MediaEndpoints.UploadFile,
      payload: formData,
      requiresToken: true,
    }).then((response) => {
      console.log(response);
      setIsLoading(false);
      setUri(response.url);
      setError(null);
    }).catch(() => {
      setError('Error occured, please, try again');
    })
  }

  const inputRef = useRef<HTMLInputElement | null>(null);

  const triggerUpload = () => {
    if (inputRef &&  inputRef.current) {
      inputRef.current.click();
    }
  }

  return (
    <div>
      {!uri && !isLoading && (
        <ImageArea onClick={triggerUpload}>
          {error ? <span>{error}</span> : <span>Click to upload an image</span>}
        </ImageArea>
      )}
      {isLoading && (
        <Spinner  color="var(--spinnerColor)" />
      )}
      {uri && !isLoading && (
        <div style={{ position: 'relative' }}>
          <EditImageButton className="fa fa-pen" onClick={triggerUpload} />
          <ImageBlock src={uri} />
        </div>
      )}
      <input
        type="file" accept="image/*"
        className="visually-hidden" ref={inputRef}
        onChange={handleUpload}
      />
    </div>
  )
}

export default UploadImageComponent;
