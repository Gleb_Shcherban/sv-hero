import React, { useState } from 'react';
import { RewardActivityApiModel } from '../../api/models/rewardables';
import {
  BlockLabel,
  EditButton,
  EditControls,
  PointsBadge,
  SignUpBlock,
  SignUpContainer,
  TextValueBlock,
} from './Rewards.style';
import UpdateSignUpModal from './modals/UpdateSignUpModal';

interface SignUpPointsComponentProps {
  data: RewardActivityApiModel;
}
const SignUpPointsComponent: React.FC<SignUpPointsComponentProps> = ({
  data,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { points, description } = data;

  return (
    <SignUpContainer>
      <EditControls>
        <EditButton
          className="fa fa-pen"
          onClick={() => {
            setIsModalOpen(true);
          }}
        />
      </EditControls>
      <BlockLabel>Sign-up points</BlockLabel>
      <SignUpBlock>
        <TextValueBlock>{description}</TextValueBlock>
        <PointsBadge>{points}</PointsBadge>
      </SignUpBlock>
      {isModalOpen && (
        <UpdateSignUpModal
          data={data}
          handleClose={() => {
            setIsModalOpen(false);
          }}
        />
      )}
    </SignUpContainer>
  );
};

export default SignUpPointsComponent;
