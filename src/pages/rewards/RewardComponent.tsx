import React from 'react';
import { RewardApiModel } from '../../api/models/rewards';
import {
  BlockLabel,
  RewardContainer,
  RewardImageBlock,
  RewardPointsBlock,
  RewardThresholdBlock,
  EditControls,
  EditButton
} from './Rewards.style';

interface RewardComponentProps {
  data: RewardApiModel;
  handleDelete: (id: string) => void;
  setUpdate: (data: RewardApiModel) => void;
}

const RewardComponent: React.FC<RewardComponentProps> = ({ data, handleDelete, setUpdate }) => {
  const { name, imageUri, redemptionPoints, id } = data;

  return (
    <RewardContainer>
      <EditControls>
        <EditButton className="fa fa-trash" onClick={() => handleDelete(id)} />
        <EditButton className="fa fa-pen" onClick={() => setUpdate(data)} />
      </EditControls>
      <BlockLabel>{name}</BlockLabel>
      <RewardImageBlock url={imageUri} />
      <BlockLabel>Threshold</BlockLabel>
      <RewardPointsBlock>
        <RewardThresholdBlock>{redemptionPoints}</RewardThresholdBlock>
      </RewardPointsBlock>
    </RewardContainer>
  )
}

export default RewardComponent;
