import React from 'react';
import { MaximumRewardsReachedContainer } from '../UploadImageComponent.style';
import { RewardsPageStrings } from '../../../common/localization/en';

export const MaximumRewardsReached: React.FC = () => {
  return (
    <MaximumRewardsReachedContainer>
      {RewardsPageStrings.MaximumRewardsReached}
    </MaximumRewardsReachedContainer>
  );
};
