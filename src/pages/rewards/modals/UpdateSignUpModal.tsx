import React, { useState } from 'react';
import { RewardActivityApiModel } from '../../../api/models/rewardables';
import { useAppDispatch } from '../../../store';
import { updateRewardables } from '../../../store/slices/rewardablesSlice';
import {
  FormInput,
  ModalCta,
  ModalOverlay,
  UpdateSignUpModalContainer,
} from '../Rewards.style';

interface UpdateSignUpModalProps {
  data: RewardActivityApiModel;
  handleClose: () => void;
}

const UpdateSignUpModal: React.FC<UpdateSignUpModalProps> = ({
  data,
  handleClose,
}) => {
  const [description, setDescription] = useState(data.description);
  const [points, setPoints] = useState<string | number>(data.points);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();

  const handleUpdate = () => {
    setIsLoading(true);
    const { id } = data;
    dispatch(
      updateRewardables({
        id,
        description,
        points: Number(points),
      })
    )
      .then(() => {
        setIsLoading(false);
        handleClose();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <ModalOverlay onClick={handleClose}>
      <UpdateSignUpModalContainer onClick={(e) => e.stopPropagation()}>
        <label>
          Name
          <FormInput
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter reward name"
          />
        </label>
        <label>
          Points
          <FormInput
            type="text"
            value={points}
            onChange={(e) => setPoints(e.target.value)}
            placeholder="100"
          />
        </label>
        <ModalCta
          disabled={!description || !points || isLoading}
          onClick={handleUpdate}
        >
          {isLoading ? 'Loading...' : 'Update'}
        </ModalCta>
      </UpdateSignUpModalContainer>
    </ModalOverlay>
  );
};

export default UpdateSignUpModal;
