import React, { useState } from 'react';
import { RewardActivityApiModel } from '../../../api/models/rewardables';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { updateRewardables } from '../../../store/slices/rewardablesSlice';
import { FormInput, ModalCta, ModalOverlay, UpdateSignUpModalContainer } from '../Rewards.style';
import { VenueAttributeName } from '../../../api/models/venue';
import { createOrUpdateVenueAttribute } from '../../../store/slices/venueSlice';

interface UpdateShareModalProps {
  data: RewardActivityApiModel;
  handleClose: () => void;
}

const UpdateShareModal: React.FC<UpdateShareModalProps> = ({ data, handleClose }) => {
  const { isLoading, attributes } = useTypedSelector((state) => state.venue.venueAttributes);
  const [description, setDescription] = useState(data.description);
  const [title, setTitle] = useState(data.title);
  const [points, setPoints] = useState<string | number>(data.points);
  const [shareText, setShareText] = useState<string>(
    attributes[VenueAttributeName.ShareText] || ''
  );
  const dispatch = useAppDispatch();

  const handleUpdate = () => {
    const { id } = data;
    dispatch(
      updateRewardables({
        id,
        description,
        title,
        points: Number(points),
      })
    );
    dispatch(
      createOrUpdateVenueAttribute([{ name: VenueAttributeName.ShareText, value: shareText }])
    );
    handleClose();
  };

  return (
    <ModalOverlay onClick={handleClose}>
      <UpdateSignUpModalContainer onClick={(e) => e.stopPropagation()}>
        <label>
          Title
          <FormInput
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Enter title"
          />
        </label>
        <label>
          Description
          <FormInput
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter description"
          />
        </label>
        <label>
          Share text
          <FormInput
            type="text"
            value={shareText}
            onChange={(e) => setShareText(e.target.value)}
            placeholder="Enter share text"
          />
        </label>
        <label>
          Points
          <FormInput
            type="text"
            value={points}
            onChange={(e) => setPoints(e.target.value)}
            placeholder="100"
          />
        </label>
        <ModalCta
          disabled={!description || !points || !shareText || isLoading}
          onClick={handleUpdate}
        >
          {isLoading ? 'Loading...' : 'Update'}
        </ModalCta>
      </UpdateSignUpModalContainer>
    </ModalOverlay>
  );
};

export default UpdateShareModal;
