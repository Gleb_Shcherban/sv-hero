import React from 'react';
import { RewardsPageStrings } from '../../../common/localization/en';
import { Button } from 'react-bootstrap';
import { EditVideoTitleContainer, TitleRow } from '../../videoTool/videoTool.style';
import { ButtonsContainer } from '../../monetization/editItemForms/EditForm.style';
import { useAppDispatch, useTypedSelector } from '../../../store';
import { deleteReward } from '../../../store/slices/rewardsSlice';

interface RemoveCategoryModalProps {
  id: string;
  title: string;
  closeModal: () => void;
}

export const RemoveRewardModal: React.FC<RemoveCategoryModalProps> = ({
  id,
  title,
  closeModal,
}) => {
  const { isLoading } = useTypedSelector((state) => state.rewards);
  const dispatch = useAppDispatch();
  const getButtonVariant = (variant: string) => {
    return isLoading ? 'dark' : variant;
  };

  const deleteCategory = () => {
    if (isLoading) return;
    dispatch(deleteReward({ id }));
    closeModal();
  };

  const promptString = RewardsPageStrings.DeleteRewardPrompt.replace('{title}', title);

  return (
    <EditVideoTitleContainer>
      <TitleRow>{promptString}</TitleRow>
      <ButtonsContainer>
        <Button variant={getButtonVariant('danger')} onClick={deleteCategory}>
          {RewardsPageStrings.DeleteReward}
        </Button>
      </ButtonsContainer>
    </EditVideoTitleContainer>
  );
};
