import React, { useState } from 'react';
import { VenueEndpoints } from '../../../api/endpoints';
import { RewardActivityApiModel } from '../../../api/models/rewardables';
import {
  CreateOrUpdateVenueAttributesRequest,
  CreateOrUpdateVenueAttributesResponse,
  VenueAttributeName,
} from '../../../api/models/venue';
import { httpClient } from '../../../services/httpClient/httpClient';
import { useAppDispatch } from '../../../store';
import { updateRewardables } from '../../../store/slices/rewardablesSlice';
import { getAttributes } from '../../../store/slices/venueSlice';
import { FormInput, ModalCta, ModalOverlay, UpdateSignUpModalContainer } from '../Rewards.style';
import UploadImageComponent from '../UploadImageComponent';

interface UpdateReferModalProps {
  data: RewardActivityApiModel;
  handleClose: () => void;
  salesCode: string;
  text: string;
}

const UpdateReferModal: React.FC<UpdateReferModalProps> = ({
  data,
  handleClose,
  salesCode,
  text,
}) => {
  const [title, setTitle] = useState(data.title);
  const [description, setDescription] = useState(data.description);
  const [points, setPoints] = useState<string | number>(data.points);
  const [imageUri, setImageUri] = useState(data.imageUrl);
  const [isLoading, setIsLoading] = useState(false);
  const [inviteText, setInviteText] = useState(text);
  const [code, setCode] = useState(salesCode);
  const dispatch = useAppDispatch();

  const triggerUpdate = () => {
    dispatch(getAttributes());
  };

  const updateAttributes = async () => {
    httpClient
      .post<CreateOrUpdateVenueAttributesRequest, CreateOrUpdateVenueAttributesResponse>({
        url: VenueEndpoints.CreateOrUpdateVenueAttributes,
        requiresToken: true,
        payload: [
          {
            name: VenueAttributeName.InviteText,
            value: inviteText,
          },
          {
            name: VenueAttributeName.SalesCodeStructure,
            value: code,
          },
        ],
      })
      .then(() => {
        console.log('success update');
        setIsLoading(false);
        triggerUpdate();
        handleClose();
      })
      .catch((e) => {
        console.log('Error, while uploading image', e);
      });
  };

  const handleUpdate = () => {
    setIsLoading(true);
    const { id } = data;
    dispatch(
      updateRewardables({
        id,
        description,
        points: Number(points),
        title,
        imageUrl: imageUri,
      })
    )
      .then(() => {
        updateAttributes();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <ModalOverlay onClick={handleClose}>
      <UpdateSignUpModalContainer onClick={(e) => e.stopPropagation()}>
        <label>
          Title
          <FormInput
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Enter title"
          />
        </label>
        <label>
          Description
          <FormInput
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter description"
          />
        </label>
        <label>
          Points
          <FormInput
            type="text"
            value={points}
            onChange={(e) => setPoints(e.target.value)}
            placeholder="100"
          />
        </label>
        <label>
          Refer Text
          <FormInput
            type="text"
            value={inviteText}
            onChange={(e) => setInviteText(e.target.value)}
            placeholder="Invite text"
          />
        </label>
        <label>
          Sales Code Structure
          <FormInput
            type="text"
            value={code}
            onChange={(e) => setCode(e.target.value)}
            placeholder="XIDDO"
          />
        </label>
        <UploadImageComponent uri={imageUri} setUri={setImageUri} />
        <ModalCta
          disabled={!description || !points || !inviteText || isLoading}
          onClick={handleUpdate}
        >
          {isLoading ? 'Loading...' : 'Update'}
        </ModalCta>
      </UpdateSignUpModalContainer>
    </ModalOverlay>
  );
};

export default UpdateReferModal;
