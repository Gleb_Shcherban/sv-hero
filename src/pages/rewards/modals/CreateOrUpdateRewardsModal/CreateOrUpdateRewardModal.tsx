import React, { useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import UploadImageComponent from '../../UploadImageComponent';
import { ModalOverlay, CreateRewardModalContainer, ModalCta } from '../../Rewards.style';
import { useAppDispatch, useTypedSelector } from '../../../../store';
import { createReward, updateReward } from '../../../../store/slices/rewardsSlice';
import { RewardsPageStrings } from '../../../../common/localization/en';
import { InputField } from '../../../../common/components/inputField/InputField';
import { CreateRewardForm, validationSchema, defaultInitialValues } from './formHelper';
import { RewardApiModel } from '../../../../api/models/rewards';

interface CreateRewardModalProps {
  data?: RewardApiModel;
  handleClose: () => void;
}

export const CreateOrUpdateRewardModal: React.FC<CreateRewardModalProps> = ({
  handleClose,
  data,
}) => {
  const dispatch = useAppDispatch();
  const [imageUri, setImageUri] = useState(data ? data.imageUri : '');
  const { isLoading } = useTypedSelector((state) => state.rewards);
  const initialValues: CreateRewardForm = data
    ? { name: data.name, reward: data.reward, redemptionPoints: data.redemptionPoints }
    : defaultInitialValues;

  const rewardForm = useFormik({
    initialValues,
    validateOnBlur: false,
    validateOnChange: false,
    validationSchema: Yup.object().shape<CreateRewardForm>(validationSchema),
    onSubmit: (values) => {
      data ? handleChangeReward(values) : handleCreateReward(values);
    },
  });

  const handleCreateReward = (formValues: CreateRewardForm) => {
    if (!imageUri) return;

    dispatch(
      createReward({
        imageUri,
        name: formValues.name,
        redemptionPoints: Number(formValues.redemptionPoints),
        reward: formValues.reward,
        isActive: true,
      })
    );
    handleClose();
  };

  const handleChangeReward = (formValues: CreateRewardForm) => {
    if (!imageUri || !data) return;

    dispatch(
      updateReward({
        id: data.id,
        imageUri,
        venueId: data.venueId,
        name: formValues.name,
        redemptionPoints: Number(formValues.redemptionPoints),
        reward: formValues.reward,
        isActive: data.isActive,
      })
    );
    handleClose();
  };

  const changeNumberValue = (val: React.ChangeEvent<HTMLInputElement>, field: string) => {
    const resultValue = parseInt(val.target.value);
    rewardForm.setFieldValue(field, resultValue ? String(resultValue) : '0');
  };

  const getButtonText = () => {
    if (isLoading) return RewardsPageStrings.Loading;
    if (data) return RewardsPageStrings.UpdateReward;
    return RewardsPageStrings.CreateReward;
  };

  return (
    <ModalOverlay onClick={handleClose}>
      <CreateRewardModalContainer onClick={(e) => e.stopPropagation()}>
        <InputField
          name="name"
          value={rewardForm.values.name}
          onChange={rewardForm.handleChange}
          placeholder={RewardsPageStrings.EnterRewardNamePlaceholder}
          errorText={rewardForm.errors.name}
          headerText={'Name'}
        />
        <br />
        <InputField
          name="reward"
          value={rewardForm.values.reward}
          onChange={rewardForm.handleChange}
          placeholder={RewardsPageStrings.EnterRewardValuePlaceholder}
          errorText={rewardForm.errors.reward}
          headerText={'Reward'}
        />
        <br />
        <InputField
          name="redemptionPoints"
          type="number"
          value={rewardForm.values.redemptionPoints}
          onChange={(val) => changeNumberValue(val, 'redemptionPoints')}
          // placeholder={RewardsPageStrings.EnterRedemptionPointsPlaceholder}
          errorText={rewardForm.errors.redemptionPoints}
          headerText={'Redemption points'}
        />
        <UploadImageComponent uri={imageUri} setUri={setImageUri} />
        <ModalCta
          disabled={
            !rewardForm.values.name || !rewardForm.values.redemptionPoints || !imageUri || isLoading
          }
          onClick={rewardForm.submitForm}
        >
          {getButtonText()}
        </ModalCta>
      </CreateRewardModalContainer>
    </ModalOverlay>
  );
};
