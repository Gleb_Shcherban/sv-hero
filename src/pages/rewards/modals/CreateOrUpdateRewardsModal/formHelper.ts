import * as Yup from 'yup';

export interface CreateRewardForm {
  name: string;
  reward: string;
  redemptionPoints: number;
}

export const defaultInitialValues: CreateRewardForm = {
  name: '',
  reward: '',
  redemptionPoints: 0,
};

export const validationSchema: Yup.ObjectSchemaDefinition<CreateRewardForm> = {
  name: Yup.string()
    .matches(/^[_A-z0-9]*((-|\s|!|@|,)*[_A-z0-9])*$/gi, 'Should only contain numbers and letters')
    .max(200, 'max length 200 symbols')
    .required('Required'),
  reward: Yup.string()
    .max(200, 'max length 200 symbols')
    .matches(/^[_A-z0-9]*((-|\s|!|@|,)*[_A-z0-9])*$/gi, 'Should only contain numbers and letters')
    .required('Required'),
  redemptionPoints: Yup.number().required('Required'),
};
